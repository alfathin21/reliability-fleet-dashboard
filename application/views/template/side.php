<body>
  <div id="wrapper">
   <nav class="navbar-default navbar-static-side" role="navigation" style="position:fixed" role="navigation">
    <div class="sidebar-collapse">
      <ul class="nav metismenu" id="side-menu">
        <!-- menu nama admin -->
        <li class="nav-header">
          <div class="dropdown profile-element"> <span>
         
             <?php if ($this->session->userdata('unit') == 'GA') :?>
                <img alt="image" style="border: 1px solid white; box-shadow: 1px 1px 9px rgba(0,0,0,0.4);" class="img-circle" src="<?= base_url()?>assets/logo/logo_ga.jpeg" width="63" />
                <?php elseif ($this->session->userdata('unit') == 'QG') : ?>
                  <img alt="image" style="border: 2px solid white; box-shadow: 1px 1px 9px rgba(0,0,0,0.4);" class="img-circle" src="<?= base_url()?>assets/logo/logo_qg.jpg" width="63" />
                  <?php elseif ($this->session->userdata('unit') == 'Full access') : ?>
                    <img alt="image" style="border: 2px solid white; box-shadow: 1px 1px 9px rgba(0,0,0,0.4);" class="img-circle" src="<?= base_url()?>assets/images/user.png" width="63x" />
                    <?php elseif ($this->session->userdata('unit') == 'SJ') : ?>
                      <img alt="image" style="border: 1px solid white; box-shadow: 1px 1px 9px rgba(0,0,0,0.4);" class="img-circle" src="<?= base_url()?>assets/logo/logo_sj.jpg" width="63" />
                      <?php elseif ($this->session->userdata('unit') == 'IN') :?>
                        <img alt="image" style="border: 1px solid white; box-shadow: 1px 1px 9px rgba(0,0,0,0.4);" class="img-circle" src="<?= base_url()?>assets/logo/logo_in.jpg" width="63" />
                        <?php elseif ($this->session->userdata('unit') == 'OT') :?>
                          <img alt="image" style="border: 1px solid white; box-shadow: 1px 1px 9px rgba(0,0,0,0.4);" class="img-circle" src="<?= base_url()?>assets/logo/user.png" width="63" />
                          <?php else : ?>
                            <img alt="image" style="border: 2px solid white; box-shadow: 1px 1px 9px rgba(0,0,0,0.4);" class="img-circle" src="https://talentlead.gmf-aeroasia.co.id/images/avatar/<?php echo $this->session->userdata('nopeg');?>.jpg" width="63x" />
                          <?php endif; ?>
          </span>
          <a class="dropdown-toggle" href="#">
            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?= $this->session->userdata('nama');?></strong>
            </span>

              <?php if ($this->session->userdata('unit') == 'GA') : ?>
                            <span style="text-shadow: 1px 1px 9px rgba(0,0,0,0.9)" class="text-xs block">Operator : GARUDA </b></span> </span> </a>
                            <?php elseif ($this->session->userdata('unit') == 'QG') :?>
                              <span style="text-shadow: 1px 1px 9px rgba(0,0,0,0.9)" class="text-xs block">Operator : CITILINK </b></span>
                            </span> </a>
                            <?php elseif ($this->session->userdata('unit') == 'SJ') :?>
                              <span style="text-shadow: 1px 1px 9px rgba(0,0,0,0.9)" class="text-xs block">Operator : SRIWIJAYA </b></span>
                            </span> </a>
                            <?php elseif ($this->session->userdata('unit') == 'IN') :?>
                              <span style="text-shadow: 1px 1px 9px rgba(0,0,0,0.9)" class="text-xs block">Operator : NAM AIR </b></span>
                            </span> </a>
                            <?php elseif ($this->session->userdata('unit') == 'OT') : ?>
                            <span style="text-shadow: 1px 1px 9px rgba(0,0,0,0.9)" class="text-xs block">UNIT : OTHERS</b></span> </span> </a>
                            <?php else : ?>
                             <span style="text-shadow: 1px 1px 9px rgba(0,0,0,0.9)" class="text-xs block">UNIT : <?= $this->session->userdata('unit') ?></b></span> </span> </a>
                           <?php endif; ?>
          </div>
          <div class="logo-element">
          </div>
        </li>
        <!-- akhir -->

        <!-- Menu kanan -->
        <?php if ($title == 'Techlog / Delay') :?>
          <li class="active">
            <a href="<?= base_url() ?>techlog"><i class="fa fa-folder-o"></i> <span class="nav-label">Techlog / Delay</span> <span class="fa arrow"></span></a>
          </li>
          <?php else : ?>
           <li>
            <a href="<?= base_url() ?>techlog"><i class="fa fa-folder-o"></i> <span class="nav-label">Techlog / Delay</span> <span class="fa arrow"></span></a>
          </li>
        <?php endif; ?>

        <?php if ($title == 'Pareto Techlog / Delay') :?>
         <li class="active">
          <a  href="<?= base_url() ?>pareto"><i class="fa fa-laptop"></i> <span class="nav-label"> Pareto Techlog / Delay</span> <span class="fa arrow"></span></a>
        </li>
        <?php else : ?>
         <li>
          <a  href="<?= base_url() ?>pareto"><i class="fa fa-laptop"></i> <span class="nav-label"> Pareto Techlog / Delay</span> <span class="fa arrow"></span></a>
        </li>
      <?php endif; ?>


      <?php if ($title  == 'Components Removal') : ?>
        <li class="active">
          <a href="<?= base_url() ?>components" ><i class="fa fa-cogs"></i> <span class="nav-label">Components Removal</span> <span class="fa arrow"></span></a>
        </li>
        <?php else : ?>
          <li>
            <a href="<?= base_url() ?>components" ><i class="fa fa-cogs"></i> <span class="nav-label">Components Removal</span> <span class="fa arrow"></span></a>
          </li>
        <?php endif; ?>

        <?php if ($title == 'Pareto Component Removal') : ?>
         <li class="active">
          <a  href="<?= base_url() ?>PComponent" ><i class="fa fa-hdd-o"></i> <span class="nav-label"> Pareto  Removal</span> <span class="fa arrow"></span></a>
        </li>    

        <?php else : ?>
          <li>
            <a  href="<?= base_url() ?>PComponent" ><i class="fa fa-hdd-o"></i> <span class="nav-label"> Pareto  Removal</span> <span class="fa arrow"></span></a>
          </li>    
        <?php endif; ?>
        

        <?php if ($title == 'MTBUR') : ?>
          <li class="active">
            <a href="<?= base_url() ?>mtbur"><i class="fa fa-calculator"></i> <span class="nav-label"> MTBUR</span> <span class="fa arrow"></span></a>
          </li>


          <?php else : ?>
            <li>
              <a href="<?= base_url() ?>mtbur"><i class="fa fa-calculator"></i> <span class="nav-label"> MTBUR</span> <span class="fa arrow"></span></a>
            </li>
          <?php endif; ?>


          <?php if ($title == 'World Wide') : ?>
            <li class="active">
              <a href="<?= base_url() ?>mtburww"><i class="fa fa-globe"></i> <span class="nav-label"> MTBUR WW</span> <span class="fa arrow"></span></a>
            </li>
            <?php else : ?>
             <li>
              <a href="<?= base_url() ?>mtburww"><i class="fa fa-globe"></i> <span class="nav-label"> MTBUR WW</span> <span class="fa arrow"></span></a>
            </li>
          <?php endif; ?>

       
          <!-- Akhir Menu -->
        </ul>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg dashbard-1">
      <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
          <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
          </div>
          <ul class="nav navbar-top-links navbar-right">
            <!-- Notif E-mail -->
            <li class="dropdown">

              <ul class="dropdown-menu dropdown-messages">
               <li class="divider"></li>

             </ul>
           </li>
           <!-- notif message -->
           <li>
            <a href="<?= base_url('auth/logout') ?>">
              <i class="fa fa-sign-out"></i> Log out
            </a>
          </li>
        </ul>
        <!-- Akhir notif -->
      </nav>
    </div>

    <div class="row">
      <div class="col-lg-12">