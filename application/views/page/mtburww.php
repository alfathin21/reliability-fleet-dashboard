<div class="wrapper wrapper-content">
<div class="row">
  <div class="col-md-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-globe"></i>&nbsp; World Wide MTBUR</h5>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content inspinia-timeline table-responsive" id="result">
         <iframe src="https://drive.google.com/embeddedfolderview?id=0BwGUEcfNKl-MODdjYUlvbzR0a2c#grid" style="width:100%; height:600px; border:0;"></iframe>
      </div>
    </div>
  </div>
</div>
</div>
</div>

