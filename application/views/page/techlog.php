<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins" id="form_query" >
        <div class="ibox-title">
          <h5><i class="fa fa-database"></i>&nbsp; Filter Techlog / Delay Criteria</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline" >
          <div class="row" >
            <div class="col-md-4">
              <div class="form-group">
                <label for="">OPERATOR <span style="color: red">*</span> :</label>
                <select onchange="io();" class="form-control" id="operator">
                  <?php if ($unit == 'GA') : ?>
                    <option value="GARUDA INDONESIA">GARUDA</option>
                    <?php elseif ($unit == 'QG'): ?> 
                      <option value="CITILINK">CITILINK</option>
                      <?php elseif ($unit == 'SJ'): ?> 
                        <option value="SRIWIJAYA">SRIWIJAYA</option>
                        <?php elseif ($unit == 'IN'): ?> 
                          <option value="NAM">NAM AIR</option>
                          <?php else : ?>
                           <option value=""></option>
                           <option value="GARUDA INDONESIA">GARUDA</option>
                           <option value="CITILINK">CITILINK</option>
                           <option value="SRIWIJAYA">SRIWIJAYA</option>
                           <option value="NAM">NAM AIR</option>
                         <?php endif; ?>
                       </select>
                     </div>
                   </div>
                   <div class="col-md-4">
                    <div class="form-group">
                      <label for="">A/C TYPE <span style="color: red">*</span> :</label>
                      <select onchange="ac();"  class="form-control" id="actype" multiple="" >
                       <option value=""></option>
                     </select>
                   </div>
                 </div>
                 <div class="col-md-4">
                  <div class="form-group">
                    <label for="">A/C REG :</label>
                    <input type="text" name="acreg" id="acreg" class="form-control">
                    <span><i>Entry without "PK-"</i></span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="">DATE FROM <span style="color: red">*</span> :</label>
                    <input type="hidden" id="clue">
                    <input type="text" name="date_from" id="date_from"  data-language='en' data-date-format="yyyy-mm-dd" class="form-control">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="">DATE TO <span style="color: red">*</span> :</label>
                    <input type="text" name="date_to" id="date_to"  data-language='en' data-date-format="yyyy-mm-dd" class="form-control">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="">ATA :</label><br>
                    <div id="ot"></div>
                    <div id="at">
                      <select onchange="atakey()" class="form-control" id="ata">
                        <option value=""></option>
                        <?php foreach ($ata as $key) :?>
                          <option value="<?= $key['ATA'] ?>"><?= $key['ATA'] ?></option>
                        <?php endforeach; ?>
                      </select> 
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="">SUB ATA :</label><br>
                    <input type="number" class="form-control" id="subata">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="">KEYWORD ( Free Text ) :</label>
                    <input type="text" name="keyword" id="keyword" class="form-control">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="">KEYPROBLEM :</label>
                    <div id="oy"></div>
                    <div id="ky">
                      <select name="" class="form-control" id="KeyProblem"></select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
               <div class="col-md-6">
                <div class="form-group">
                  <label class="col-sm-4 control-label">DELAY / PIREP :</label>
                  <div class="col-sm-12">
                    <div class="radio">
                      <label>
                        <input type="radio" name="depir" value="delay" id="radio_delay" onclick="check(this.value)"> DELAY <br>
                        <label class="checkbox-inline">
                          <input type="checkbox" id="cl_delay"> DELAY
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" id="cl_cancel"> CANCEL
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" id="cl_x"> NON TECHNICAL DELAY
                        </label><br>
                      </label>
                    </div>
                    <select name="" id="filter" class="form-control">
                      <option value=""></option>
                      <option value="RTA">RTA</option>
                      <option value="RTO">RTO</option>
                      <option value="RTB">RTB</option>
                      <option value="RTG">RTG</option>
                    </select>
                    <div class="radio">
                      <label>
                        <input type="radio" name="depir" value="pirep" id="radio_pirep" onclick="check(this.value)"> TECHLOG<br>
                        <label class="checkbox-inline">
                          <input type="hidden" id="dect">
                          <input type="checkbox" name="sama" class="a"  id="pirep"> PIREP
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" name="sama"  class="a" id="marep"> MAREP
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" name="sama"  class="a"  id="sj"> SCHEDULED JOB CARD
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" name="sama"  class="a" id="un"> CLOSING PROBLEM
                        </label>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="p">
                 <br> <br> <br> <br> <br> <br><br><br>
                 <button type="button" id="display" class="btn btn-success"><i class="fa fa-print"></i>&nbsp;&nbsp;Display Report</button>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
   <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-bar-chart"></i>&nbsp; Grafik Table Delay / Techlog</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <div id="crut" class="table-responsive">
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-bar-chart"></i>&nbsp; Result Table Delay / Techlog</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hovered" id="table_delay"></table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript"> 

$(document).on('keypress',function(e) {
  if(e.which == 13) { 
 if ($.fn.dataTable.isDataTable('#table_delay')) {
    var table = $("#table_delay").DataTable();
    table.destroy();
    $('#table_delay').empty();
  }
  var actype = $('#actype').val();
  var operator = $('#operator').val();
  var acreg  = $('#acreg').val();
  var date_to = $('#date_to').val();
  var KeyProblem = $('#KeyProblem').val();
  var date_from = $('#date_from').val();
  var ata = $('#ata').val();
  var sub_ata = $('#subata').val();
  var keyword = $('#keyword').val();
  var cl_delay = $('#cl_delay').val();
  var cl_x = $('#cl_x').val();
  var cl_cancel = $('#cl_cancel').val();
  var radio_delay = $('#radio_delay').val();
  var radio_pirep = $('#radio_pirep').val();
  var dect = $('#dect').val();
  var filter = $('#filter').val();
  var sj = $('#sj').val();
  var un = $('#un').val();
  var pirep = $('#pirep').val();
  var marep = $('#marep').val();
  var d_t = moment(date_to).format("DD MMMM YYYY");
  var d_f = moment(date_from).format("DD MMMM YYYY");
  var cl_dcp = document.getElementById("cl_delay").checked || document.getElementById("cl_cancel").checked || document.getElementById("cl_x").checked ;
  var cl_pir = document.getElementById("sj").checked || document.getElementById("un").checked || document.getElementById("pirep").checked || document.getElementById("marep").checked;
  if (operator == '') {
    alert("Please select Operator !");
    return false;
  }
  if (actype == '' || actype == null) {
    alert("Please select  A/Ctype !");
    return false;
  }
  if(document.getElementById("radio_delay").checked){
   if(date_from == "" || date_to == ""){
    alert("Field Datefrom and Dateto must not empty");
    return false;
  }
  else if(cl_dcp == false){
    alert("Checklist Delay, Cancel or All must not empty");
    return false;
  }
}
if(document.getElementById("radio_pirep").checked){
 if(date_from == "" || date_to == ""){
  alert("Field Datefrom and Dateto must not empty");
  return false;
}
else if(cl_pir == false){
  alert("Checklist Pirep, Marep or others must not empty");
  return false;
}
}
$('#display').attr('disabled', true);
$('#display').html('Loading .. !');
var pencarian_hasil = '<thead id ="head_cek">' +
'<th class="text-center">No</th>' +
'<th class="text-center">Notif</th>' +
'<th class="text-center">Date</th>' +
'<th class="text-center">A/C Type</th>' +
'<th class="text-center">A/C Reg</th>' +
'<th class="text-center">Sta Dep</th>' +
'<th class="text-center">Sta Arr</th>' +
'<th class="text-center">Flight No</th>' +
'<th class="text-center">Tech Dur</th>' +
'<th class="text-center">ATA' +
'<th class="text-center">Sub ATA' +
'<th class="text-center">Problem' +
'<th class="text-center ">Rectification' +
'<th class="text-center col-md-3" width="90%">Chronology' +
'<th class="text-center">DCP' +
'<th class="text-center">RTB <br> RTA <br> RTO' +
'<th class="text-center">#' +
'</tr></thead>' +
'<tbody id="enakeun"></tbody>';
var pencarian_kedua = '<thead id ="head_cek">' +
'<th class="text-center">No</th>' +
'<th class="text-center">Date</th>' +
'<th class="text-center">Sequence</th>' +
'<th class="text-center">Notification Number</th>' +
'<th class="text-center">A/C Type</th>' +
'<th class="text-center">A/C Reg</th>' +
'<th class="text-center">Sta Dep</th>' +
'<th class="text-center">Sta Arr</th>' +
'<th class="text-center">Flight No' +
'<th class="text-center">ATA' +
'<th class="text-center">Sub ATA' +
'<th class="text-center">Problem' +
'<th class="text-center">Rectification' +
'<th class="text-center">Coding' +
'<th class="text-center">#' +
'</tr></thead>' +
'<tbody id="enakeun"></tbody>';
if (document.getElementById("radio_delay").checked) {
  $('#table_delay').append(pencarian_hasil);

  console.log(KeyProblem);
  var judul = 'Delay - ' + actype + date_from + ' - ' + date_to + acreg +  ata;
  var judul_delay = 'Delay - ' + actype + ' ' + acreg + ' ' + ata + ' ' + KeyProblem + ' ' + sub_ata;
  var table = $("#table_delay").DataTable({
   dom: '<"html5buttons"B>lTfgitp',
   buttons: [
   {extend: 'excel', title: judul},
   {extend: 'pdf', title: judul,pageSize: 'A0'},
   ],
   aLengthMenu : [[10, 25, 50, -1], [10, 25, 50, "All"]],
   ajax: {
    "url": "<?= base_url('Techlog/search') ?>",
    "type": "POST",
    "data": {
      "actype": actype,
      "acreg": acreg,
      "dateto": date_to,
      "datefrom": date_from,
      "ata": ata,
      "sub_ata": sub_ata,
      "keyword": keyword,
      "radio_delay" : radio_delay,
      "radio_pirep" : radio_pirep,
      "cl_delay": cl_delay,
      "cl_x": cl_x,
      "cl_cancel": cl_cancel,
      "KeyProblem" : KeyProblem,
      "filter"  : filter
    },
    complete: function (res) {
      $('#display').attr('disabled', false);
      $('#display').html('<i class="fa fa-print"></i>&nbsp;&nbsp;DISPLAY REPORT');
      var b = res.responseJSON;
      var c = b.x;
      var title = b.title;
      var d = b.y;
      var e = [];
      var f = [];
      $('#cl_delay').prop('checked', false);
      $('#cl_x').prop('checked', false);
      $('#cl_cancel').prop('checked', false);
      $('#cl_delay').val('');
      $('#cl_x').val('');
      $('#cl_cancel').val('');
      for (var i = 0; i <  c.length; i++) {
        e.push(c[i]);
        var j = Math.round(Number(d[i]));
        f.push(j);

      }
      Highcharts.chart('crut', {
        title: {
          text: judul_delay
        },

        subtitle: {
          text: 'Periode : ' + d_f + ' - ' + d_t
        },
        xAxis: {
          categories: e
        },
        yAxis: {
          title: {
            text: 'Number Of Occurrence'
          },
          showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
        }, 
        series: [ {
          name: '',
          data: f
        }],
        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
              }
            }
          }]
        }
      });
    }
  },
  'columns': [
  {
    data : 'no'
  },
  {
    data : 'notif'
  },
  {
    data: 'DateEvent'
  },
  {
    data: 'ACtype'
  },
  {
    data: 'Reg'
  },
  {
    data: 'DepSta'
  },   
  {
    data: 'ArivSta'
  },
  {
    data: 'FlightNo'
  },
  {
    data: 'total'
  },
  {
    data: 'ATAtdm'
  },
  {
    data: 'SubATAtdm'
  },
  {
    data: 'Problem'
  },
  {
    data: 'last',
  },
  {
    data: 'Rectification',

  },
  {
    data: 'DCP'
  }, 
  {
    data: 'RtABO'
  },
  {
    data: 'act'
  }
  ]
});
  $('body').addClass('mini-navbar');

} else if (document.getElementById("radio_pirep").checked) {
  $('#table_delay').append(pencarian_kedua);
  var judul = 'Pirep - ' + actype + date_from + ' - ' + date_to + acreg +  ata;
  var judul_pirep = 'Pirep - ' + actype + ' ' + acreg + ' ' + ata + ' ' + KeyProblem + ' ' + sub_ata;
  var table = $("#table_delay").DataTable({

   dom: '<"html5buttons"B>lTfgitp',
   buttons: [
   {extend: 'excel', title: judul},
   {extend: 'pdf', title: judul,pageSize: 'A0'},
   ],
   aLengthMenu : [[10, 25, 50, -1], [10, 25, 50, "All"]],

   ajax: {
    "url": "<?= base_url('Techlog/search2') ?>",
    "type": "POST",
    "data": {
      "actype": actype,
      "acreg": acreg,
      "dateto": date_to,
      "datefrom": date_from,
      "ata": ata,
      "sub_ata": sub_ata,
      "keyword": keyword,
      "radio_delay" : radio_delay,
      "radio_pirep" : radio_pirep,
      "dect"        : dect,
      "KeyProblem"  : KeyProblem,
      "filter"      : filter,
      "sj"          : sj,
      "un"          : un,
      "pirep"       : pirep,
      "marep"       : marep
    },
    complete: function (res) {
      $('#display').attr('disabled', false);
      $('#display').html('<i class="fa fa-print"></i>&nbsp;&nbsp;DISPLAY REPORT');
      var b = res.responseJSON;
      var c = b.x;
      var title = b.title;
      var d = b.y;
      var e = [];
      var f = [];
      $('#pirep').prop('checked', false);
      $('#marep').prop('checked', false);
      $('#un').prop('checked', false);
      $('#sj').prop('checked', false);
      $('#pirep').val('');
      $('#marep').val('');
      $('#un').val('');
      $('#sj').val('');
      for (var i = 0; i <  c.length; i++) {
        e.push(c[i]);
        var j = Math.round(Number(d[i]));
        f.push(j);

      }
      Highcharts.chart('crut', {
        title: {
          text: judul_pirep
        },

        subtitle: {
          text: 'Periode : ' + d_f + ' - ' + d_t
        },
        xAxis: {
          categories: e
        },
       yAxis: {
          title: {
            text: 'Number Of Occurrence'
          },
          showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
        },
        series: [ {
          name: 'Month',
          data: f
        }],

        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
              }
            }
          }]
        }

      });
    }

  },

  'columns': [{
    data: 'no'
  },
  {
    data: 'DATE'
  },
  {
    data: 'SEQ'
  },
  {
    data: 'Notification'
  },
  {
    data: 'ACTYPE'
  },   
  {
    data: 'REG'
  },
  {
    data: 'STADEP'
  },
  {
    data: 'STAARR'
  },
  {
    data: 'FN'
  },
  {
    data: 'ATA'
  },
  {
    data: 'SUB_ATA'
  },
  {
    data: 'PROBLEM'
  },
  {
    data: 'ACTION'
  }, 
  {
    data: 'PirepMarep'
  },   
  {
    data: 'act'
  }
  ]
});
  $('body').addClass('mini-navbar');

}
  }
});




  $('#cl_delay').on('click', function () {
    $('#cl_delay').val('d');
  });
  $('#cl_cancel').on('click', function () {
    $('#cl_cancel').val('c');
  });    
  $('#cl_x').on('click', function () {
    $('#cl_x').val('x');
  });  
  $('#pirep').on('click', function () {
    $('#pirep').val('Pirep');
  }); 
  $('#marep').on('click', function () {
    $('#marep').val('Marep');
  });
  $('#sj').on('click', function () {
    $('#sj').val('Scheduled Job Card / Work Order');
  }); 
  $('#un').on('click', function () {
    $('#un').val('Closing Problem');
  });
  $('#display').on('click', function () {
   if ($.fn.dataTable.isDataTable('#table_delay')) {
    var table = $("#table_delay").DataTable();
    table.destroy();
    $('#table_delay').empty();
  }
  var actype = $('#actype').val();
  var operator = $('#operator').val();
  var acreg  = $('#acreg').val();
  var date_to = $('#date_to').val();
  var KeyProblem = $('#KeyProblem').val();
  var date_from = $('#date_from').val();
  var ata = $('#ata').val();
  var sub_ata = $('#subata').val();
  var keyword = $('#keyword').val();
  var cl_delay = $('#cl_delay').val();
  var cl_x = $('#cl_x').val();
  var cl_cancel = $('#cl_cancel').val();
  var radio_delay = $('#radio_delay').val();
  var radio_pirep = $('#radio_pirep').val();
  var dect = $('#dect').val();
  var filter = $('#filter').val();
  var sj = $('#sj').val();
  var un = $('#un').val();
  var pirep = $('#pirep').val();
  var marep = $('#marep').val();
  var d_t = moment(date_to).format("DD MMMM YYYY");
  var d_f = moment(date_from).format("DD MMMM YYYY");
  var cl_dcp = document.getElementById("cl_delay").checked || document.getElementById("cl_cancel").checked || document.getElementById("cl_x").checked ;
  var cl_pir = document.getElementById("sj").checked || document.getElementById("un").checked || document.getElementById("pirep").checked || document.getElementById("marep").checked;
  if (operator == '') {
    alert("Please select Operator !");
    return false;
  }
  if (actype == '' || actype == null) {
    alert("Please select  A/Ctype !");
    return false;
  }
  if(document.getElementById("radio_delay").checked){
   if(date_from == "" || date_to == ""){
    alert("Field Datefrom and Dateto must not empty");
    return false;
  }
  else if(cl_dcp == false){
    alert("Checklist Delay, Cancel or All must not empty");
    return false;
  }
}
if(document.getElementById("radio_pirep").checked){
 if(date_from == "" || date_to == ""){
  alert("Field Datefrom and Dateto must not empty");
  return false;
}
else if(cl_pir == false){
  alert("Checklist Pirep, Marep or others must not empty");
  return false;
}
}
$('#display').attr('disabled', true);
$('#display').html('Loading .. !');
var pencarian_hasil = '<thead id ="head_cek">' +
'<th class="text-center">No</th>' +
'<th class="text-center">Notif</th>' +
'<th class="text-center">Date</th>' +
'<th class="text-center">A/C Type</th>' +
'<th class="text-center">A/C Reg</th>' +
'<th class="text-center">Sta Dep</th>' +
'<th class="text-center">Sta Arr</th>' +
'<th class="text-center">Flight No</th>' +
'<th class="text-center">Tech Dur</th>' +
'<th class="text-center">ATA' +
'<th class="text-center">Sub ATA' +
'<th class="text-center">Problem' +
'<th class="text-center ">Rectification' +
'<th class="text-center col-md-3" width="90%">Chronology' +
'<th class="text-center">DCP' +
'<th class="text-center">RTB <br> RTA <br> RTO' +
'<th class="text-center">#' +
'</tr></thead>' +
'<tbody id="enakeun"></tbody>';
var pencarian_kedua = '<thead id ="head_cek">' +
'<th class="text-center">No</th>' +
'<th class="text-center">Date</th>' +
'<th class="text-center">Sequence</th>' +
'<th class="text-center">Notification Number</th>' +
'<th class="text-center">A/C Type</th>' +
'<th class="text-center">A/C Reg</th>' +
'<th class="text-center">Sta Dep</th>' +
'<th class="text-center">Sta Arr</th>' +
'<th class="text-center">Flight No' +
'<th class="text-center">ATA' +
'<th class="text-center">Sub ATA' +
'<th class="text-center">Problem' +
'<th class="text-center">Rectification' +
'<th class="text-center">Coding' +
'<th class="text-center">#' +
'</tr></thead>' +
'<tbody id="enakeun"></tbody>';
if (document.getElementById("radio_delay").checked) {
  $('#table_delay').append(pencarian_hasil);

  console.log(KeyProblem);
  var judul = 'Delay - ' + actype + date_from + ' - ' + date_to + acreg +  ata;
  var judul_delay = 'Delay - ' + actype + ' ' + acreg + ' ' + ata + ' ' + KeyProblem + ' ' + sub_ata;
  var table = $("#table_delay").DataTable({
   dom: '<"html5buttons"B>lTfgitp',
   buttons: [
   {extend: 'excel', title: judul},
   {extend: 'pdf', title: judul,pageSize: 'A0'},
   ],
   aLengthMenu : [[10, 25, 50, -1], [10, 25, 50, "All"]],
   ajax: {
    "url": "<?= base_url('Techlog/search') ?>",
    "type": "POST",
    "data": {
      "actype": actype,
      "acreg": acreg,
      "dateto": date_to,
      "datefrom": date_from,
      "ata": ata,
      "sub_ata": sub_ata,
      "keyword": keyword,
      "radio_delay" : radio_delay,
      "radio_pirep" : radio_pirep,
      "cl_delay": cl_delay,
      "cl_x": cl_x,
      "cl_cancel": cl_cancel,
      "KeyProblem" : KeyProblem,
      "filter"  : filter
    },
    complete: function (res) {
      $('#display').attr('disabled', false);
      $('#display').html('<i class="fa fa-print"></i>&nbsp;&nbsp;DISPLAY REPORT');
      var b = res.responseJSON;
      var c = b.x;
      var title = b.title;
      var d = b.y;
      var e = [];
      var f = [];
      $('#cl_delay').prop('checked', false);
      $('#cl_x').prop('checked', false);
      $('#cl_cancel').prop('checked', false);
      $('#cl_delay').val('');
      $('#cl_x').val('');
      $('#cl_cancel').val('');
      for (var i = 0; i <  c.length; i++) {
        e.push(c[i]);
        var j = Math.round(Number(d[i]));
        f.push(j);

      }
      Highcharts.chart('crut', {
        title: {
          text: judul_delay
        },

        subtitle: {
          text: 'Periode : ' + d_f + ' - ' + d_t
        },
        xAxis: {
          categories: e
        },
        yAxis: {
          title: {
            text: 'Number Of Occurrence'
          },
          showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
        }, 
        series: [ {
          name: '',
          data: f
        }],
        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
              }
            }
          }]
        }
      });
    }
  },
  'columns': [
  {
    data : 'no'
  },
  {
    data : 'notif'
  },
  {
    data: 'DateEvent'
  },
  {
    data: 'ACtype'
  },
  {
    data: 'Reg'
  },
  {
    data: 'DepSta'
  },   
  {
    data: 'ArivSta'
  },
  {
    data: 'FlightNo'
  },
  {
    data: 'total'
  },
  {
    data: 'ATAtdm'
  },
  {
    data: 'SubATAtdm'
  },
  {
    data: 'Problem'
  },
  {
    data: 'last',
  },
  {
    data: 'Rectification',

  },
  {
    data: 'DCP'
  }, 
  {
    data: 'RtABO'
  },
  {
    data: 'act'
  }
  ]
});
  $('body').addClass('mini-navbar');

} else if (document.getElementById("radio_pirep").checked) {
  $('#table_delay').append(pencarian_kedua);
  var judul = 'Pirep - ' + actype + date_from + ' - ' + date_to + acreg +  ata;
  var judul_pirep = 'Pirep - ' + actype + ' ' + acreg + ' ' + ata + ' ' + KeyProblem + ' ' + sub_ata;
  var table = $("#table_delay").DataTable({

   dom: '<"html5buttons"B>lTfgitp',
   buttons: [
   {extend: 'excel', title: judul},
   {extend: 'pdf', title: judul,pageSize: 'A0'},
   ],
   aLengthMenu : [[10, 25, 50, -1], [10, 25, 50, "All"]],

   ajax: {
    "url": "<?= base_url('Techlog/search2') ?>",
    "type": "POST",
    "data": {
      "actype": actype,
      "acreg": acreg,
      "dateto": date_to,
      "datefrom": date_from,
      "ata": ata,
      "sub_ata": sub_ata,
      "keyword": keyword,
      "radio_delay" : radio_delay,
      "radio_pirep" : radio_pirep,
      "dect"        : dect,
      "KeyProblem"  : KeyProblem,
      "filter"      : filter,
      "sj"          : sj,
      "un"          : un,
      "pirep"       : pirep,
      "marep"       : marep
    },
    complete: function (res) {
      $('#display').attr('disabled', false);
      $('#display').html('<i class="fa fa-print"></i>&nbsp;&nbsp;DISPLAY REPORT');
      var b = res.responseJSON;
      var c = b.x;
      var title = b.title;
      var d = b.y;
      var e = [];
      var f = [];
      $('#pirep').prop('checked', false);
      $('#marep').prop('checked', false);
      $('#un').prop('checked', false);
      $('#sj').prop('checked', false);
      $('#pirep').val('');
      $('#marep').val('');
      $('#un').val('');
      $('#sj').val('');
      for (var i = 0; i <  c.length; i++) {
        e.push(c[i]);
        var j = Math.round(Number(d[i]));
        f.push(j);

      }
      Highcharts.chart('crut', {
        title: {
          text: judul_pirep
        },

        subtitle: {
          text: 'Periode : ' + d_f + ' - ' + d_t
        },
        xAxis: {
          categories: e
        },
       yAxis: {
          title: {
            text: 'Number Of Occurrence'
          },
          showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
        },
        series: [ {
          name: 'Month',
          data: f
        }],

        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
              }
            }
          }]
        }

      });
    }

  },

  'columns': [{
    data: 'no'
  },
  {
    data: 'DATE'
  },
  {
    data: 'SEQ'
  },
  {
    data: 'Notification'
  },
  {
    data: 'ACTYPE'
  },   
  {
    data: 'REG'
  },
  {
    data: 'STADEP'
  },
  {
    data: 'STAARR'
  },
  {
    data: 'FN'
  },
  {
    data: 'ATA'
  },
  {
    data: 'SUB_ATA'
  },
  {
    data: 'PROBLEM'
  },
  {
    data: 'ACTION'
  }, 
  {
    data: 'PirepMarep'
  },   
  {
    data: 'act'
  }
  ]
});
  $('body').addClass('mini-navbar');

}

});
document.getElementById("radio_delay").checked = true;
check(document.getElementById("radio_delay").value);
function check(depir) {
  if(depir == "pirep"){
    document.getElementById("cl_delay").disabled = true;
    document.getElementById("cl_cancel").disabled = true;
    document.getElementById("cl_x").disabled = true;
    document.getElementById("pirep").disabled = false;
    document.getElementById("marep").disabled = false;
    document.getElementById("un").disabled = false;
    document.getElementById("sj").disabled = false;
    document.getElementById("filter").disabled = true;
    $('#cl_delay').prop('checked', false);
    $('#cl_x').prop('checked', false);
    $('#cl_cancel').prop('checked', false);
    $('#cl_delay').val('');
    $('#cl_x').val('');
    $('#cl_cancel').val('');
  }
  else{
    document.getElementById("pirep").disabled = true;
    document.getElementById("marep").disabled = true;
    document.getElementById("cl_delay").disabled = false;
    document.getElementById("cl_cancel").disabled = false;
    document.getElementById("cl_x").disabled = false;
    document.getElementById("un").disabled = true;
    document.getElementById("sj").disabled = true;
    document.getElementById("filter").disabled = false;
    $('#un').prop('checked', false);
    $('#sj').prop('checked', false);
    $('#pirep').prop('checked', false);
    $('#marep').prop('checked', false);
    $('#marep').val('');
    $('#un').val('');
    $('#sj').val('');
  } 
}
function atakeyword()
{
  var ata = $('#ata').val();
  $.ajax({
    type:"post",
    data:{'ata':ata},
    url:"<?= base_url('Search/ata') ?>",
    dataType: 'json',
    success: function(data){
     var ht = '<option value=""></option>'
     $('#KeyProblem').append(ht);
     for (var i = 0; i < data.length; i++) {
      var html = '<option value = "'+data[i]['ATA_DESC']+'">'+data[i]['ATA_DESC']+'</option>';
      $('#KeyProblem').append(html);
    } 
  },
});
}




  //confirm input form, if there is null in subject which must not null
</script>

