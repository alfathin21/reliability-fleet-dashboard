<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins" id="form_query">
        <div class="ibox-title">
          <h5><i class="fa fa-laptop"></i>&nbsp; Filter Pareto Removal </h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <div class="row">
           <div class="col-md-3">
            <label for="operator">OPERATOR <span style="color: red">*</span> :</label>
            <select onchange="io();" class="form-control" id="operator">
              <?php if ($unit == 'GA') : ?>
                <option value="GARUDA INDONESIA">GARUDA</option>
                <?php elseif ($unit == 'QG'): ?> 
                  <option value="CITILINK">CITILINK</option>
                  <?php elseif ($unit == 'SJ'): ?> 
                    <option value="SRIWIJAYA">SRIWIJAYA</option>
                    <?php elseif ($unit == 'IN'): ?> 
                      <option value="NAM">NAM AIR</option>
                      <?php else : ?>
                       <option value=""></option>
                       <option value="GARUDA INDONESIA">GARUDA</option>
                       <option value="CITILINK">CITILINK</option>
                       <option value="SRIWIJAYA">SRIWIJAYA</option>
                       <option value="NAM">NAM AIR</option>
                     <?php endif; ?>
                   </select>
                 </div>
                 <div class="col-md-2">
                  <label for="">A/C TYPE <span style="color: red">*</span> :</label>
                  <select onchange="ac();" multiple="" class="form-control" id="actype">
                   <option value=""></option>
                 </select>
               </div>
               <div class="col-md-2 form-group">
                <label for="">ATA :</label>    
                <select class="form-control" id="ata">
                 <option value=""></option>
                 <?php foreach ($ata as $key) :?>
                  <option value="<?= $key['ATA'] ?>"><?= $key['ATA'] ?></option>
                <?php endforeach; ?>
              </select> 
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">INLINE checkboxes</label>
              <div class="col-md-5 col-sm-5">
               <input type="checkbox" onclick="uy()"  id="u"> UNSCHEDULED</label> <label class="checkbox-inline">
                &nbsp;
                <input type="checkbox" onclick="cuy()" id="s">SCHEDULED </label> <label class="checkbox-inline">
                </div>
              </div>  
            </div>
            <br>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="">DATE FROM <span style="color: red">*</span> :</label>
                  <input type="text" id="date_from" class="datepicker-here form-control" data-language='en' data-date-format="yyyy-mm-dd" />
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="">DATE TO <span style="color: red">*</span> :</label>
                  <input type="text" id="date_to" class="datepicker-here form-control" data-language='en' data-date-format="yyyy-mm-dd" />
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <div class="form-group">
                    <label for="">TOP PARETO :</label>
                    <select class="form-control" id="filter">
                      <option value="5">5</option>
                      <option value="10">10</option>
                      <option value="20">20</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <br>
                <button type="button" id="display_pareto" class="btn btn-success"><i class="fa fa-print"></i>&nbsp;&nbsp;DISPLAY REPORT</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="ibox float-e-margins" id="form_query2">
          <div class="ibox-title">
            <h5><i class="fa fa-laptop"></i>&nbsp; TOP COMPONENT REMOVAL</h5>
            <div class="ibox-tools">
              <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
              </a>
            </div>
          </div>
          <div class="ibox-content inspinia-timeline table-responsive" id="sult">
           <div id="crot"></div>
         </div>
       </div>
     </div>
   </div>
   <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-bar-chart-o"></i>&nbsp; RESULT DETAIL</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline table-responsive" id="result">
          <div class="table-responsive" id="po">
           <table id="table_pareto" class="table table-bordered table-striped table-hovered"></table>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>

<!--  -->
<!-- Pirep Detail -->
<div class="container-fluid" id="form_query34">
  <div class="row">        
    <div class="col-xs-6 col-md-6 big-box" >
      <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content ">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">TOP COMPONENT DETAIL</h4>
            </div>
            <div class="modal-body"  style="overflow-y:auto; height:70vh;">  
              <div id="po">
                <table class="table table-fixed table-bordered table-responsive table-striped" id="comp_table"></table>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-sign-out"></i> Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--  -->

<!--  -->
<script>
$(document).on('keypress',function(e) {
    if(e.which == 13) {
         $('#crot').html("");
   if ($.fn.dataTable.isDataTable('#table_pareto')) {
    var table = $("#table_pareto").DataTable();
    table.destroy();
    $('#table_pareto').empty();
  }
  var actype  =  $('#actype').val();
  var date_to =  $('#date_to').val();
  var date_from = $('#date_from').val();
  var u = $('#u').val();
  var s = $('#s').val();
  var ata = $('#ata').val();
  var d_t = moment(date_to).format("DD MMMM YYYY");
  var d_f = moment(date_from).format("DD MMMM YYYY");
  var filter =  $('#filter').val();
  if (actype === "" || actype == null) {
    alert('Please select A/C Type !');
    return false;
  }
  if (date_to == '' || date_from == '') {
    alert('Please select Date From and Date To !');
    return false;
  }
  $('#display_pareto').attr('disabled', true);
  $('#display_pareto').html('Loading .. !');
  var pencarian_hasil = '<thead id ="head_cek">' +
  '<th width="10px;" class="text-center">No</th>' +
  '<th class="text-center">Code</th>' +
  '<th class="text-center">Part Name</th>' +
  '</tr></thead>' +
  '<tbody id="enakeun"></tbody>';

  $('#table_pareto').append(pencarian_hasil);
  var table = $("#table_pareto").DataTable({
    retrieve: true,
    pagin: false,
    dom: 'Bfrtip',
    buttons: [
    {extend: 'excel', title: 'REPORT TOP COMPONENT '},
    {extend: 'pdf', title: 'REPORT TOP COMPONENT ', orientation: 'landscape',
    pageSize: 'LETTER'}
    ],
    ajax: {
      "url": "<?= base_url('PComponent/search') ?>",
      "type": "POST",
      "data": {
        "actype": actype,
        "date_to": date_to,
        "date_from": date_from,
        "u" : u,
        "s" : s,
        "ata" : ata,
        "filter": filter
      },
      complete: function (res) {
       $('#display_pareto').attr('disabled', false);
       $('#display_pareto').html('<i class="fa fa-print"></i>&nbsp;&nbsp;DISPLAY REPORT');
       var jml =  res.responseJSON.jumlah; 
     
       var ojk = 'TOP COMPONENT REMOVAL - ' +  actype + ' ' + ata ; 
       if (jml == 20) {
        var part_0 = res.responseJSON.data[0].PartNo;
        var part_1 = res.responseJSON.data[1].PartNo;
        var part_2 = res.responseJSON.data[2].PartNo;
        var part_3 = res.responseJSON.data[3].PartNo;
        var part_4 = res.responseJSON.data[4].PartNo;
        var part_5 = res.responseJSON.data[5].PartNo;
        var part_6 = res.responseJSON.data[6].PartNo;
        var part_7 = res.responseJSON.data[7].PartNo;
        var part_8 = res.responseJSON.data[8].PartNo;
        var part_9 = res.responseJSON.data[9].PartNo;
        var part_10 = res.responseJSON.data[10].PartNo;
        var part_11 = res.responseJSON.data[11].PartNo;
        var part_12 = res.responseJSON.data[12].PartNo;
        var part_13 = res.responseJSON.data[13].PartNo;
        var part_14 = res.responseJSON.data[14].PartNo;
        var part_15 = res.responseJSON.data[15].PartNo;
        var part_16 = res.responseJSON.data[16].PartNo;
        var part_17 = res.responseJSON.data[17].PartNo;
        var part_18 = res.responseJSON.data[18].PartNo;
        var part_19 = res.responseJSON.data[19].PartNo;
        var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
        var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
        var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
        var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
        var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
        var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
        var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
        var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
        var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
        var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
        var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
        var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
        var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
        var hasil_part_13 = Number(res.responseJSON.data[13].number_of_part);
        var hasil_part_14 = Number(res.responseJSON.data[14].number_of_part);
        var hasil_part_15 = Number(res.responseJSON.data[15].number_of_part);
        var hasil_part_16 = Number(res.responseJSON.data[16].number_of_part);
        var hasil_part_17 = Number(res.responseJSON.data[17].number_of_part);
        var hasil_part_18 = Number(res.responseJSON.data[18].number_of_part);
        var hasil_part_19 = Number(res.responseJSON.data[19].number_of_part);
        var zr = [
        res.responseJSON.data[0].PartNo,
        res.responseJSON.data[1].PartNo,
        res.responseJSON.data[2].PartNo,
        res.responseJSON.data[3].PartNo,
        res.responseJSON.data[4].PartNo,
        res.responseJSON.data[5].PartNo,
        res.responseJSON.data[6].PartNo,
        res.responseJSON.data[7].PartNo,
        res.responseJSON.data[8].PartNo,
        res.responseJSON.data[9].PartNo,
        res.responseJSON.data[10].PartNo,
        res.responseJSON.data[11].PartNo,
        res.responseJSON.data[12].PartNo,
        res.responseJSON.data[13].PartNo,
        res.responseJSON.data[14].PartNo,
        res.responseJSON.data[15].PartNo,
        res.responseJSON.data[16].PartNo,
        res.responseJSON.data[17].PartNo,
        res.responseJSON.data[18].PartNo,
        res.responseJSON.data[19].PartNo
        ];

        Highcharts.chart('crot', {
          chart: {
            type: 'column'
          },
          title: {
            text: ojk
          },
          subtitle: {
            text: 'Periode : ' + d_f + ' - ' + d_t
          },
          xAxis: {
            type: 'category'
          },
          yAxis: {
            title: {
              text: 'Number Top Comp Removal'
            }

          },
          legend: {
            enabled: false
          },

          plotOptions: {
            series: {
              cursor: 'pointer',
              point: {
                events: {
                  click: function () {
                   $('#myModal2').modal('show');
                   if ($.fn.dataTable.isDataTable('#comp_table')) {
                    var table = $("#comp_table").DataTable();
                    table.destroy();
                    $('#comp_table').html("");
                  }
                  var category = this.category;
                  var dikirim = zr[category];
                  var pencarian_hasil = '<thead id ="sakj">' +
                  '<th class="text-center">No</th>' +
                  '<th class="text-center">Notification</th>' +
                  '<th class="text-center">ATA</th>' +
                  '<th class="text-center">Equipment</th>' +
                  '<th class="text-center">Part Number</th>' +
                  '<th class="text-center">Part Name</th>' +
                  '<th class="text-center">Serial Number</th>' +
                  '<th class="text-center">Register</th>' +
                  '<th class="text-center">A/C Type</th>' +
                  '<th class="text-center">RemCode</th>' +
                  '<th class="text-center">Real Reason</th>' +
                  '<th class="text-center">Date Removal</th>' +
                  '<th class="text-center">#</th>' +
                  '</tr></thead>' +
                  '<tbody id="enakeun"></tbody>';
                  $('#comp_table').append(pencarian_hasil);
                  var table = $("#comp_table").DataTable({
                    retrieve: true,
                    pagin: false,
                    dom: 'Bfrtip',
                    buttons: [
                    {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
                      columns: [0,1,2,3,4,5,6,7,8,9,10,11]
                    }
                  },
                  {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
                  pageSize: 'LETTER',exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,9,10,11]
                  }}
                  ],
                  ajax: {
                    "url": "<?= base_url('PComponent/CompDetail') ?>",
                    "type": "POST",
                    "data": {
                      "actype": actype,
                      "date_from": date_from,
                      "date_to": date_to,
                      "u": u,
                      "s": s,
                      "dikirim": dikirim,
                      "ata" : ata
                    },
                  },

                  'columns': [
                  {
                    data: 'no'
                  },
                  {
                    data: 'ID'
                  },
                  {
                    data: 'ATA'
                  },
                  {
                    data: 'AIN'
                  },
                  {
                    data: 'PartNo'
                  },
                  {
                    data: 'PartName'
                  },
                  {
                    data: 'SerialNo'
                  },
                  {
                    data: 'Reg'
                  },
                  {
                    data: 'Aircraft'
                  },
                  {
                    data: 'RemCode'
                  },
                  {
                    data: 'real_reason'
                  },
                  {
                    data: 'DateRem'
                  },
                  {
                    data: 'act'
                  },

                  ]
                });
                }
              }
            }
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
        },

        series: [
        {
          name: "PART NO",
          colorByPoint: true,
          data: [
          {
            name: part_0,
            y: hasil_part_0

          },
          {
            name: part_1,
            y: hasil_part_1

          },
          {
            name: part_2,
            y: hasil_part_2
          },
          {
            name: part_3,
            y: hasil_part_3
          },
          {
            name: part_4,
            y: hasil_part_4
          },
          {
            name: part_5,
            y: hasil_part_5
          },
          {
            name: part_6,
            y: hasil_part_6
          },
          {
            name: part_7,
            y: hasil_part_7
          },
          {
            name: part_8,
            y: hasil_part_8
          },
          {
            name: part_9,
            y: hasil_part_9
          },
          {
            name: part_10,
            y: hasil_part_10
          },
          {
            name: part_11,
            y: hasil_part_11
          },
          {
            name: part_12,
            y: hasil_part_12
          },
          {
            name: part_13,
            y: hasil_part_13
          },
          {
            name: part_14,
            y: hasil_part_14
          },
          {
            name: part_15,
            y: hasil_part_15
          },
          {
            name: part_16,
            y: hasil_part_16
          },
          {
            name: part_17,
            y: hasil_part_17
          },
          {
            name: part_18,
            y: hasil_part_18
          },
          {
            name: part_19,
            y: hasil_part_19

          }
          ]
        }
        ]
      });
}
else if (jml == 19) {
  var part_0 = res.responseJSON.data[0].PartNo;
  var part_1 = res.responseJSON.data[1].PartNo;
  var part_2 = res.responseJSON.data[2].PartNo;
  var part_3 = res.responseJSON.data[3].PartNo;
  var part_4 = res.responseJSON.data[4].PartNo;
  var part_5 = res.responseJSON.data[5].PartNo;
  var part_6 = res.responseJSON.data[6].PartNo;
  var part_7 = res.responseJSON.data[7].PartNo;
  var part_8 = res.responseJSON.data[8].PartNo;
  var part_9 = res.responseJSON.data[9].PartNo;
  var part_10 = res.responseJSON.data[10].PartNo;
  var part_11 = res.responseJSON.data[11].PartNo;
  var part_12 = res.responseJSON.data[12].PartNo;
  var part_13 = res.responseJSON.data[13].PartNo;
  var part_14 = res.responseJSON.data[14].PartNo;
  var part_15 = res.responseJSON.data[15].PartNo;
  var part_16 = res.responseJSON.data[16].PartNo;
  var part_17 = res.responseJSON.data[17].PartNo;
  var part_18 = res.responseJSON.data[18].PartNo;
  var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
  var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
  var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
  var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
  var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
  var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
  var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
  var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
  var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
  var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
  var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
  var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
  var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
  var hasil_part_13 = Number(res.responseJSON.data[13].number_of_part);
  var hasil_part_14 = Number(res.responseJSON.data[14].number_of_part);
  var hasil_part_15 = Number(res.responseJSON.data[15].number_of_part);
  var hasil_part_16 = Number(res.responseJSON.data[16].number_of_part);
  var hasil_part_17 = Number(res.responseJSON.data[17].number_of_part);
  var hasil_part_18 = Number(res.responseJSON.data[18].number_of_part);
  var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo,
  res.responseJSON.data[3].PartNo,
  res.responseJSON.data[4].PartNo,
  res.responseJSON.data[5].PartNo,
  res.responseJSON.data[6].PartNo,
  res.responseJSON.data[7].PartNo,
  res.responseJSON.data[8].PartNo,
  res.responseJSON.data[9].PartNo,
  res.responseJSON.data[10].PartNo,
  res.responseJSON.data[11].PartNo,
  res.responseJSON.data[12].PartNo,
  res.responseJSON.data[13].PartNo,
  res.responseJSON.data[14].PartNo,
  res.responseJSON.data[15].PartNo,
  res.responseJSON.data[16].PartNo,
  res.responseJSON.data[17].PartNo,
  res.responseJSON.data[18].PartNo
  ];

  Highcharts.chart('crot', {
    chart: {
      type: 'column'
    },
    title: {
      text: ojk
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number Top Comp Removal'
      }

    },
    legend: {
      enabled: false
    },

    plotOptions: {
      series: {
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal2').modal('show');
             if ($.fn.dataTable.isDataTable('#comp_table')) {
              var table = $("#comp_table").DataTable();
              table.destroy();
              $('#comp_table').html("");
            }
            var category = this.category;
            var dikirim = zr[category];
            var pencarian_hasil = '<thead id ="sakj">' +
            '<th class="text-center">No</th>' +
            '<th class="text-center">Notification</th>' +
            '<th class="text-center">ATA</th>' +
            '<th class="text-center">Equipment</th>' +
            '<th class="text-center">Part Number</th>' +
            '<th class="text-center">Part Name</th>' +
            '<th class="text-center">Serial Number</th>' +
            '<th class="text-center">Register</th>' +
            '<th class="text-center">A/C Type</th>' +
            '<th class="text-center">RemCode</th>' +
            '<th class="text-center">Real Reason</th>' +
            '<th class="text-center">Date Removal</th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead>' +
            '<tbody id="enakeun"></tbody>';
            $('#comp_table').append(pencarian_hasil);
            var table = $("#comp_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9,10,11]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
            pageSize: 'LETTER',exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }}
            ],
            ajax: {
              "url": "<?= base_url('PComponent/CompDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "date_from": date_from,
                "date_to": date_to,
                "u": u,
                "s": s,
                "dikirim": dikirim,
                "ata" : ata
              },
            },

            'columns': [
            {
              data: 'no'
            },
            {
              data: 'ID'
            },
            {
              data: 'ATA'
            },
            {
              data: 'AIN'
            },
            {
              data: 'PartNo'
            },
            {
              data: 'PartName'
            },
            {
              data: 'SerialNo'
            },
            {
              data: 'Reg'
            },
            {
              data: 'Aircraft'
            },
            {
              data: 'RemCode'
            },
            {
              data: 'real_reason'
            },
            {
              data: 'DateRem'
            },
            {
              data: 'act'
            },

            ]
          });
          }
        }
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },

  series: [
  {
    name: "PART NO",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_8,
      y: hasil_part_8
    },
    {
      name: part_9,
      y: hasil_part_9
    },
    {
      name: part_10,
      y: hasil_part_10
    },
    {
      name: part_11,
      y: hasil_part_11
    },
    {
      name: part_12,
      y: hasil_part_12
    },
    {
      name: part_13,
      y: hasil_part_13
    },
    {
      name: part_14,
      y: hasil_part_14
    },
    {
      name: part_15,
      y: hasil_part_15
    },
    {
      name: part_16,
      y: hasil_part_16
    },
    {
      name: part_17,
      y: hasil_part_17
    },
    {
      name: part_18,
      y: hasil_part_18
    }
    ]
  }
  ]
});
}
else if (jml == 18) {
  var part_0 = res.responseJSON.data[0].PartNo;
  var part_1 = res.responseJSON.data[1].PartNo;
  var part_2 = res.responseJSON.data[2].PartNo;
  var part_3 = res.responseJSON.data[3].PartNo;
  var part_4 = res.responseJSON.data[4].PartNo;
  var part_5 = res.responseJSON.data[5].PartNo;
  var part_6 = res.responseJSON.data[6].PartNo;
  var part_7 = res.responseJSON.data[7].PartNo;
  var part_8 = res.responseJSON.data[8].PartNo;
  var part_9 = res.responseJSON.data[9].PartNo;
  var part_10 = res.responseJSON.data[10].PartNo;
  var part_11 = res.responseJSON.data[11].PartNo;
  var part_12 = res.responseJSON.data[12].PartNo;
  var part_13 = res.responseJSON.data[13].PartNo;
  var part_14 = res.responseJSON.data[14].PartNo;
  var part_15 = res.responseJSON.data[15].PartNo;
  var part_16 = res.responseJSON.data[16].PartNo;
  var part_17 = res.responseJSON.data[17].PartNo;
  var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
  var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
  var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
  var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
  var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
  var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
  var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
  var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
  var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
  var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
  var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
  var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
  var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
  var hasil_part_13 = Number(res.responseJSON.data[13].number_of_part);
  var hasil_part_14 = Number(res.responseJSON.data[14].number_of_part);
  var hasil_part_15 = Number(res.responseJSON.data[15].number_of_part);
  var hasil_part_16 = Number(res.responseJSON.data[16].number_of_part);
  var hasil_part_17 = Number(res.responseJSON.data[17].number_of_part);
  var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo,
  res.responseJSON.data[3].PartNo,
  res.responseJSON.data[4].PartNo,
  res.responseJSON.data[5].PartNo,
  res.responseJSON.data[6].PartNo,
  res.responseJSON.data[7].PartNo,
  res.responseJSON.data[8].PartNo,
  res.responseJSON.data[9].PartNo,
  res.responseJSON.data[10].PartNo,
  res.responseJSON.data[11].PartNo,
  res.responseJSON.data[12].PartNo,
  res.responseJSON.data[13].PartNo,
  res.responseJSON.data[14].PartNo,
  res.responseJSON.data[15].PartNo,
  res.responseJSON.data[16].PartNo,
  res.responseJSON.data[17].PartNo
  ];

  Highcharts.chart('crot', {
    chart: {
      type: 'column'
    },
    title: {
      text: ojk
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number Top Comp Removal'
      }

    },
    legend: {
      enabled: false
    },

    plotOptions: {
      series: {
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal2').modal('show');
             if ($.fn.dataTable.isDataTable('#comp_table')) {
              var table = $("#comp_table").DataTable();
              table.destroy();
              $('#comp_table').html("");
            }
            var category = this.category;
            var dikirim = zr[category];
            var pencarian_hasil = '<thead id ="sakj">' +
            '<th class="text-center">No</th>' +
            '<th class="text-center">Notification</th>' +
            '<th class="text-center">ATA</th>' +
            '<th class="text-center">Equipment</th>' +
            '<th class="text-center">Part Number</th>' +
            '<th class="text-center">Part Name</th>' +
            '<th class="text-center">Serial Number</th>' +
            '<th class="text-center">Register</th>' +
            '<th class="text-center">A/C Type</th>' +
            '<th class="text-center">RemCode</th>' +
            '<th class="text-center">Real Reason</th>' +
            '<th class="text-center">Date Removal</th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead>' +
            '<tbody id="enakeun"></tbody>';
            $('#comp_table').append(pencarian_hasil);
            var table = $("#comp_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9,10,11]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
            pageSize: 'LETTER',exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }}
            ],
            ajax: {
              "url": "<?= base_url('PComponent/CompDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "date_from": date_from,
                "date_to": date_to,
                "u": u,
                "s": s,
                "dikirim": dikirim,
                "ata" : ata
              },
            },

            'columns': [
            {
              data: 'no'
            },
            {
              data: 'ID'
            },
            {
              data: 'ATA'
            },
            {
              data: 'AIN'
            },
            {
              data: 'PartNo'
            },
            {
              data: 'PartName'
            },
            {
              data: 'SerialNo'
            },
            {
              data: 'Reg'
            },
            {
              data: 'Aircraft'
            },
            {
              data: 'RemCode'
            },
            {
              data: 'real_reason'
            },
            {
              data: 'DateRem'
            },
            {
              data: 'act'
            },

            ]
          });
          }
        }
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },

  series: [
  {
    name: "PART NO",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_8,
      y: hasil_part_8
    },
    {
      name: part_9,
      y: hasil_part_9
    },
    {
      name: part_10,
      y: hasil_part_10
    },
    {
      name: part_11,
      y: hasil_part_11
    },
    {
      name: part_12,
      y: hasil_part_12
    },
    {
      name: part_13,
      y: hasil_part_13
    },
    {
      name: part_14,
      y: hasil_part_14
    },
    {
      name: part_15,
      y: hasil_part_15
    },
    {
      name: part_16,
      y: hasil_part_16
    },
    {
      name: part_17,
      y: hasil_part_17
    }
    ]
  }
  ]
});

}
else if (jml == 17) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var part_10 = res.responseJSON.data[10].PartNo;
 var part_11 = res.responseJSON.data[11].PartNo;
 var part_12 = res.responseJSON.data[12].PartNo;
 var part_13 = res.responseJSON.data[13].PartNo;
 var part_14 = res.responseJSON.data[14].PartNo;
 var part_15 = res.responseJSON.data[15].PartNo;
 var part_16 = res.responseJSON.data[16].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
 var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
 var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
 var hasil_part_13 = Number(res.responseJSON.data[13].number_of_part);
 var hasil_part_14 = Number(res.responseJSON.data[14].number_of_part);
 var hasil_part_15 = Number(res.responseJSON.data[15].number_of_part);
 var hasil_part_16 = Number(res.responseJSON.data[16].number_of_part);
 var zr = [
 res.responseJSON.data[0].PartNo,
 res.responseJSON.data[1].PartNo,
 res.responseJSON.data[2].PartNo,
 res.responseJSON.data[3].PartNo,
 res.responseJSON.data[4].PartNo,
 res.responseJSON.data[5].PartNo,
 res.responseJSON.data[6].PartNo,
 res.responseJSON.data[7].PartNo,
 res.responseJSON.data[8].PartNo,
 res.responseJSON.data[9].PartNo,
 res.responseJSON.data[10].PartNo,
 res.responseJSON.data[11].PartNo,
 res.responseJSON.data[12].PartNo,
 res.responseJSON.data[13].PartNo,
 res.responseJSON.data[14].PartNo,
 res.responseJSON.data[15].PartNo,
 res.responseJSON.data[16].PartNo
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  }
  ]
}
]
});

}
else if (jml == 16) {
  var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var part_10 = res.responseJSON.data[10].PartNo;
 var part_11 = res.responseJSON.data[11].PartNo;
 var part_12 = res.responseJSON.data[12].PartNo;
 var part_13 = res.responseJSON.data[13].PartNo;
 var part_14 = res.responseJSON.data[14].PartNo;
 var part_15 = res.responseJSON.data[15].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
 var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
 var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
 var hasil_part_13 = Number(res.responseJSON.data[13].number_of_part);
 var hasil_part_14 = Number(res.responseJSON.data[14].number_of_part);
 var hasil_part_15 = Number(res.responseJSON.data[15].number_of_part);
 var zr = [
 res.responseJSON.data[0].PartNo,
 res.responseJSON.data[1].PartNo,
 res.responseJSON.data[2].PartNo,
 res.responseJSON.data[3].PartNo,
 res.responseJSON.data[4].PartNo,
 res.responseJSON.data[5].PartNo,
 res.responseJSON.data[6].PartNo,
 res.responseJSON.data[7].PartNo,
 res.responseJSON.data[8].PartNo,
 res.responseJSON.data[9].PartNo,
 res.responseJSON.data[10].PartNo,
 res.responseJSON.data[11].PartNo,
 res.responseJSON.data[12].PartNo,
 res.responseJSON.data[13].PartNo,
 res.responseJSON.data[14].PartNo,
 res.responseJSON.data[15].PartNo
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  }
  ]
}
]
});

}
else if (jml == 15) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var part_10 = res.responseJSON.data[10].PartNo;
 var part_11 = res.responseJSON.data[11].PartNo;
 var part_12 = res.responseJSON.data[12].PartNo;
 var part_13 = res.responseJSON.data[13].PartNo;
 var part_14 = res.responseJSON.data[14].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
 var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
 var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
 var hasil_part_13 = Number(res.responseJSON.data[13].number_of_part);
 var hasil_part_14 = Number(res.responseJSON.data[14].number_of_part);
 var zr = [
 res.responseJSON.data[0].PartNo,
 res.responseJSON.data[1].PartNo,
 res.responseJSON.data[2].PartNo,
 res.responseJSON.data[3].PartNo,
 res.responseJSON.data[4].PartNo,
 res.responseJSON.data[5].PartNo,
 res.responseJSON.data[6].PartNo,
 res.responseJSON.data[7].PartNo,
 res.responseJSON.data[8].PartNo,
 res.responseJSON.data[9].PartNo,
 res.responseJSON.data[10].PartNo,
 res.responseJSON.data[11].PartNo,
 res.responseJSON.data[12].PartNo,
 res.responseJSON.data[13].PartNo,
 res.responseJSON.data[14].PartNo
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  }
  ]
}
]
});

}
else if (jml == 14) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var part_10 = res.responseJSON.data[10].PartNo;
 var part_11 = res.responseJSON.data[11].PartNo;
 var part_12 = res.responseJSON.data[12].PartNo;
 var part_13 = res.responseJSON.data[13].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
 var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
 var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
 var hasil_part_13 = Number(res.responseJSON.data[13].number_of_part);
 var zr = [
 res.responseJSON.data[0].PartNo,
 res.responseJSON.data[1].PartNo,
 res.responseJSON.data[2].PartNo,
 res.responseJSON.data[3].PartNo,
 res.responseJSON.data[4].PartNo,
 res.responseJSON.data[5].PartNo,
 res.responseJSON.data[6].PartNo,
 res.responseJSON.data[7].PartNo,
 res.responseJSON.data[8].PartNo,
 res.responseJSON.data[9].PartNo,
 res.responseJSON.data[10].PartNo,
 res.responseJSON.data[11].PartNo,
 res.responseJSON.data[12].PartNo,
 res.responseJSON.data[13].PartNo
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  }
  ]
}
]
});



}
else if (jml == 13) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var part_10 = res.responseJSON.data[10].PartNo;
 var part_11 = res.responseJSON.data[11].PartNo;
 var part_12 = res.responseJSON.data[12].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
 var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
 var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
 var zr = [
 res.responseJSON.data[0].PartNo,
 res.responseJSON.data[1].PartNo,
 res.responseJSON.data[2].PartNo,
 res.responseJSON.data[3].PartNo,
 res.responseJSON.data[4].PartNo,
 res.responseJSON.data[5].PartNo,
 res.responseJSON.data[6].PartNo,
 res.responseJSON.data[7].PartNo,
 res.responseJSON.data[8].PartNo,
 res.responseJSON.data[9].PartNo,
 res.responseJSON.data[10].PartNo,
 res.responseJSON.data[11].PartNo,
 res.responseJSON.data[12].PartNo
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  }
  ]
}
]
});

}
else if (jml == 12) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var part_10 = res.responseJSON.data[10].PartNo;
 var part_11 = res.responseJSON.data[11].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
 var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
 var zr = [
 res.responseJSON.data[0].PartNo,
 res.responseJSON.data[1].PartNo,
 res.responseJSON.data[2].PartNo,
 res.responseJSON.data[3].PartNo,
 res.responseJSON.data[4].PartNo,
 res.responseJSON.data[5].PartNo,
 res.responseJSON.data[6].PartNo,
 res.responseJSON.data[7].PartNo,
 res.responseJSON.data[8].PartNo,
 res.responseJSON.data[9].PartNo,
 res.responseJSON.data[10].PartNo,
 res.responseJSON.data[11].PartNo
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  }
  ]
}
]
});

}
else if (jml == 11) {
  var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var part_10 = res.responseJSON.data[10].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
 var zr = [
 res.responseJSON.data[0].PartNo,
 res.responseJSON.data[1].PartNo,
 res.responseJSON.data[2].PartNo,
 res.responseJSON.data[3].PartNo,
 res.responseJSON.data[4].PartNo,
 res.responseJSON.data[5].PartNo,
 res.responseJSON.data[6].PartNo,
 res.responseJSON.data[7].PartNo,
 res.responseJSON.data[8].PartNo,
 res.responseJSON.data[9].PartNo,
 res.responseJSON.data[10].PartNo
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  }
  ]
}
]
});

}
else if (jml ==  10) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var zr = [res.responseJSON.data[0].PartNo,res.responseJSON.data[1].PartNo,res.responseJSON.data[2].PartNo,res.responseJSON.data[3].PartNo,res.responseJSON.data[4].PartNo,res.responseJSON.data[5].PartNo,res.responseJSON.data[6].PartNo,res.responseJSON.data[7].PartNo,res.responseJSON.data[8].PartNo,res.responseJSON.data[9].PartNo];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  }
  ]
}
]

});
} else if (jml == 9) {
 var part_0     = res.responseJSON.data[0].PartNo;
 var part_1      = res.responseJSON.data[1].PartNo;
 var part_2     = res.responseJSON.data[2].PartNo;
 var part_3     = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
             $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL'
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  }
  ]
}
]



});
} else if (jml == 8) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;

 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);


   var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo,
  res.responseJSON.data[3].PartNo,
  res.responseJSON.data[4].PartNo,
  res.responseJSON.data[5].PartNo,
  res.responseJSON.data[6].PartNo,
  res.responseJSON.data[7].PartNo
  ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
             $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  }
  ]
}
]



});
} else if (jml == 7) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;

 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
    var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo,
  res.responseJSON.data[3].PartNo,
  res.responseJSON.data[4].PartNo,
  res.responseJSON.data[5].PartNo,
  res.responseJSON.data[6].PartNo
  ];


 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
             $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  }
  ]
}
]

});
} else if (jml == 6) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;

 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
  var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo,
  res.responseJSON.data[3].PartNo,
  res.responseJSON.data[4].PartNo,
  res.responseJSON.data[5].PartNo
  ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
             $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  }
  ]
}
]



});
} else if (jml == 5) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
   var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo,
  res.responseJSON.data[3].PartNo,
  res.responseJSON.data[4].PartNo
  ];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
             $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];


          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  }
  ]
}
]
});
} else if (jml ==  4) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);

    var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo,
  res.responseJSON.data[3].PartNo
  ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
             $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  }
  ]
}
]
});
} else if (jml ==  3) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;

 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);


    var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo
  ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
             $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  }
  ]
}
]
});
} else if (jml == 2) {
  var part_0 = res.responseJSON.data[0].PartNo;
  var part_1 = res.responseJSON.data[1].PartNo;

  var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
  var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);

      var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo
  ];


  Highcharts.chart('crot', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP 10 COMPONENT REMOVAL'
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number Top Comp Removal'
      }

    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal2').modal('show');
             if ($.fn.dataTable.isDataTable('#comp_table')) {
              var table = $("#comp_table").DataTable();
              table.destroy();
               $('#comp_table').html("");
            }
            var category = this.category;
            var dikirim = zr[category];
            var pencarian_hasil = '<thead id ="sakj">' +
            '<th class="text-center">No</th>' +
            '<th class="text-center">Notification</th>' +
            '<th class="text-center">ATA</th>' +
            '<th class="text-center">Equipment</th>' +
            '<th class="text-center">Part Number</th>' +
            '<th class="text-center">Part Name</th>' +
            '<th class="text-center">Serial Number</th>' +
            '<th class="text-center">Register</th>' +
            '<th class="text-center">A/C Type</th>' +
            '<th class="text-center">RemCode</th>' +
            '<th class="text-center">Real Reason</th>' +
            '<th class="text-center">Date Removal</th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead>' +
            '<tbody id="enakeun"></tbody>';
            $('#comp_table').append(pencarian_hasil);
            var table = $("#comp_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9,10,11]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
            pageSize: 'LETTER',exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }}
            ],
            ajax: {
              "url": "<?= base_url('PComponent/CompDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "date_from": date_from,
                "date_to": date_to,
                "u": u,
                "s": s,
                "dikirim": dikirim,
                "ata" : ata
              },
            },

            'columns': [
            {
              data: 'no'
            },
            {
              data: 'ID'
            },
            {
              data: 'ATA'
            },
            {
              data: 'AIN'
            },
            {
              data: 'PartNo'
            },
            {
              data: 'PartName'
            },
            {
              data: 'SerialNo'
            },
            {
              data: 'Reg'
            },
            {
              data: 'Aircraft'
            },
            {
              data: 'RemCode'
            },
            {
              data: 'real_reason'
            },
            {
              data: 'DateRem'
            },
            {
              data: 'act'
            },
            ]
          });
          }
        }
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },

  series: [
  {
    name: "PART NO",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    }
    ]
  }
  ]

});
} else if (jml == 1) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);

       var zr = [
  res.responseJSON.data[0].PartNo
  ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  }
  ]
}
]



});
}

}

},
'columns': [{
  data: 'no'
},
{
  data: 'PartNo'
},
{
  data: 'PartName'
}
]
},

);

$('body').addClass('mini-navbar');
    }
});

  $('#display_pareto').on('click', function () {
   $('#crot').html("");
   if ($.fn.dataTable.isDataTable('#table_pareto')) {
    var table = $("#table_pareto").DataTable();
    table.destroy();
    $('#table_pareto').empty();
  }
  var actype  =  $('#actype').val();
  var date_to =  $('#date_to').val();
  var date_from = $('#date_from').val();
  var u = $('#u').val();
  var s = $('#s').val();
  var ata = $('#ata').val();
  var d_t = moment(date_to).format("DD MMMM YYYY");
  var d_f = moment(date_from).format("DD MMMM YYYY");
  var filter =  $('#filter').val();
  if (actype === "" || actype == null) {
    alert('Please select A/C Type !');
    return false;
  }
  if (date_to == '' || date_from == '') {
    alert('Please select Date From and Date To !');
    return false;
  }
  $('#display_pareto').attr('disabled', true);
  $('#display_pareto').html('Loading .. !');
  var pencarian_hasil = '<thead id ="head_cek">' +
  '<th width="10px;" class="text-center">No</th>' +
  '<th class="text-center">Code</th>' +
  '<th class="text-center">Part Name</th>' +
  '</tr></thead>' +
  '<tbody id="enakeun"></tbody>';

  $('#table_pareto').append(pencarian_hasil);
  var table = $("#table_pareto").DataTable({
    retrieve: true,
    pagin: false,
    dom: 'Bfrtip',
    buttons: [
    {extend: 'excel', title: 'REPORT TOP COMPONENT '},
    {extend: 'pdf', title: 'REPORT TOP COMPONENT ', orientation: 'landscape',
    pageSize: 'LETTER'}
    ],
    ajax: {
      "url": "<?= base_url('PComponent/search') ?>",
      "type": "POST",
      "data": {
        "actype": actype,
        "date_to": date_to,
        "date_from": date_from,
        "u" : u,
        "s" : s,
        "ata" : ata,
        "filter": filter
      },
      complete: function (res) {
       $('#display_pareto').attr('disabled', false);
       $('#display_pareto').html('<i class="fa fa-print"></i>&nbsp;&nbsp;DISPLAY REPORT');
       var jml =  res.responseJSON.jumlah; 
     
       var ojk = 'TOP COMPONENT REMOVAL - ' +  actype + ' ' + ata ; 
       if (jml == 20) {
        var part_0 = res.responseJSON.data[0].PartNo;
        var part_1 = res.responseJSON.data[1].PartNo;
        var part_2 = res.responseJSON.data[2].PartNo;
        var part_3 = res.responseJSON.data[3].PartNo;
        var part_4 = res.responseJSON.data[4].PartNo;
        var part_5 = res.responseJSON.data[5].PartNo;
        var part_6 = res.responseJSON.data[6].PartNo;
        var part_7 = res.responseJSON.data[7].PartNo;
        var part_8 = res.responseJSON.data[8].PartNo;
        var part_9 = res.responseJSON.data[9].PartNo;
        var part_10 = res.responseJSON.data[10].PartNo;
        var part_11 = res.responseJSON.data[11].PartNo;
        var part_12 = res.responseJSON.data[12].PartNo;
        var part_13 = res.responseJSON.data[13].PartNo;
        var part_14 = res.responseJSON.data[14].PartNo;
        var part_15 = res.responseJSON.data[15].PartNo;
        var part_16 = res.responseJSON.data[16].PartNo;
        var part_17 = res.responseJSON.data[17].PartNo;
        var part_18 = res.responseJSON.data[18].PartNo;
        var part_19 = res.responseJSON.data[19].PartNo;
        var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
        var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
        var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
        var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
        var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
        var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
        var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
        var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
        var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
        var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
        var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
        var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
        var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
        var hasil_part_13 = Number(res.responseJSON.data[13].number_of_part);
        var hasil_part_14 = Number(res.responseJSON.data[14].number_of_part);
        var hasil_part_15 = Number(res.responseJSON.data[15].number_of_part);
        var hasil_part_16 = Number(res.responseJSON.data[16].number_of_part);
        var hasil_part_17 = Number(res.responseJSON.data[17].number_of_part);
        var hasil_part_18 = Number(res.responseJSON.data[18].number_of_part);
        var hasil_part_19 = Number(res.responseJSON.data[19].number_of_part);
        var zr = [
        res.responseJSON.data[0].PartNo,
        res.responseJSON.data[1].PartNo,
        res.responseJSON.data[2].PartNo,
        res.responseJSON.data[3].PartNo,
        res.responseJSON.data[4].PartNo,
        res.responseJSON.data[5].PartNo,
        res.responseJSON.data[6].PartNo,
        res.responseJSON.data[7].PartNo,
        res.responseJSON.data[8].PartNo,
        res.responseJSON.data[9].PartNo,
        res.responseJSON.data[10].PartNo,
        res.responseJSON.data[11].PartNo,
        res.responseJSON.data[12].PartNo,
        res.responseJSON.data[13].PartNo,
        res.responseJSON.data[14].PartNo,
        res.responseJSON.data[15].PartNo,
        res.responseJSON.data[16].PartNo,
        res.responseJSON.data[17].PartNo,
        res.responseJSON.data[18].PartNo,
        res.responseJSON.data[19].PartNo
        ];

        Highcharts.chart('crot', {
          chart: {
            type: 'column'
          },
          title: {
            text: ojk
          },
          subtitle: {
            text: 'Periode : ' + d_f + ' - ' + d_t
          },
          xAxis: {
            type: 'category'
          },
          yAxis: {
            title: {
              text: 'Number Top Comp Removal'
            }

          },
          legend: {
            enabled: false
          },

          plotOptions: {
            series: {
              cursor: 'pointer',
              point: {
                events: {
                  click: function () {
                   $('#myModal2').modal('show');
                   if ($.fn.dataTable.isDataTable('#comp_table')) {
                    var table = $("#comp_table").DataTable();
                    table.destroy();
                    $('#comp_table').html("");
                  }
                  var category = this.category;
                  var dikirim = zr[category];
                  var pencarian_hasil = '<thead id ="sakj">' +
                  '<th class="text-center">No</th>' +
                  '<th class="text-center">Notification</th>' +
                  '<th class="text-center">ATA</th>' +
                  '<th class="text-center">Equipment</th>' +
                  '<th class="text-center">Part Number</th>' +
                  '<th class="text-center">Part Name</th>' +
                  '<th class="text-center">Serial Number</th>' +
                  '<th class="text-center">Register</th>' +
                  '<th class="text-center">A/C Type</th>' +
                  '<th class="text-center">RemCode</th>' +
                  '<th class="text-center">Real Reason</th>' +
                  '<th class="text-center">Date Removal</th>' +
                  '<th class="text-center">#</th>' +
                  '</tr></thead>' +
                  '<tbody id="enakeun"></tbody>';
                  $('#comp_table').append(pencarian_hasil);
                  var table = $("#comp_table").DataTable({
                    retrieve: true,
                    pagin: false,
                    dom: 'Bfrtip',
                    buttons: [
                    {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
                      columns: [0,1,2,3,4,5,6,7,8,9,10,11]
                    }
                  },
                  {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
                  pageSize: 'LETTER',exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,9,10,11]
                  }}
                  ],
                  ajax: {
                    "url": "<?= base_url('PComponent/CompDetail') ?>",
                    "type": "POST",
                    "data": {
                      "actype": actype,
                      "date_from": date_from,
                      "date_to": date_to,
                      "u": u,
                      "s": s,
                      "dikirim": dikirim,
                      "ata" : ata
                    },
                  },

                  'columns': [
                  {
                    data: 'no'
                  },
                  {
                    data: 'ID'
                  },
                  {
                    data: 'ATA'
                  },
                  {
                    data: 'AIN'
                  },
                  {
                    data: 'PartNo'
                  },
                  {
                    data: 'PartName'
                  },
                  {
                    data: 'SerialNo'
                  },
                  {
                    data: 'Reg'
                  },
                  {
                    data: 'Aircraft'
                  },
                  {
                    data: 'RemCode'
                  },
                  {
                    data: 'real_reason'
                  },
                  {
                    data: 'DateRem'
                  },
                  {
                    data: 'act'
                  },

                  ]
                });
                }
              }
            }
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
        },

        series: [
        {
          name: "PART NO",
          colorByPoint: true,
          data: [
          {
            name: part_0,
            y: hasil_part_0

          },
          {
            name: part_1,
            y: hasil_part_1

          },
          {
            name: part_2,
            y: hasil_part_2
          },
          {
            name: part_3,
            y: hasil_part_3
          },
          {
            name: part_4,
            y: hasil_part_4
          },
          {
            name: part_5,
            y: hasil_part_5
          },
          {
            name: part_6,
            y: hasil_part_6
          },
          {
            name: part_7,
            y: hasil_part_7
          },
          {
            name: part_8,
            y: hasil_part_8
          },
          {
            name: part_9,
            y: hasil_part_9
          },
          {
            name: part_10,
            y: hasil_part_10
          },
          {
            name: part_11,
            y: hasil_part_11
          },
          {
            name: part_12,
            y: hasil_part_12
          },
          {
            name: part_13,
            y: hasil_part_13
          },
          {
            name: part_14,
            y: hasil_part_14
          },
          {
            name: part_15,
            y: hasil_part_15
          },
          {
            name: part_16,
            y: hasil_part_16
          },
          {
            name: part_17,
            y: hasil_part_17
          },
          {
            name: part_18,
            y: hasil_part_18
          },
          {
            name: part_19,
            y: hasil_part_19

          }
          ]
        }
        ]
      });
}
else if (jml == 19) {
  var part_0 = res.responseJSON.data[0].PartNo;
  var part_1 = res.responseJSON.data[1].PartNo;
  var part_2 = res.responseJSON.data[2].PartNo;
  var part_3 = res.responseJSON.data[3].PartNo;
  var part_4 = res.responseJSON.data[4].PartNo;
  var part_5 = res.responseJSON.data[5].PartNo;
  var part_6 = res.responseJSON.data[6].PartNo;
  var part_7 = res.responseJSON.data[7].PartNo;
  var part_8 = res.responseJSON.data[8].PartNo;
  var part_9 = res.responseJSON.data[9].PartNo;
  var part_10 = res.responseJSON.data[10].PartNo;
  var part_11 = res.responseJSON.data[11].PartNo;
  var part_12 = res.responseJSON.data[12].PartNo;
  var part_13 = res.responseJSON.data[13].PartNo;
  var part_14 = res.responseJSON.data[14].PartNo;
  var part_15 = res.responseJSON.data[15].PartNo;
  var part_16 = res.responseJSON.data[16].PartNo;
  var part_17 = res.responseJSON.data[17].PartNo;
  var part_18 = res.responseJSON.data[18].PartNo;
  var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
  var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
  var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
  var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
  var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
  var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
  var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
  var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
  var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
  var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
  var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
  var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
  var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
  var hasil_part_13 = Number(res.responseJSON.data[13].number_of_part);
  var hasil_part_14 = Number(res.responseJSON.data[14].number_of_part);
  var hasil_part_15 = Number(res.responseJSON.data[15].number_of_part);
  var hasil_part_16 = Number(res.responseJSON.data[16].number_of_part);
  var hasil_part_17 = Number(res.responseJSON.data[17].number_of_part);
  var hasil_part_18 = Number(res.responseJSON.data[18].number_of_part);
  var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo,
  res.responseJSON.data[3].PartNo,
  res.responseJSON.data[4].PartNo,
  res.responseJSON.data[5].PartNo,
  res.responseJSON.data[6].PartNo,
  res.responseJSON.data[7].PartNo,
  res.responseJSON.data[8].PartNo,
  res.responseJSON.data[9].PartNo,
  res.responseJSON.data[10].PartNo,
  res.responseJSON.data[11].PartNo,
  res.responseJSON.data[12].PartNo,
  res.responseJSON.data[13].PartNo,
  res.responseJSON.data[14].PartNo,
  res.responseJSON.data[15].PartNo,
  res.responseJSON.data[16].PartNo,
  res.responseJSON.data[17].PartNo,
  res.responseJSON.data[18].PartNo
  ];

  Highcharts.chart('crot', {
    chart: {
      type: 'column'
    },
    title: {
      text: ojk
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number Top Comp Removal'
      }

    },
    legend: {
      enabled: false
    },

    plotOptions: {
      series: {
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal2').modal('show');
             if ($.fn.dataTable.isDataTable('#comp_table')) {
              var table = $("#comp_table").DataTable();
              table.destroy();
              $('#comp_table').html("");
            }
            var category = this.category;
            var dikirim = zr[category];
            var pencarian_hasil = '<thead id ="sakj">' +
            '<th class="text-center">No</th>' +
            '<th class="text-center">Notification</th>' +
            '<th class="text-center">ATA</th>' +
            '<th class="text-center">Equipment</th>' +
            '<th class="text-center">Part Number</th>' +
            '<th class="text-center">Part Name</th>' +
            '<th class="text-center">Serial Number</th>' +
            '<th class="text-center">Register</th>' +
            '<th class="text-center">A/C Type</th>' +
            '<th class="text-center">RemCode</th>' +
            '<th class="text-center">Real Reason</th>' +
            '<th class="text-center">Date Removal</th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead>' +
            '<tbody id="enakeun"></tbody>';
            $('#comp_table').append(pencarian_hasil);
            var table = $("#comp_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9,10,11]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
            pageSize: 'LETTER',exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }}
            ],
            ajax: {
              "url": "<?= base_url('PComponent/CompDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "date_from": date_from,
                "date_to": date_to,
                "u": u,
                "s": s,
                "dikirim": dikirim,
                "ata" : ata
              },
            },

            'columns': [
            {
              data: 'no'
            },
            {
              data: 'ID'
            },
            {
              data: 'ATA'
            },
            {
              data: 'AIN'
            },
            {
              data: 'PartNo'
            },
            {
              data: 'PartName'
            },
            {
              data: 'SerialNo'
            },
            {
              data: 'Reg'
            },
            {
              data: 'Aircraft'
            },
            {
              data: 'RemCode'
            },
            {
              data: 'real_reason'
            },
            {
              data: 'DateRem'
            },
            {
              data: 'act'
            },

            ]
          });
          }
        }
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },

  series: [
  {
    name: "PART NO",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_8,
      y: hasil_part_8
    },
    {
      name: part_9,
      y: hasil_part_9
    },
    {
      name: part_10,
      y: hasil_part_10
    },
    {
      name: part_11,
      y: hasil_part_11
    },
    {
      name: part_12,
      y: hasil_part_12
    },
    {
      name: part_13,
      y: hasil_part_13
    },
    {
      name: part_14,
      y: hasil_part_14
    },
    {
      name: part_15,
      y: hasil_part_15
    },
    {
      name: part_16,
      y: hasil_part_16
    },
    {
      name: part_17,
      y: hasil_part_17
    },
    {
      name: part_18,
      y: hasil_part_18
    }
    ]
  }
  ]
});
}
else if (jml == 18) {
  var part_0 = res.responseJSON.data[0].PartNo;
  var part_1 = res.responseJSON.data[1].PartNo;
  var part_2 = res.responseJSON.data[2].PartNo;
  var part_3 = res.responseJSON.data[3].PartNo;
  var part_4 = res.responseJSON.data[4].PartNo;
  var part_5 = res.responseJSON.data[5].PartNo;
  var part_6 = res.responseJSON.data[6].PartNo;
  var part_7 = res.responseJSON.data[7].PartNo;
  var part_8 = res.responseJSON.data[8].PartNo;
  var part_9 = res.responseJSON.data[9].PartNo;
  var part_10 = res.responseJSON.data[10].PartNo;
  var part_11 = res.responseJSON.data[11].PartNo;
  var part_12 = res.responseJSON.data[12].PartNo;
  var part_13 = res.responseJSON.data[13].PartNo;
  var part_14 = res.responseJSON.data[14].PartNo;
  var part_15 = res.responseJSON.data[15].PartNo;
  var part_16 = res.responseJSON.data[16].PartNo;
  var part_17 = res.responseJSON.data[17].PartNo;
  var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
  var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
  var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
  var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
  var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
  var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
  var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
  var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
  var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
  var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
  var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
  var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
  var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
  var hasil_part_13 = Number(res.responseJSON.data[13].number_of_part);
  var hasil_part_14 = Number(res.responseJSON.data[14].number_of_part);
  var hasil_part_15 = Number(res.responseJSON.data[15].number_of_part);
  var hasil_part_16 = Number(res.responseJSON.data[16].number_of_part);
  var hasil_part_17 = Number(res.responseJSON.data[17].number_of_part);
  var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo,
  res.responseJSON.data[3].PartNo,
  res.responseJSON.data[4].PartNo,
  res.responseJSON.data[5].PartNo,
  res.responseJSON.data[6].PartNo,
  res.responseJSON.data[7].PartNo,
  res.responseJSON.data[8].PartNo,
  res.responseJSON.data[9].PartNo,
  res.responseJSON.data[10].PartNo,
  res.responseJSON.data[11].PartNo,
  res.responseJSON.data[12].PartNo,
  res.responseJSON.data[13].PartNo,
  res.responseJSON.data[14].PartNo,
  res.responseJSON.data[15].PartNo,
  res.responseJSON.data[16].PartNo,
  res.responseJSON.data[17].PartNo
  ];

  Highcharts.chart('crot', {
    chart: {
      type: 'column'
    },
    title: {
      text: ojk
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number Top Comp Removal'
      }

    },
    legend: {
      enabled: false
    },

    plotOptions: {
      series: {
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal2').modal('show');
             if ($.fn.dataTable.isDataTable('#comp_table')) {
              var table = $("#comp_table").DataTable();
              table.destroy();
              $('#comp_table').html("");
            }
            var category = this.category;
            var dikirim = zr[category];
            var pencarian_hasil = '<thead id ="sakj">' +
            '<th class="text-center">No</th>' +
            '<th class="text-center">Notification</th>' +
            '<th class="text-center">ATA</th>' +
            '<th class="text-center">Equipment</th>' +
            '<th class="text-center">Part Number</th>' +
            '<th class="text-center">Part Name</th>' +
            '<th class="text-center">Serial Number</th>' +
            '<th class="text-center">Register</th>' +
            '<th class="text-center">A/C Type</th>' +
            '<th class="text-center">RemCode</th>' +
            '<th class="text-center">Real Reason</th>' +
            '<th class="text-center">Date Removal</th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead>' +
            '<tbody id="enakeun"></tbody>';
            $('#comp_table').append(pencarian_hasil);
            var table = $("#comp_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9,10,11]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
            pageSize: 'LETTER',exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }}
            ],
            ajax: {
              "url": "<?= base_url('PComponent/CompDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "date_from": date_from,
                "date_to": date_to,
                "u": u,
                "s": s,
                "dikirim": dikirim,
                "ata" : ata
              },
            },

            'columns': [
            {
              data: 'no'
            },
            {
              data: 'ID'
            },
            {
              data: 'ATA'
            },
            {
              data: 'AIN'
            },
            {
              data: 'PartNo'
            },
            {
              data: 'PartName'
            },
            {
              data: 'SerialNo'
            },
            {
              data: 'Reg'
            },
            {
              data: 'Aircraft'
            },
            {
              data: 'RemCode'
            },
            {
              data: 'real_reason'
            },
            {
              data: 'DateRem'
            },
            {
              data: 'act'
            },

            ]
          });
          }
        }
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },

  series: [
  {
    name: "PART NO",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_8,
      y: hasil_part_8
    },
    {
      name: part_9,
      y: hasil_part_9
    },
    {
      name: part_10,
      y: hasil_part_10
    },
    {
      name: part_11,
      y: hasil_part_11
    },
    {
      name: part_12,
      y: hasil_part_12
    },
    {
      name: part_13,
      y: hasil_part_13
    },
    {
      name: part_14,
      y: hasil_part_14
    },
    {
      name: part_15,
      y: hasil_part_15
    },
    {
      name: part_16,
      y: hasil_part_16
    },
    {
      name: part_17,
      y: hasil_part_17
    }
    ]
  }
  ]
});

}
else if (jml == 17) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var part_10 = res.responseJSON.data[10].PartNo;
 var part_11 = res.responseJSON.data[11].PartNo;
 var part_12 = res.responseJSON.data[12].PartNo;
 var part_13 = res.responseJSON.data[13].PartNo;
 var part_14 = res.responseJSON.data[14].PartNo;
 var part_15 = res.responseJSON.data[15].PartNo;
 var part_16 = res.responseJSON.data[16].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
 var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
 var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
 var hasil_part_13 = Number(res.responseJSON.data[13].number_of_part);
 var hasil_part_14 = Number(res.responseJSON.data[14].number_of_part);
 var hasil_part_15 = Number(res.responseJSON.data[15].number_of_part);
 var hasil_part_16 = Number(res.responseJSON.data[16].number_of_part);
 var zr = [
 res.responseJSON.data[0].PartNo,
 res.responseJSON.data[1].PartNo,
 res.responseJSON.data[2].PartNo,
 res.responseJSON.data[3].PartNo,
 res.responseJSON.data[4].PartNo,
 res.responseJSON.data[5].PartNo,
 res.responseJSON.data[6].PartNo,
 res.responseJSON.data[7].PartNo,
 res.responseJSON.data[8].PartNo,
 res.responseJSON.data[9].PartNo,
 res.responseJSON.data[10].PartNo,
 res.responseJSON.data[11].PartNo,
 res.responseJSON.data[12].PartNo,
 res.responseJSON.data[13].PartNo,
 res.responseJSON.data[14].PartNo,
 res.responseJSON.data[15].PartNo,
 res.responseJSON.data[16].PartNo
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  }
  ]
}
]
});

}
else if (jml == 16) {
  var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var part_10 = res.responseJSON.data[10].PartNo;
 var part_11 = res.responseJSON.data[11].PartNo;
 var part_12 = res.responseJSON.data[12].PartNo;
 var part_13 = res.responseJSON.data[13].PartNo;
 var part_14 = res.responseJSON.data[14].PartNo;
 var part_15 = res.responseJSON.data[15].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
 var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
 var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
 var hasil_part_13 = Number(res.responseJSON.data[13].number_of_part);
 var hasil_part_14 = Number(res.responseJSON.data[14].number_of_part);
 var hasil_part_15 = Number(res.responseJSON.data[15].number_of_part);
 var zr = [
 res.responseJSON.data[0].PartNo,
 res.responseJSON.data[1].PartNo,
 res.responseJSON.data[2].PartNo,
 res.responseJSON.data[3].PartNo,
 res.responseJSON.data[4].PartNo,
 res.responseJSON.data[5].PartNo,
 res.responseJSON.data[6].PartNo,
 res.responseJSON.data[7].PartNo,
 res.responseJSON.data[8].PartNo,
 res.responseJSON.data[9].PartNo,
 res.responseJSON.data[10].PartNo,
 res.responseJSON.data[11].PartNo,
 res.responseJSON.data[12].PartNo,
 res.responseJSON.data[13].PartNo,
 res.responseJSON.data[14].PartNo,
 res.responseJSON.data[15].PartNo
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  }
  ]
}
]
});

}
else if (jml == 15) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var part_10 = res.responseJSON.data[10].PartNo;
 var part_11 = res.responseJSON.data[11].PartNo;
 var part_12 = res.responseJSON.data[12].PartNo;
 var part_13 = res.responseJSON.data[13].PartNo;
 var part_14 = res.responseJSON.data[14].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
 var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
 var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
 var hasil_part_13 = Number(res.responseJSON.data[13].number_of_part);
 var hasil_part_14 = Number(res.responseJSON.data[14].number_of_part);
 var zr = [
 res.responseJSON.data[0].PartNo,
 res.responseJSON.data[1].PartNo,
 res.responseJSON.data[2].PartNo,
 res.responseJSON.data[3].PartNo,
 res.responseJSON.data[4].PartNo,
 res.responseJSON.data[5].PartNo,
 res.responseJSON.data[6].PartNo,
 res.responseJSON.data[7].PartNo,
 res.responseJSON.data[8].PartNo,
 res.responseJSON.data[9].PartNo,
 res.responseJSON.data[10].PartNo,
 res.responseJSON.data[11].PartNo,
 res.responseJSON.data[12].PartNo,
 res.responseJSON.data[13].PartNo,
 res.responseJSON.data[14].PartNo
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  }
  ]
}
]
});

}
else if (jml == 14) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var part_10 = res.responseJSON.data[10].PartNo;
 var part_11 = res.responseJSON.data[11].PartNo;
 var part_12 = res.responseJSON.data[12].PartNo;
 var part_13 = res.responseJSON.data[13].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
 var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
 var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
 var hasil_part_13 = Number(res.responseJSON.data[13].number_of_part);
 var zr = [
 res.responseJSON.data[0].PartNo,
 res.responseJSON.data[1].PartNo,
 res.responseJSON.data[2].PartNo,
 res.responseJSON.data[3].PartNo,
 res.responseJSON.data[4].PartNo,
 res.responseJSON.data[5].PartNo,
 res.responseJSON.data[6].PartNo,
 res.responseJSON.data[7].PartNo,
 res.responseJSON.data[8].PartNo,
 res.responseJSON.data[9].PartNo,
 res.responseJSON.data[10].PartNo,
 res.responseJSON.data[11].PartNo,
 res.responseJSON.data[12].PartNo,
 res.responseJSON.data[13].PartNo
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  }
  ]
}
]
});



}
else if (jml == 13) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var part_10 = res.responseJSON.data[10].PartNo;
 var part_11 = res.responseJSON.data[11].PartNo;
 var part_12 = res.responseJSON.data[12].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
 var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
 var hasil_part_12 = Number(res.responseJSON.data[12].number_of_part);
 var zr = [
 res.responseJSON.data[0].PartNo,
 res.responseJSON.data[1].PartNo,
 res.responseJSON.data[2].PartNo,
 res.responseJSON.data[3].PartNo,
 res.responseJSON.data[4].PartNo,
 res.responseJSON.data[5].PartNo,
 res.responseJSON.data[6].PartNo,
 res.responseJSON.data[7].PartNo,
 res.responseJSON.data[8].PartNo,
 res.responseJSON.data[9].PartNo,
 res.responseJSON.data[10].PartNo,
 res.responseJSON.data[11].PartNo,
 res.responseJSON.data[12].PartNo
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  }
  ]
}
]
});

}
else if (jml == 12) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var part_10 = res.responseJSON.data[10].PartNo;
 var part_11 = res.responseJSON.data[11].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
 var hasil_part_11 = Number(res.responseJSON.data[11].number_of_part);
 var zr = [
 res.responseJSON.data[0].PartNo,
 res.responseJSON.data[1].PartNo,
 res.responseJSON.data[2].PartNo,
 res.responseJSON.data[3].PartNo,
 res.responseJSON.data[4].PartNo,
 res.responseJSON.data[5].PartNo,
 res.responseJSON.data[6].PartNo,
 res.responseJSON.data[7].PartNo,
 res.responseJSON.data[8].PartNo,
 res.responseJSON.data[9].PartNo,
 res.responseJSON.data[10].PartNo,
 res.responseJSON.data[11].PartNo
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  }
  ]
}
]
});

}
else if (jml == 11) {
  var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var part_10 = res.responseJSON.data[10].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var hasil_part_10 = Number(res.responseJSON.data[10].number_of_part);
 var zr = [
 res.responseJSON.data[0].PartNo,
 res.responseJSON.data[1].PartNo,
 res.responseJSON.data[2].PartNo,
 res.responseJSON.data[3].PartNo,
 res.responseJSON.data[4].PartNo,
 res.responseJSON.data[5].PartNo,
 res.responseJSON.data[6].PartNo,
 res.responseJSON.data[7].PartNo,
 res.responseJSON.data[8].PartNo,
 res.responseJSON.data[9].PartNo,
 res.responseJSON.data[10].PartNo
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  }
  ]
}
]
});

}
else if (jml ==  10) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var part_9 = res.responseJSON.data[9].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 var hasil_part_9 = Number(res.responseJSON.data[9].number_of_part);
 var zr = [res.responseJSON.data[0].PartNo,res.responseJSON.data[1].PartNo,res.responseJSON.data[2].PartNo,res.responseJSON.data[3].PartNo,res.responseJSON.data[4].PartNo,res.responseJSON.data[5].PartNo,res.responseJSON.data[6].PartNo,res.responseJSON.data[7].PartNo,res.responseJSON.data[8].PartNo,res.responseJSON.data[9].PartNo];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
            $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },

          ]
        });
        }
      }
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  }
  ]
}
]

});
} else if (jml == 9) {
 var part_0     = res.responseJSON.data[0].PartNo;
 var part_1      = res.responseJSON.data[1].PartNo;
 var part_2     = res.responseJSON.data[2].PartNo;
 var part_3     = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;
 var part_8 = res.responseJSON.data[8].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);
 var hasil_part_8 = Number(res.responseJSON.data[8].number_of_part);
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
             $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL'
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  }
  ]
}
]



});
} else if (jml == 8) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;
 var part_7 = res.responseJSON.data[7].PartNo;

 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
 var hasil_part_7 = Number(res.responseJSON.data[7].number_of_part);


   var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo,
  res.responseJSON.data[3].PartNo,
  res.responseJSON.data[4].PartNo,
  res.responseJSON.data[5].PartNo,
  res.responseJSON.data[6].PartNo,
  res.responseJSON.data[7].PartNo
  ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
             $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  }
  ]
}
]



});
} else if (jml == 7) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;
 var part_6 = res.responseJSON.data[6].PartNo;

 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
 var hasil_part_6 = Number(res.responseJSON.data[6].number_of_part);
    var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo,
  res.responseJSON.data[3].PartNo,
  res.responseJSON.data[4].PartNo,
  res.responseJSON.data[5].PartNo,
  res.responseJSON.data[6].PartNo
  ];


 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
             $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  }
  ]
}
]

});
} else if (jml == 6) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var part_5 = res.responseJSON.data[5].PartNo;

 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
 var hasil_part_5 = Number(res.responseJSON.data[5].number_of_part);
  var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo,
  res.responseJSON.data[3].PartNo,
  res.responseJSON.data[4].PartNo,
  res.responseJSON.data[5].PartNo
  ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
             $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  }
  ]
}
]



});
} else if (jml == 5) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var part_4 = res.responseJSON.data[4].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);
 var hasil_part_4 = Number(res.responseJSON.data[4].number_of_part);
   var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo,
  res.responseJSON.data[3].PartNo,
  res.responseJSON.data[4].PartNo
  ];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
             $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];


          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  }
  ]
}
]
});
} else if (jml ==  4) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;
 var part_3 = res.responseJSON.data[3].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);
 var hasil_part_3 = Number(res.responseJSON.data[3].number_of_part);

    var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo,
  res.responseJSON.data[3].PartNo
  ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
             $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  }
  ]
}
]
});
} else if (jml ==  3) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var part_1 = res.responseJSON.data[1].PartNo;
 var part_2 = res.responseJSON.data[2].PartNo;

 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
 var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);
 var hasil_part_2 = Number(res.responseJSON.data[2].number_of_part);


    var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo,
  res.responseJSON.data[2].PartNo
  ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
             $('#comp_table').html("");
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  }
  ]
}
]
});
} else if (jml == 2) {
  var part_0 = res.responseJSON.data[0].PartNo;
  var part_1 = res.responseJSON.data[1].PartNo;

  var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);
  var hasil_part_1 = Number(res.responseJSON.data[1].number_of_part);

      var zr = [
  res.responseJSON.data[0].PartNo,
  res.responseJSON.data[1].PartNo
  ];


  Highcharts.chart('crot', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP 10 COMPONENT REMOVAL'
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number Top Comp Removal'
      }

    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal2').modal('show');
             if ($.fn.dataTable.isDataTable('#comp_table')) {
              var table = $("#comp_table").DataTable();
              table.destroy();
               $('#comp_table').html("");
            }
            var category = this.category;
            var dikirim = zr[category];
            var pencarian_hasil = '<thead id ="sakj">' +
            '<th class="text-center">No</th>' +
            '<th class="text-center">Notification</th>' +
            '<th class="text-center">ATA</th>' +
            '<th class="text-center">Equipment</th>' +
            '<th class="text-center">Part Number</th>' +
            '<th class="text-center">Part Name</th>' +
            '<th class="text-center">Serial Number</th>' +
            '<th class="text-center">Register</th>' +
            '<th class="text-center">A/C Type</th>' +
            '<th class="text-center">RemCode</th>' +
            '<th class="text-center">Real Reason</th>' +
            '<th class="text-center">Date Removal</th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead>' +
            '<tbody id="enakeun"></tbody>';
            $('#comp_table').append(pencarian_hasil);
            var table = $("#comp_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9,10,11]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
            pageSize: 'LETTER',exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }}
            ],
            ajax: {
              "url": "<?= base_url('PComponent/CompDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "date_from": date_from,
                "date_to": date_to,
                "u": u,
                "s": s,
                "dikirim": dikirim,
                "ata" : ata
              },
            },

            'columns': [
            {
              data: 'no'
            },
            {
              data: 'ID'
            },
            {
              data: 'ATA'
            },
            {
              data: 'AIN'
            },
            {
              data: 'PartNo'
            },
            {
              data: 'PartName'
            },
            {
              data: 'SerialNo'
            },
            {
              data: 'Reg'
            },
            {
              data: 'Aircraft'
            },
            {
              data: 'RemCode'
            },
            {
              data: 'real_reason'
            },
            {
              data: 'DateRem'
            },
            {
              data: 'act'
            },
            ]
          });
          }
        }
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },

  series: [
  {
    name: "PART NO",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    }
    ]
  }
  ]

});
} else if (jml == 1) {
 var part_0 = res.responseJSON.data[0].PartNo;
 var hasil_part_0 = Number(res.responseJSON.data[0].number_of_part);

       var zr = [
  res.responseJSON.data[0].PartNo
  ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: ojk
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number Top Comp Removal'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#comp_table')) {
            var table = $("#comp_table").DataTable();
            table.destroy();
          }
          var category = this.category;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Notification</th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">Equipment</th>' +
          '<th class="text-center">Part Number</th>' +
          '<th class="text-center">Part Name</th>' +
          '<th class="text-center">Serial Number</th>' +
          '<th class="text-center">Register</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">RemCode</th>' +
          '<th class="text-center">Real Reason</th>' +
          '<th class="text-center">Date Removal</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#comp_table').append(pencarian_hasil);
          var table = $("#comp_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP COMPONENT DETAIL', exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP COMPONENT DETAIL', orientation: 'landscape',
          pageSize: 'LETTER',exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11]
          }}
          ],
          ajax: {
            "url": "<?= base_url('PComponent/CompDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "date_from": date_from,
              "date_to": date_to,
              "u": u,
              "s": s,
              "dikirim": dikirim,
              "ata" : ata
            },
          },

          'columns': [
          {
            data: 'no'
          },
          {
            data: 'ID'
          },
          {
            data: 'ATA'
          },
          {
            data: 'AIN'
          },
          {
            data: 'PartNo'
          },
          {
            data: 'PartName'
          },
          {
            data: 'SerialNo'
          },
          {
            data: 'Reg'
          },
          {
            data: 'Aircraft'
          },
          {
            data: 'RemCode'
          },
          {
            data: 'real_reason'
          },
          {
            data: 'DateRem'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    }
  }
},

tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},

series: [
{
  name: "PART NO",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  }
  ]
}
]



});
}

}

},
'columns': [{
  data: 'no'
},
{
  data: 'PartNo'
},
{
  data: 'PartName'
}
]
},

);

$('body').addClass('mini-navbar');
});

function uy(){
  var u =  $('#u').val();
  if (u != 'U') {
   $('#u').val('U');
 } else {
  $('#u').val('');
}
}
function cuy(){
  var s =  $('#s').val();
  if (s != 'S') {
   $('#s').val('S');
 } else {
  $('#s').val('');
}
}
</script>


