<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-pencil-square"></i>&nbsp; PIREP / UPDATE DATA</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>

        <div class="ibox-content inspinia-timeline">
          <form method="POST" action="<?= base_url('Techlog/UpdatePirep') ?>">
            <div class="row">
              <div class="form-group col-md-2">
                <label for="">Notification ID  :</label>
                <input type="text" class="form-control" readonly="" name="notif" value="<?= $dt['Notification'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">SEQ :</label>
                <input type="text" class="form-control" name="seq" value="<?= $dt['SEQ'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">STADEP :</label>
                <input type="text" class="form-control" name="stadep" value="<?= $dt['STADEP'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">STAARR :</label>
                <input type="text" class="form-control" name="staar" value="<?= $dt['STAARR'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">ATA :</label>
                <input type="text" class="form-control" name="ata" value="<?= $dt['ATA'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">SUB ATA :</label>
                <input type="text" class="form-control" name="subata" value="<?= $dt['SUBATA'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">Flight No :</label>
                <input type="text" class="form-control" name="fn" value="<?= $dt['FN'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">Register :</label>
                <input type="text" class="form-control" name="reg" value="<?= $dt['REG'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">A/C Type :</label>
                <select class="form-control" name="actype" id="actype2">
                  <option selected="" value="<?=  $dt['ac'] ?>"><?=  $dt['ac'] ?></option>
                   <?php foreach ($actype as $key ) :?>
                     <option value="<?=  $key['ACType'] ?>"><?=  $key['ACType'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
                 <div class="form-group col-md-3">
                <label for="">Date :</label>
                <input type="date" class="form-control" name="date" value="<?= $dt['DATE'] ?>">
              </div>
               <div class="form-group col-md-3">
                <label for="">Coding :</label>
                <input type="text" class="form-control" name="coding" value="<?= $dt['PirepMarep'] ?>">
              </div>
              <div class="form-group col-md-6">
                <label for="">PROBLEM :</label>
                <input type="text" class="form-control" name="problem" value="<?= $dt['PROBLEM'] ?>">
              </div>
              <div class="form-group col-md-6">
                <label for="">KeyProblem :</label>
                <input type="text" class="form-control" name="KeyProblem" value="<?= $dt['Keyword'] ?>">
              </div>
               <div class="form-group col-md-12">
                <label for="">Rectification :</label>
                <textarea name="action"><?= $dt['ACTION'] ?></textarea>
              </div>
              <div class="col-md-12">
                <button class="btn btn-success" name="update"><i class="fa fa-edit"></i> &nbsp; UPDATE DATA</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


<script>
 CKEDITOR.replace( 'action' );
 CKEDITOR.config.removePlugins = 'elementspath';
</script>

