<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins" id="form_query">
        <div class="ibox-title">
          <h5><i class="fa fa-laptop"></i>&nbsp; Filter Componnent Removal Criteria </h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <div class="row">
            <div class="col-md-4">
             <label for="operator">OPERATOR <span style="color: red">*</span> :</label>
             <select onchange="io();" class="form-control" id="operator">
              <?php if ($unit == 'GA') : ?>
                <option value="GARUDA INDONESIA">GARUDA</option>
                <?php elseif ($unit == 'QG'): ?> 
                  <option value="CITILINK">CITILINK</option>
                  <?php elseif ($unit == 'SJ'): ?> 
                    <option value="SRIWIJAYA">SRIWIJAYA</option>
                    <?php elseif ($unit == 'IN'): ?> 
                      <option value="NAM">NAM AIR</option>
                      <?php else : ?>
                       <option value=""></option>
                       <option value="GARUDA INDONESIA">GARUDA</option>
                       <option value="CITILINK">CITILINK</option>
                       <option value="SRIWIJAYA">SRIWIJAYA</option>
                       <option value="NAM">NAM AIR</option>
                     <?php endif; ?>
                   </select>
                 </div>
                 <div class="col-md-4">
                  <label for="">A/C TYPE <span style="color: red">*</span> :</label>
                  <select onchange="ac();" multiple class="form-control" id="actype">
                   <option value=""></option>
                 </select>
               </div>
               <div class="col-md-4">
                <div class="form-group">
                  <label for="">A/C REG :</label>
                  <input type="text" name="acreg" id="acreg" class="form-control">
                  <span><i>Entry without "PK-"</i></span>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <div class="form-group">
                    <label for="">PART NUMBER :</label>
                    <input type="text" name="part" id="part" class="form-control">
                  </div>
                </div>

              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="form-group">
                    <label for="">PART NAME :</label>
                    <input type="text" name="part_name" id="part_name" class="form-control">
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <div class="form-group">
                    <label for="">ATA :</label>
                    <div id="ot"></div>
                    <div id="at">
                      <select class="form-control" id="ata"> 
                        <option value=""></option>
                        <?php foreach ($ata as $key) :?>
                          <option value="<?= $key['ATA'] ?>"><?= $key['ATA'] ?></option>
                        <?php endforeach; ?>
                      </select> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="">DATE FROM <span style="color: red">*</span> :</label>
                  <input type="text" id="date_from" class="datepicker-here form-control" data-language='en' data-date-format="yyyy-mm-dd" />
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="">DATE TO <span style="color: red">*</span> :</label>
                  <input type="text" id="date_to" class="datepicker-here form-control" data-language='en' data-date-format="yyyy-mm-dd" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">REMOVAL CODE :</label>
                <br>
                <div style="margin-left: -10px" class="col-md-6 col-sm-5">
                  <input type="checkbox" onclick="uy()"  id="u"> <label class="checkbox-inline"> UNSCHEDULED</label>
                  &nbsp;
                  <input type="checkbox"  onclick="cuy()" id="s"><label class="checkbox-inline"> SCHEDULED </label> 
                </div>
                <br>
              </div>
              <div class="col-md-12">
                <button type="button" id="component_view" class="btn btn-success"><i class="fa fa-print"></i>&nbsp;&nbsp;Display Report</button>
              </div>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
              <div class="ibox float-e-margins" id="form_query2">
                <div class="ibox-title">
                  <h5><i class="fa fa-laptop"></i>&nbsp; Grafik Component Removal</h5>
                  <div class="ibox-tools">
                    <a class="collapse-link">
                      <i class="fa fa-chevron-up"></i>
                    </a>
                  </div>
                </div>
                <div class="ibox-content inspinia-timeline table-responsive" id="sult">
                 <div id="crut"></div>
               </div>
             </div>
           </div>
         </div>
         <div class="row">
          <div class="col-md-12">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <h5><i class="fa fa-bar-chart-o"></i>&nbsp; Result Table Component Removal</h5>
                <div class="ibox-tools">
                  <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                  </a>
                </div>
              </div>
              <div class="ibox-content inspinia-timeline table-responsive" id="result">
               <div class="table-responsive">
                 <table id="table_component_removal" class="table table-bordered table-striped table-hovered"></table>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
   <script>

$(document).on('keypress',function(e) {
    if(e.which == 13) {
if ($.fn.dataTable.isDataTable('#table_component_removal')) {
      var table = $("#table_component_removal").DataTable();
      table.destroy();
      $('#table_component_removal').empty();
    }
    var actype = $('#actype').val();
    var acreg  = $('#acreg').val();
    var part_no = $('#part_no').val();
    var part_name = $('#part_name').val();
    var date_to = $('#date_to').val();
    var date_from = $('#date_from').val();
    var d_t = moment(date_to).format("DD MMMM YYYY");
    var d_f = moment(date_from).format("DD MMMM YYYY");
    var part = $('#part').val();
    var u = $('#u').val();
    var s = $('#s').val();
    var ata = $('#ata').val();


    if (date_to == '' || date_from == '') {
      alert('Please select Date From and Date To !');
      return false;
    } 
    if (actype == '' || actype == null) {
      alert('Please select A/C Type !');
      return false;
    }
    $('#component_view').attr('disabled', true);
    $('#component_view').html('Loading .. !');
    var pencarian_hasil = '<thead id ="head_cek">' +
    '<th class="text-center">No</th>' +
    '<th class="text-center">Notification</th>' +
    '<th class="text-center">ATA</th>' +
    '<th class="text-center">Equipment</th>' +
    '<th class="text-center">Part Number</th>' +
    '<th class="text-center">Part Name</th>' +
    '<th class="text-center">Serial Number</th>' +
    '<th class="text-center">Register</th>' +
    '<th class="text-center">A/C Type</th>' +
    '<th class="text-center">RemCode</th>' +
    '<th class="text-center">Real Reason</th>' +
    '<th class="text-center">Date Removal</th>' +
    '<th class="text-center">TSN</th>' +
    '<th class="text-center">TSI</th>' +
    '<th class="text-center">CSN</th>' +
    '<th class="text-center">CSI</th>' +
    '<th class="text-center">#</th>' +
    '</tr></thead>' +
    '<tbody id="enakeun"></tbody>';
    $('#table_component_removal').append(pencarian_hasil);
    var judul = 'Component Removal ' +  actype + ' ' + acreg + ' '+  ata ;
    var table = $("#table_component_removal").DataTable({
     retrieve: true,
     pagin: false,
     dom: 'Bfrtip',
     buttons: [
     {extend: 'excel', title: 'REPORT ' + judul,
     exportOptions: {
      columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
    }
  },
  {extend: 'pdf', title: 'REPORT ' + judul, orientation: 'landscape',
  exportOptions: {
    columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
  },
  pageSize: 'A0'}
  ],
  ajax: {
    "url": "<?= base_url('Components/search') ?>",
    "type": "POST",
    "data": {
      "actype": actype,
      "acreg": acreg,
      "date_to": date_to,
      "date_from": date_from,
      "u": u,
      "s": s,
      "part": part,
      "part_name": part_name,
      "ata": ata,
    },
    complete: function (res) {
      $('#component_view').attr('disabled', false);
      $('#component_view').html('<i class="fa fa-print"></i>&nbsp;&nbsp;DISPLAY REPORT');
      var b = res.responseJSON;
      var c = b.graph;
      var act = b.title;
      var e = [];
      var f = [];
      console.log(act);
      for (var i = 0; i <  c.length; i++) {
        e.push(c[i].dates);
        var j = Number(c[i].number_of_rem);
        f.push(j);

      }
      var label_data = [];
      var jumlah_pirep = [];
      Highcharts.chart('crut', {
        title: {
          text: judul
        },

        subtitle: {
          text: 'Periode : ' + d_f + ' - ' + d_t
        },
        xAxis: {
          categories: e
        },
      
           yAxis: {
          title: {
            text: 'Number of Component Removal'
          },
          showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
        },
        series: [{
          name: '',
          data: f
        }],

        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
              }
            }
          }]
        }

      });



    }

  },
  'columns': [{
    data: 'no'
  },
  {
    data: 'notification'
  },
  {
    data: 'ata'
  },
  {
    data: 'equipment'
  },
  {
    data: 'PartNo'
  },
  {
    data: 'PartName'
  },
  {
    data: 'SerialNo'
  },   
  {
    data: 'Reg'
  },
  {
    data: 'Aircraft'
  },
  {
    data: 'RemCode'
  },
  {
    data: 'real_reason'
  },
  {
    data: 'DateRem'
  },
  {
    data: 'TSN'
  },
  {
    data: 'TSI'
  },
  {
    data: 'CSN'
  },
  {
    data: 'CSI'
  },
  {
    data: 'act'
  }

  ]
});

    $('body').addClass('mini-navbar');
    }
});



    $('#component_view').on('click', function () {
     if ($.fn.dataTable.isDataTable('#table_component_removal')) {
      var table = $("#table_component_removal").DataTable();
      table.destroy();
      $('#table_component_removal').empty();
    }
    var actype = $('#actype').val();
    var acreg  = $('#acreg').val();
    var part_no = $('#part_no').val();
    var part_name = $('#part_name').val();
    var date_to = $('#date_to').val();
    var date_from = $('#date_from').val();
    var d_t = moment(date_to).format("DD MMMM YYYY");
    var d_f = moment(date_from).format("DD MMMM YYYY");
    var part = $('#part').val();
    var u = $('#u').val();
    var s = $('#s').val();
    var ata = $('#ata').val();


    if (date_to == '' || date_from == '') {
      alert('Please select Date From and Date To !');
      return false;
    } 
    if (actype == '' || actype == null) {
      alert('Please select A/C Type !');
      return false;
    }
    $('#component_view').attr('disabled', true);
    $('#component_view').html('Loading .. !');
    var pencarian_hasil = '<thead id ="head_cek">' +
    '<th class="text-center">No</th>' +
    '<th class="text-center">Notification</th>' +
    '<th class="text-center">ATA</th>' +
    '<th class="text-center">Equipment</th>' +
    '<th class="text-center">Part Number</th>' +
    '<th class="text-center">Part Name</th>' +
    '<th class="text-center">Serial Number</th>' +
    '<th class="text-center">Register</th>' +
    '<th class="text-center">A/C Type</th>' +
    '<th class="text-center">RemCode</th>' +
    '<th class="text-center">Real Reason</th>' +
    '<th class="text-center">Date Removal</th>' +
    '<th class="text-center">TSN</th>' +
    '<th class="text-center">TSI</th>' +
    '<th class="text-center">CSN</th>' +
    '<th class="text-center">CSI</th>' +
    '<th class="text-center">#</th>' +
    '</tr></thead>' +
    '<tbody id="enakeun"></tbody>';
    $('#table_component_removal').append(pencarian_hasil);
    var judul = 'Component Removal ' +  actype + ' ' + acreg + ' '+  ata ;
    var table = $("#table_component_removal").DataTable({
     retrieve: true,
     pagin: false,
     dom: 'Bfrtip',
     buttons: [
     {extend: 'excel', title: 'REPORT ' + judul,
     exportOptions: {
      columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
    }
  },
  {extend: 'pdf', title: 'REPORT ' + judul, orientation: 'landscape',
  exportOptions: {
    columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
  },
  pageSize: 'A0'}
  ],
  ajax: {
    "url": "<?= base_url('Components/search') ?>",
    "type": "POST",
    "data": {
      "actype": actype,
      "acreg": acreg,
      "date_to": date_to,
      "date_from": date_from,
      "u": u,
      "s": s,
      "part": part,
      "part_name": part_name,
      "ata": ata,
    },
    complete: function (res) {
      $('#component_view').attr('disabled', false);
      $('#component_view').html('<i class="fa fa-print"></i>&nbsp;&nbsp;DISPLAY REPORT');
      var b = res.responseJSON;
      var c = b.graph;
      var act = b.title;
      var e = [];
      var f = [];
      console.log(act);
      for (var i = 0; i <  c.length; i++) {
        e.push(c[i].dates);
        var j = Number(c[i].number_of_rem);
        f.push(j);

      }
      var label_data = [];
      var jumlah_pirep = [];
      Highcharts.chart('crut', {
        title: {
          text: judul
        },

        subtitle: {
          text: 'Periode : ' + d_f + ' - ' + d_t
        },
        xAxis: {
          categories: e
        },
      
           yAxis: {
          title: {
            text: 'Number of Component Removal'
          },
          showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
        },
        series: [{
          name: '',
          data: f
        }],

        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
              }
            }
          }]
        }

      });



    }

  },
  'columns': [{
    data: 'no'
  },
  {
    data: 'notification'
  },
  {
    data: 'ata'
  },
  {
    data: 'equipment'
  },
  {
    data: 'PartNo'
  },
  {
    data: 'PartName'
  },
  {
    data: 'SerialNo'
  },   
  {
    data: 'Reg'
  },
  {
    data: 'Aircraft'
  },
  {
    data: 'RemCode'
  },
  {
    data: 'real_reason'
  },
  {
    data: 'DateRem'
  },
  {
    data: 'TSN'
  },
  {
    data: 'TSI'
  },
  {
    data: 'CSN'
  },
  {
    data: 'CSI'
  },
  {
    data: 'act'
  }

  ]
});

    $('body').addClass('mini-navbar');

  });
function uy(){
  var u =  $('#u').val();
  if (u != 'U') {
   $('#u').val('U');
 } else {
  $('#u').val('');
}
}
function cuy(){
  var s =  $('#s').val();
  if (s != 'S') {
   $('#s').val('S');
 } else {
  $('#s').val('');
}
}

</script>



