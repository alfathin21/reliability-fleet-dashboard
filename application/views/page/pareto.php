 <script src="https://code.highcharts.com/modules/data.js"></script>
 <script src="https://code.highcharts.com/modules/drilldown.js"></script>
 <div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
 <div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins" id="form_query">
        <div class="ibox-title">
          <h5><i class="fa fa-laptop"></i>&nbsp; Filter Pareto Criteria</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline">
          <div class="row">
            <div class="col-md-2">
              <label for="operator">OPERATOR <span style="color: red">*</span> :</label>
              <select onchange="io();" class="form-control" id="operator">
                <?php if ($unit == 'GA') : ?>
                  <option value="GARUDA INDONESIA">GARUDA</option>
                  <?php elseif ($unit == 'QG'): ?> 
                    <option value="CITILINK">CITILINK</option>
                    <?php elseif ($unit == 'SJ'): ?> 
                      <option value="SRIWIJAYA">SRIWIJAYA</option>
                      <?php elseif ($unit == 'IN'): ?> 
                        <option value="NAM">NAM AIR</option>
                        <?php else : ?>
                         <option value=""></option>
                         <option value="GARUDA INDONESIA">GARUDA</option>
                         <option value="CITILINK">CITILINK</option>
                         <option value="SRIWIJAYA">SRIWIJAYA</option>
                         <option value="NAM">NAM AIR</option>
                       <?php endif; ?>
                     </select>
                   </div>
                   <div class="col-md-2">
                    <label for="">A/C TYPE <span style="color: red">*</span> :</label>
                    <select  onchange="ac();"  name=""  class="form-control" id="actype" multiple="">
                     <option value=""></option>
                   </select>
                 </div>
                 <div class="col-md-8">
                  <div class="form-group">
                    <label class="col-sm-0 control-label">GRAPH SETTINGS FOR X-AXIS <span style="color: red">*</span> :</label>
                    <div class="col-sm-12"  style="margin-left: -33px;">
                      <label class="checkbox-inline">
                      <input type="radio" value="ac_reg"  name ="q1" onchange="radioget($(this).val())" > A/C REG 
                    </label>
                      <label class="checkbox-inline">
                       <input type="radio" value="ata"  name ="q1" onchange="radioget($(this).val())" > ATA 2 DIGIT 
                     </label>
                     
                    <label class="checkbox-inline">
                      <input  type="radio" value="subata"  name ="q1" onchange="radioget($(this).val())"> ATA 4 DIGIT 
                    </label>
                    <label class="checkbox-inline">
                      <input type="radio" value="keyproblem"  name ="q1" onchange="radioget($(this).val())" >  KEY PROBLEM 
                    </label> 
                  </div>
                </div>
                <br>
              </div>
            </div>
            <br>  
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                 <label for="">DATE FROM : <span style="color: red">*</span> :</label>
                 <input type="text" id="date_from" class="datepicker-here form-control" data-language='en' data-date-format="yyyy-mm-dd" />
               </div>
             </div>
             <div class="col-md-2">
              <div class="form-group">
                <label for="">DATE TO :<span style="color: red">*</span> :</label>
                <input type="hidden" id="p_graph">
                <input type="hidden" id="clue">
                <input type="text" id="date_to" class="datepicker-here form-control" data-language='en' data-date-format="yyyy-mm-dd" />
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="">A/C REG :</label>
                <input type="text" name="acreg" id="acreg" class="form-control">
                <span><i>Entry without "PK-"</i></span>
              </div>
            </div>
            <div class="col-md-2 form-group">
              <label for="">ATA :</label><br>
              <select onchange="atakey()" class="form-control" id="ata">
                <option value=""></option>
                <?php foreach ($ata as $key) :?>
                  <option value="<?= $key['ATA'] ?>"><?= $key['ATA'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-md-2">
              <label for="">PARETO TOP  :</label>
              <select  name="" class="idnsa form-control" id="filter">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="20">20</option>
              </select> 
            </div>
          </div>
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label for="">KEYPROBLEM :</label>
                <div id="oy"></div>
                <div id="ky">
                  <select name="" class="form-control" id="KeyProblem"></select>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <br>
              <button type="button" id="pareto_view" class="btn btn-success"><i class="fa fa-print"></i>&nbsp;&nbsp;DISPLAY REPORT</button>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="ibox float-e-margins" id="form_query2">
            <div class="ibox-title">
              <h5><i class="fa fa-bar-chart-o"></i>&nbsp; TOP DELAY</h5>
              <div class="ibox-tools">
                <a class="collapse-link">
                  <i class="fa fa-chevron-up"></i>
                </a>
              </div>
            </div>
            <div class="ibox-content inspinia-timeline table-responsive" id="sult">
             <div id="crot"></div>
           </div>
         </div>
       </div>
     </div>
     <div class="row">
      <div class="col-md-12">
        <div class="ibox float-e-margins" id="form_query2">
          <div class="ibox-title">
            <h5><i class="fa fa-bar-chart-o"></i>&nbsp; TOP PIREP</h5>
            <div class="ibox-tools">
              <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
              </a>
            </div>
          </div>
          <div class="ibox-content inspinia-timeline table-responsive" id="sult">
           <div id="crut"></div>
         </div>
       </div>
     </div>
   </div>
   <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-bar-chart-o"></i>&nbsp;TOP MAREP </h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline table-responsive" id="result">
         <div id="cret"></div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<!--  Delay Detail -->
<!-- Pirep Detail -->
<div class="container-fluid">
  <div class="row">        
    <div class="col-xs-6 col-md-6 big-box" >
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content ">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">TOP DELAY DETAIL </h4>
            </div>
            <div class="modal-body"  style="overflow-y:auto; height:70vh;">  
              <div id="op">
                <table class="table table-responsive table-bordered table-striped" id="delay_table"></table>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-sign-out"></i> Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--  -->

<!-- marep -->
<!-- Pirep Detail -->
<div class="container-fluid">
  <div class="row">        
    <div class="col-xs-6 col-md-6 big-box" >
      <div class="modal fade" id="marep_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content ">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">TOP MAREP DETAIL </h4>
            </div>
            <div class="modal-body"  style="overflow-y:auto; height:70vh;">  
              <div id="op">
                <table class="table table-responsive table-bordered table-striped" id="marep_table"></table>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-sign-out"></i> Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--  -->
<!-- Akhir marep -->
<!-- Pirep Detail -->
<div class="container-fluid">
  <div class="row">        
    <div class="col-xs-6 col-md-6 big-box" >
      <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content ">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">TOP PIREP DETAIL</h4>
            </div>
            <div class="modal-body"  style="overflow-y:auto; height:70vh;">  
              <div id="po">
                <table class="table table-fixed table-bordered table-responsive table-striped" id="pirep_table"></table>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-sign-out"></i> Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--  -->
<script>

  $(document).on('keypress',function(e) {
    if(e.which == 13) {
         $('#crut').html("");
    $('#crot').html("");
    $('#cret').html("");
    var actype = $('#actype').val();
    var acreg  = $('#acreg').val();
    var date_to = $('#date_to').val();
    var date_from = $('#date_from').val();
    var d_t = moment(date_to).format("DD MMMM YYYY");
    var d_f = moment(date_from).format("DD MMMM YYYY");
    var p_graph = $('#p_graph').val();
    var filter = $('#filter').val();
    var KeyProblem = $('#KeyProblem').val();
    var ata = $('#ata').val();
    if (actype === "" || actype == null) {
      alert('Please select A/C Type !');
      return false;
    }    if (date_to == '' || date_from == '') {
      alert('Please select Date To and Date From !');
      return false;
    }
    if (p_graph == '') {
      alert('Please select Graph settings for X-axis !');
      return false;
    }
    $('#pareto_view').attr('disabled', true);
    $('#pareto_view').html('Loading .. !');
    $.ajax({
     type:"POST",
     data:{actype:actype,acreg:acreg,date_to:date_to,date_from:date_from,p_graph:p_graph,ata:ata,filter:filter},
     url:"<?= base_url('Pareto/search') ?>",
     dataType: 'json',
     success: function(data){
      $('#pareto_view').attr('disabled', false);
      $('#pareto_view').html('<i class="fa fa-print"></i>&nbsp;&nbsp;DISPLAY REPORT');
      var judul         = actype + ' ' + acreg + ' ' + ata + ' ' + KeyProblem ;
      var sql_pirep     = data.sql_pirep;
      var sql_delay     = data.sql_delay;
      var marep         =  data.arr_marep;
      var panjang_marep =  Number(data.marep_cnt);

      if (marep !== "") {
        if (panjang_marep == 20) {
         var part_0 = marep[0].ata;
         var part_1 = marep[1].ata;
         var part_2 = marep[2].ata;
         var part_3 = marep[3].ata;
         var part_4 = marep[4].ata;  
         var part_5 = marep[5].ata;  
         var part_6 = marep[6].ata;  
         var part_7 = marep[7].ata;  
         var part_8 = marep[8].ata;  
         var part_9 = marep[9].ata;  
         var part_10 = marep[10].ata;  
         var part_11 = marep[11].ata;  
         var part_12 = marep[12].ata;  
         var part_13 = marep[13].ata;  
         var part_14 = marep[14].ata;  
         var part_15 = marep[15].ata;  
         var part_16 = marep[16].ata;  
         var part_17 = marep[17].ata;  
         var part_18 = marep[18].ata;  
         var part_19 = marep[17].ata; 

         var hasil_part_0 = Number(marep[0].number_of_ata);
         var hasil_part_1 = Number(marep[1].number_of_ata);
         var hasil_part_2 = Number(marep[2].number_of_ata);
         var hasil_part_3 = Number(marep[3].number_of_ata);
         var hasil_part_4 = Number(marep[4].number_of_ata);
         var hasil_part_5 = Number(marep[5].number_of_ata);
         var hasil_part_6 = Number(marep[6].number_of_ata);
         var hasil_part_7 = Number(marep[7].number_of_ata);
         var hasil_part_8 = Number(marep[8].number_of_ata);
         var hasil_part_9 = Number(marep[9].number_of_ata);
         var hasil_part_10 = Number(marep[10].number_of_ata);
         var hasil_part_11 = Number(marep[11].number_of_ata);
         var hasil_part_12 = Number(marep[12].number_of_ata);
         var hasil_part_13 = Number(marep[13].number_of_ata);
         var hasil_part_14 = Number(marep[14].number_of_ata);
         var hasil_part_15 = Number(marep[15].number_of_ata);
         var hasil_part_16 = Number(marep[16].number_of_ata);
         var hasil_part_17 = Number(marep[17].number_of_ata);
         var hasil_part_18 = Number(marep[18].number_of_ata);
         var hasil_part_19 = Number(marep[19].number_of_ata);


         var mr = 
         [
         marep[0].ata,
         marep[1].ata,
         marep[2].ata,
         marep[3].ata,
         marep[4].ata,
         marep[5].ata,
         marep[6].ata,
         marep[7].ata,
         marep[8].ata,
         marep[9].ata,
         marep[10].ata,
         marep[11].ata,
         marep[12].ata,
         marep[13].ata,
         marep[14].ata,
         marep[15].ata,
         marep[16].ata,
         marep[17].ata,
         marep[18].ata,
         marep[19].ata
         ];

         Highcharts.chart('cret', {
          chart: {
            type: 'column'
          },
          title: {
            text: 'TOP MAREP - ' + judul
          },
          subtitle: {
            text: 'Periode : ' + d_f + ' - ' + d_t
          },
          xAxis: {
            type: 'category'
          },
          yAxis: {
            title: {
              text: 'Number of Marep'
            },
             showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
          },
          legend: {
            enabled: false
          },
          plotOptions: {
            series: {
              borderWidth: 0,
              cursor: 'pointer',
              point: {
                events: {
                  click: function () {
                   $('#marep_modal').modal('show');
                   if ($.fn.dataTable.isDataTable('#marep_table')) {
                    var table = $("#marep_table").DataTable();
                    table.destroy();
                    $('#marep_table').html("");
                  }
                  var category = this.category;
                  var value_category = this.y;
                  var dikirim = mr[category];
                  var pencarian_hasil = '<thead id ="sakj">' +
                  '<th class="text-center">No</th>' +
                  '<th class="text-center">Date</th>' +
                  '<th class="text-center">Sequence</th>' +
                  '<th class="text-center">Notification Number</th>' +
                  '<th class="text-center">A/C Type</th>' +
                  '<th class="text-center">A/C Reg</th>' +
                  '<th class="text-center">StaDep</th>' +
                  '<th class="text-center">StaArr</th>' +
                  '<th class="text-center">Flight No </th>' +
                  '<th class="text-center">ATA</th>' +
                  '<th class="text-center">SUB ATA</th>' +
                  '<th class="text-center">Problem</th>' +
                  '<th class="text-center">Rectification</th>' +
                  '<th class="text-center">Coding</th>' +
                  '<th class="text-center">#</th>' +
                  '</tr></thead>' +
                  '<tbody id="enakeun"></tbody>';
                  $('#marep_table').append(pencarian_hasil);
                  var table = $("#marep_table").DataTable({
                    retrieve: true,
                    pagin: false,
                    dom: 'Bfrtip',
                    buttons: [
                    {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
                      columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
                    }
                  },
                  {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
                  exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
                  },
                  pageSize: 'LETTER'}
                  ],
                  ajax: {
                    "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
                    "type": "POST",
                    "data": {
                      "actype": actype,
                      "acreg": acreg,
                      "p_graph": p_graph,
                      "ata": ata,
                      "date_from": date_from,
                      "date_to": date_to,
                      "dikirim": dikirim,
                      "value_category": value_category,
                      "filter" : filter
                    },
                  },
                  'columns': [
                  {
                    data: 'no'
                  },
                  {
                    data: 'TargetDate'
                  },
                  {
                    data: 'SEQ'
                  },
                  {
                    data: 'Notification'
                  },
                  {
                    data: 'ac'
                  },
                  {
                    data: 'REG'
                  },
                  {
                    data: 'STADEP'
                  },
                  {
                    data: 'STAARR'
                  },
                  {
                    data: 'FN'
                  },
                  {
                    data: 'ATA'
                  },
                  {
                    data: 'SUBATA'
                  },
                  {
                    data: 'PROBLEM'
                  },
                  {
                    data: 'ACTION'
                  }, 
                  {
                    data: 'PirepMarep'
                  },
                  {
                    data: 'act'
                  },
                  ]
                });
                }
              }
            },
            dataLabels: {
              enabled: true,
              format: '{point.y}'
            }
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
        },
        series: [
        {
          name: "Number Of Marep",
          colorByPoint: true,
          data: [
          {
            name: part_0,
            y: hasil_part_0

          },
          {
            name: part_1,
            y: hasil_part_1

          },
          {
            name: part_2,
            y: hasil_part_2
          },
          {
            name: part_3,
            y: hasil_part_3
          },
          {
            name: part_4,
            y: hasil_part_4
          },
          {
            name: part_5,
            y: hasil_part_5
          },
          {
            name: part_6,
            y: hasil_part_6
          },
          {
            name: part_7,
            y: hasil_part_7
          },
          {
            name: part_7,
            y: hasil_part_7
          },
          {
            name: part_8,
            y: hasil_part_8
          },
          {
            name: part_9,
            y: hasil_part_9
          },
          {
            name: part_10,
            y: hasil_part_10
          },
          {
            name: part_11,
            y: hasil_part_11
          },
          {
            name: part_12,
            y: hasil_part_12
          },
          {
            name: part_12,
            y: hasil_part_12
          },
          {
            name: part_13,
            y: hasil_part_13
          },
          {
            name: part_14,
            y: hasil_part_14
          },
          {
            name: part_15,
            y: hasil_part_15
          },
          {
            name: part_16,
            y: hasil_part_16
          },
          {
            name: part_17,
            y: hasil_part_17
          },
          {
            name: part_18,
            y: hasil_part_18
          },
          {
            name: part_19,
            y: hasil_part_19
          }
          ]
        }
        ]
      });
} 

else if (panjang_marep == 19) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  
 var part_12 = marep[12].ata;  
 var part_13 = marep[13].ata;  
 var part_14 = marep[14].ata;  
 var part_15 = marep[15].ata;  
 var part_16 = marep[16].ata;  
 var part_17 = marep[17].ata;  
 var part_18 = marep[18].ata;  


 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);
 var hasil_part_12 = Number(marep[12].number_of_ata);
 var hasil_part_13 = Number(marep[13].number_of_ata);
 var hasil_part_14 = Number(marep[14].number_of_ata);
 var hasil_part_15 = Number(marep[15].number_of_ata);
 var hasil_part_16 = Number(marep[16].number_of_ata);
 var hasil_part_17 = Number(marep[17].number_of_ata);
 var hasil_part_18 = Number(marep[18].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata,
 marep[12].ata,
 marep[13].ata,
 marep[14].ata,
 marep[15].ata,
 marep[16].ata,
 marep[17].ata,
 marep[18].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  },
  {
    name: part_17,
    y: hasil_part_17
  },
  {
    name: part_18,
    y: hasil_part_18
  }
  ]
}
]
});

}
else if (panjang_marep == 18) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  
 var part_12 = marep[12].ata;  
 var part_13 = marep[13].ata;  
 var part_14 = marep[14].ata;  
 var part_15 = marep[15].ata;  
 var part_16 = marep[16].ata;  
 var part_17 = marep[17].ata;  



 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);
 var hasil_part_12 = Number(marep[12].number_of_ata);
 var hasil_part_13 = Number(marep[13].number_of_ata);
 var hasil_part_14 = Number(marep[14].number_of_ata);
 var hasil_part_15 = Number(marep[15].number_of_ata);
 var hasil_part_16 = Number(marep[16].number_of_ata);
 var hasil_part_17 = Number(marep[17].number_of_ata);


 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata,
 marep[12].ata,
 marep[13].ata,
 marep[14].ata,
 marep[15].ata,
 marep[16].ata,
 marep[17].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  },
  {
    name: part_17,
    y: hasil_part_17
  }
  ]
}
]
});

}
else if (panjang_marep == 17) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  
 var part_12 = marep[12].ata;  
 var part_13 = marep[13].ata;  
 var part_14 = marep[14].ata;  
 var part_15 = marep[15].ata;  
 var part_16 = marep[16].ata;  


 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);
 var hasil_part_12 = Number(marep[12].number_of_ata);
 var hasil_part_13 = Number(marep[13].number_of_ata);
 var hasil_part_14 = Number(marep[14].number_of_ata);
 var hasil_part_15 = Number(marep[15].number_of_ata);
 var hasil_part_16 = Number(marep[16].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata,
 marep[12].ata,
 marep[13].ata,
 marep[14].ata,
 marep[15].ata,
 marep[16].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  }
  ]
}
]
});

}
else if (panjang_marep == 16) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  
 var part_12 = marep[12].ata;  
 var part_13 = marep[13].ata;  
 var part_14 = marep[14].ata;  
 var part_15 = marep[15].ata;  
 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);
 var hasil_part_12 = Number(marep[12].number_of_ata);
 var hasil_part_13 = Number(marep[13].number_of_ata);
 var hasil_part_14 = Number(marep[14].number_of_ata);
 var hasil_part_15 = Number(marep[15].number_of_ata);
 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata,
 marep[12].ata,
 marep[13].ata,
 marep[14].ata,
 marep[15].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },

  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  }
  ]
}
]
});

}
else if (panjang_marep == 15) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  
 var part_12 = marep[12].ata;  
 var part_13 = marep[13].ata;  
 var part_14 = marep[14].ata;  
 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);
 var hasil_part_12 = Number(marep[12].number_of_ata);
 var hasil_part_13 = Number(marep[13].number_of_ata);
 var hasil_part_14 = Number(marep[14].number_of_ata);
 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata,
 marep[12].ata,
 marep[13].ata,
 marep[14].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },

  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  }
  ]
}
]
});

}
else if (panjang_marep == 14) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  
 var part_12 = marep[12].ata;  
 var part_13 = marep[13].ata;  

 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);
 var hasil_part_12 = Number(marep[12].number_of_ata);
 var hasil_part_13 = Number(marep[13].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata,
 marep[12].ata,
 marep[13].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  }
  ]
}
]
});

}
else if (panjang_marep == 13) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  
 var part_12 = marep[12].ata;  

 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);
 var hasil_part_12 = Number(marep[12].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata,
 marep[12].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_12,
    y: hasil_part_12
  }
  ]
}
]
});

}
else if (panjang_marep == 12) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  

 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);


 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  }
  ]
}
]
});

}
else if (panjang_marep == 11) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);



 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  }
  ]
}
]
});

}
else if (panjang_marep == 10) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  }
  ]
}
]
});
}
else if (panjang_marep == 9) {
  var part_0 = marep[0].ata;
  var part_1 = marep[1].ata;
  var part_2 = marep[2].ata;
  var part_3 = marep[3].ata;
  var part_4 = marep[4].ata;  
  var part_5 = marep[5].ata;  
  var part_6 = marep[6].ata;  
  var part_7 = marep[7].ata;  
  var part_8 = marep[8].ata;  

  var hasil_part_0 = Number(marep[0].number_of_ata);
  var hasil_part_1 = Number(marep[1].number_of_ata);
  var hasil_part_2 = Number(marep[2].number_of_ata);
  var hasil_part_3 = Number(marep[3].number_of_ata);
  var hasil_part_4 = Number(marep[4].number_of_ata);
  var hasil_part_5 = Number(marep[5].number_of_ata);
  var hasil_part_6 = Number(marep[6].number_of_ata);
  var hasil_part_7 = Number(marep[7].number_of_ata);
  var hasil_part_8 = Number(marep[8].number_of_ata);

  var mr = 
  [
  marep[0].ata,
  marep[1].ata,
  marep[2].ata,
  marep[3].ata,
  marep[4].ata,
  marep[5].ata,
  marep[6].ata,
  marep[7].ata,
  marep[8].ata
  ];

  Highcharts.chart('cret', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP MAREP - ' + judul
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number of Marep'
      },
       showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#marep_modal').modal('show');
             if ($.fn.dataTable.isDataTable('#marep_table')) {
              var table = $("#marep_table").DataTable();
              table.destroy();
              $('#marep_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = mr[category];
            var pencarian_hasil = '<thead id ="sakj">' +
            '<th class="text-center">No</th>' +
            '<th class="text-center">Date</th>' +
            '<th class="text-center">Sequence</th>' +
            '<th class="text-center">Notification Number</th>' +
            '<th class="text-center">A/C Type</th>' +
            '<th class="text-center">A/C Reg</th>' +
            '<th class="text-center">StaDep</th>' +
            '<th class="text-center">StaArr</th>' +
            '<th class="text-center">Flight No </th>' +
            '<th class="text-center">ATA</th>' +
            '<th class="text-center">SUB ATA</th>' +
            '<th class="text-center">Problem</th>' +
            '<th class="text-center">Rectification</th>' +
            '<th class="text-center">Coding</th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead>' +
            '<tbody id="enakeun"></tbody>';
            $('#marep_table').append(pencarian_hasil);
            var table = $("#marep_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'SEQ'
            },
            {
              data: 'Notification'
            },
            {
              data: 'ac'
            },
            {
              data: 'REG'
            },
            {
              data: 'STADEP'
            },
            {
              data: 'STAARR'
            },
            {
              data: 'FN'
            },
            {
              data: 'ATA'
            },
            {
              data: 'SUBATA'
            },
            {
              data: 'PROBLEM'
            },
            {
              data: 'ACTION'
            }, 
            {
              data: 'PirepMarep'
            },
            {
              data: 'act'
            },
            ]
          });
          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Marep",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_8,
      y: hasil_part_8
    }
    ]
  }
  ]
});


}
else if (panjang_marep == 8) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  

 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  }
  ]
}
]
});

}
else if (panjang_marep == 7) {
  var part_0 = marep[0].ata;
  var part_1 = marep[1].ata;
  var part_2 = marep[2].ata;
  var part_3 = marep[3].ata;
  var part_4 = marep[4].ata;  
  var part_5 = marep[5].ata;  
  var part_6 = marep[6].ata;  

  var hasil_part_0 = Number(marep[0].number_of_ata);
  var hasil_part_1 = Number(marep[1].number_of_ata);
  var hasil_part_2 = Number(marep[2].number_of_ata);
  var hasil_part_3 = Number(marep[3].number_of_ata);
  var hasil_part_4 = Number(marep[4].number_of_ata);
  var hasil_part_5 = Number(marep[5].number_of_ata);
  var hasil_part_6 = Number(marep[6].number_of_ata);

  var mr = 
  [
  marep[0].ata,
  marep[1].ata,
  marep[2].ata,
  marep[3].ata,
  marep[4].ata,
  marep[5].ata,
  marep[6].ata
  ];

  Highcharts.chart('cret', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP MAREP - ' + judul
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number of Marep'
      },
       showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#marep_modal').modal('show');
             if ($.fn.dataTable.isDataTable('#marep_table')) {
              var table = $("#marep_table").DataTable();
              table.destroy();
              $('#marep_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = mr[category];
            var pencarian_hasil = '<thead id ="sakj">' +
            '<th class="text-center">No</th>' +
            '<th class="text-center">Date</th>' +
            '<th class="text-center">Sequence</th>' +
            '<th class="text-center">Notification Number</th>' +
            '<th class="text-center">A/C Type</th>' +
            '<th class="text-center">A/C Reg</th>' +
            '<th class="text-center">StaDep</th>' +
            '<th class="text-center">StaArr</th>' +
            '<th class="text-center">Flight No </th>' +
            '<th class="text-center">ATA</th>' +
            '<th class="text-center">SUB ATA</th>' +
            '<th class="text-center">Problem</th>' +
            '<th class="text-center">Rectification</th>' +
            '<th class="text-center">Coding</th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead>' +
            '<tbody id="enakeun"></tbody>';
            $('#marep_table').append(pencarian_hasil);
            var table = $("#marep_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'SEQ'
            },
            {
              data: 'Notification'
            },
            {
              data: 'ac'
            },
            {
              data: 'REG'
            },
            {
              data: 'STADEP'
            },
            {
              data: 'STAARR'
            },
            {
              data: 'FN'
            },
            {
              data: 'ATA'
            },
            {
              data: 'SUBATA'
            },
            {
              data: 'PROBLEM'
            },
            {
              data: 'ACTION'
            }, 
            {
              data: 'PirepMarep'
            },
            {
              data: 'act'
            },
            ]
          });
          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Marep",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    }
    ]
  }
  ]
});

}
else if (panjang_marep == 6) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  }
  ]
}
]
});


}
else if (panjang_marep == 4) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;

 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  }
  ]
}
]
});

}
else if (panjang_marep == 3) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;

 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  }
  ]
}
]
});

}
else if (panjang_marep == 2) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;


 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  }
  ]
}
]
});

}
else if (panjang_marep == 1) {

 var part_0 = marep[0].ata;



 var hasil_part_0 = Number(marep[0].number_of_ata);

 var mr = 
 [
 marep[0].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  }
  ]
}
]
});

}

else if (panjang_marep == 5) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata
 ];
 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  }
  ]
}
]
});
}


}
var pirep         =  data.arr_pirep;
var panjang_pirep = Number(data.pirep_cnt);
if (pirep !== "") {
  if (panjang_pirep == 20) {
   var part_0 = pirep[0].ata;
   var part_1 = pirep[1].ata;
   var part_2 = pirep[2].ata;
   var part_3 = pirep[3].ata;
   var part_4 = pirep[4].ata;
   var part_5 = pirep[5].ata;
   var part_6 = pirep[6].ata;
   var part_7 = pirep[7].ata;
   var part_8 = pirep[8].ata;
   var part_9 = pirep[9].ata;
   var part_10 = pirep[10].ata;
   var part_11 = pirep[11].ata;
   var part_12 = pirep[12].ata;
   var part_13 = pirep[13].ata;
   var part_14 = pirep[14].ata;
   var part_15 = pirep[15].ata;
   var part_16 = pirep[16].ata;
   var part_17 = pirep[17].ata;
   var part_18 = pirep[18].ata;
   var part_19 = pirep[19].ata;
   var hasil_part_0 = Number(pirep[0].number_of_ata);
   var hasil_part_1 = Number(pirep[1].number_of_ata);
   var hasil_part_2 = Number(pirep[2].number_of_ata);
   var hasil_part_3 = Number(pirep[3].number_of_ata);
   var hasil_part_4 = Number(pirep[4].number_of_ata);
   var hasil_part_5 = Number(pirep[5].number_of_ata);
   var hasil_part_6 = Number(pirep[6].number_of_ata);
   var hasil_part_7 = Number(pirep[7].number_of_ata);
   var hasil_part_8 = Number(pirep[8].number_of_ata);
   var hasil_part_9 = Number(pirep[9].number_of_ata);
   var hasil_part_10 = Number(pirep[10].number_of_ata);
   var hasil_part_11 = Number(pirep[11].number_of_ata);
   var hasil_part_12 = Number(pirep[12].number_of_ata);
   var hasil_part_13 = Number(pirep[13].number_of_ata);
   var hasil_part_14 = Number(pirep[14].number_of_ata);
   var hasil_part_15 = Number(pirep[15].number_of_ata);
   var hasil_part_16 = Number(pirep[16].number_of_ata);
   var hasil_part_17 = Number(pirep[17].number_of_ata);
   var hasil_part_18 = Number(pirep[18].number_of_ata);
   var hasil_part_19 = Number(pirep[19].number_of_ata);
   var zr = [
   pirep[0].ata,
   pirep[1].ata,
   pirep[2].ata,
   pirep[3].ata,
   pirep[4].ata,
   pirep[5].ata,
   pirep[6].ata,
   pirep[7].ata,
   pirep[8].ata,
   pirep[9].ata,
   pirep[10].ata,
   pirep[11].ata,
   pirep[12].ata,
   pirep[13].ata,
   pirep[14].ata,
   pirep[15].ata,
   pirep[16].ata,
   pirep[17].ata,
   pirep[18].ata,
   pirep[19].ata
   ];

   Highcharts.chart('crut', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP PIREP - ' + judul
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number of Pirep'
      },
       showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal2').modal('show');
             if ($.fn.dataTable.isDataTable('#pirep_table')) {
              var table = $("#pirep_table").DataTable();
              table.destroy();
              $('#pirep_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = zr[category];
            var pencarian_hasil = '<thead id ="sakj">' +
            '<th class="text-center">No</th>' +
            '<th class="text-center">Date</th>' +
            '<th class="text-center">Sequence</th>' +
            '<th class="text-center">Notification Number</th>' +
            '<th class="text-center">A/C Type</th>' +
            '<th class="text-center">A/C Reg</th>' +
            '<th class="text-center">StaDep</th>' +
            '<th class="text-center">StaArr</th>' +
            '<th class="text-center">Flight No </th>' +
            '<th class="text-center">ATA</th>' +
            '<th class="text-center">SUB ATA</th>' +
            '<th class="text-center">Problem</th>' +
            '<th class="text-center">Rectification</th>' +
            '<th class="text-center">Coding</th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead>' +
            '<tbody id="enakeun"></tbody>';
            $('#pirep_table').append(pencarian_hasil);
            var table = $("#pirep_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'SEQ'
            },
            {
              data: 'Notification'
            },
            {
              data: 'ac'
            },
            {
              data: 'REG'
            },
            {
              data: 'STADEP'
            },
            {
              data: 'STAARR'
            },
            {
              data: 'FN'
            },
            {
              data: 'ATA'
            },
            {
              data: 'SUBATA'
            },
            {
              data: 'PROBLEM'
            },
            {
              data: 'ACTION'
            }, 
            {
              data: 'PirepMarep'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Pirep",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_8,
      y: hasil_part_8
    },
    {
      name: part_9,
      y: hasil_part_9
    },
    {
      name: part_10,
      y: hasil_part_10
    },
    {
      name: part_11,
      y: hasil_part_11
    },
    {
      name: part_12,
      y: hasil_part_12
    },
    {
      name: part_13,
      y: hasil_part_13
    },
    {
      name: part_13,
      y: hasil_part_13
    },
    {
      name: part_14,
      y: hasil_part_14
    },
    {
      name: part_15,
      y: hasil_part_15
    },
    {
      name: part_16,
      y: hasil_part_16
    },
    {
      name: part_17,
      y: hasil_part_17
    },
    {
      name: part_18,
      y: hasil_part_18
    },
    {
      name: part_19,
      y: hasil_part_19
    }
    ]
  }
  ]
});

} 
else if (panjang_pirep == 19) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var part_12 = pirep[12].ata;
 var part_13 = pirep[13].ata;
 var part_14 = pirep[14].ata;
 var part_15 = pirep[15].ata;
 var part_16 = pirep[16].ata;
 var part_17 = pirep[17].ata;
 var part_18 = pirep[18].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var hasil_part_12 = Number(pirep[12].number_of_ata);
 var hasil_part_13 = Number(pirep[13].number_of_ata);
 var hasil_part_14 = Number(pirep[14].number_of_ata);
 var hasil_part_15 = Number(pirep[15].number_of_ata);
 var hasil_part_16 = Number(pirep[16].number_of_ata);
 var hasil_part_17 = Number(pirep[17].number_of_ata);
 var hasil_part_18 = Number(pirep[18].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata,
 pirep[12].ata,
 pirep[13].ata,
 pirep[14].ata,
 pirep[15].ata,
 pirep[16].ata,
 pirep[17].ata,
 pirep[18].ata
 ];

 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  },
  {
    name: part_17,
    y: hasil_part_17
  },
  {
    name: part_18,
    y: hasil_part_18
  }
  ]
}
]
});

}
else if (panjang_pirep == 18) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var part_12 = pirep[12].ata;
 var part_13 = pirep[13].ata;
 var part_14 = pirep[14].ata;
 var part_15 = pirep[15].ata;
 var part_16 = pirep[16].ata;
 var part_17 = pirep[17].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var hasil_part_12 = Number(pirep[12].number_of_ata);
 var hasil_part_13 = Number(pirep[13].number_of_ata);
 var hasil_part_14 = Number(pirep[14].number_of_ata);
 var hasil_part_15 = Number(pirep[15].number_of_ata);
 var hasil_part_16 = Number(pirep[16].number_of_ata);
 var hasil_part_17 = Number(pirep[17].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata,
 pirep[12].ata,
 pirep[13].ata,
 pirep[14].ata,
 pirep[15].ata,
 pirep[16].ata,
 pirep[17].ata
 ];

 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  },
  {
    name: part_17,
    y: hasil_part_17
  }
  ]
}
]
});


}
else if (panjang_pirep == 17) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var part_12 = pirep[12].ata;
 var part_13 = pirep[13].ata;
 var part_14 = pirep[14].ata;
 var part_15 = pirep[15].ata;
 var part_16 = pirep[16].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var hasil_part_12 = Number(pirep[12].number_of_ata);
 var hasil_part_13 = Number(pirep[13].number_of_ata);
 var hasil_part_14 = Number(pirep[14].number_of_ata);
 var hasil_part_15 = Number(pirep[15].number_of_ata);
 var hasil_part_16 = Number(pirep[16].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata,
 pirep[12].ata,
 pirep[13].ata,
 pirep[14].ata,
 pirep[15].ata,
 pirep[16].ata
 ];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  }
  ]
}
]
});
}
else if (panjang_pirep == 16) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var part_12 = pirep[12].ata;
 var part_13 = pirep[13].ata;
 var part_14 = pirep[14].ata;
 var part_15 = pirep[15].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var hasil_part_12 = Number(pirep[12].number_of_ata);
 var hasil_part_13 = Number(pirep[13].number_of_ata);
 var hasil_part_14 = Number(pirep[14].number_of_ata);
 var hasil_part_15 = Number(pirep[15].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata,
 pirep[12].ata,
 pirep[13].ata,
 pirep[14].ata,
 pirep[15].ata
 ];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  }
  ]
}
]
});

}
else if (panjang_pirep == 15) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var part_12 = pirep[12].ata;
 var part_13 = pirep[13].ata;
 var part_14 = pirep[14].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var hasil_part_12 = Number(pirep[12].number_of_ata);
 var hasil_part_13 = Number(pirep[13].number_of_ata);
 var hasil_part_14 = Number(pirep[14].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata,
 pirep[12].ata,
 pirep[13].ata,
 pirep[14].ata
 ];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  }
  ]
}
]
});
}
else if (panjang_pirep == 14) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var part_12 = pirep[12].ata;
 var part_13 = pirep[13].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var hasil_part_12 = Number(pirep[12].number_of_ata);
 var hasil_part_13 = Number(pirep[13].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata,
 pirep[12].ata,
 pirep[13].ata
 ];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_13,
    y: hasil_part_13
  }
  ]
}
]
});
}
else if (panjang_pirep == 13) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var part_12 = pirep[12].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var hasil_part_12 = Number(pirep[12].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata,
 pirep[12].ata
 ];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  }
  ]
}
]
});
}
else if (panjang_pirep == 12) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata
 ];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  }
  ]
}
]
});
}
else if (panjang_pirep == 11) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata
 ];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  }
  ]
}
]
});

}
else if (panjang_pirep == 10) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata,pirep[3].ata,pirep[4].ata,pirep[5].ata,pirep[6].ata,pirep[7].ata,pirep[8].ata,pirep[9].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  }
  ]
}
]
});
}else if (panjang_pirep == 9) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata,pirep[3].ata,pirep[4].ata,pirep[5].ata,pirep[6].ata,pirep[7].ata,pirep[8].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  }
  ]
}
]
});
} else if (panjang_pirep == 8) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata,pirep[3].ata,pirep[4].ata,pirep[5].ata,pirep[6].ata,pirep[7].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  }
  ]
}
]
});
} else if (panjang_pirep == 7) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata,pirep[3].ata,pirep[4].ata,pirep[5].ata,pirep[6].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  }
  ]
}
]
});

} else if (panjang_pirep == 6) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata,pirep[3].ata,pirep[4].ata,pirep[5].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  }
  ]
}
]
});
} else if (panjang_pirep == 5) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata,pirep[3].ata,pirep[4].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  }
  ]
}
]
});

} else if (panjang_pirep == 4) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata,pirep[3].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  }
  ]
}
]
});

} else if (panjang_pirep == 3) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  }
  ]
}
]
});

} else if (panjang_pirep == 2) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  }
  ]
}
]
});

} else if (panjang_pirep == 1) {
 var part_0 = pirep[0].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var zr = [pirep[0].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  }
  ]
}
]
});
}
}

var delay =  data.arr_delay;
var panjang_delay = Number(data.delay_cnt);
if (delay !== "") {
 if (panjang_delay == 20) {
   var part_0 = delay[0].ATAtdm;
   var part_1 = delay[1].ATAtdm;
   var part_2 = delay[2].ATAtdm;
   var part_3 = delay[3].ATAtdm;
   var part_4 = delay[4].ATAtdm;
   var part_5 = delay[5].ATAtdm;
   var part_6 = delay[6].ATAtdm;
   var part_7 = delay[7].ATAtdm;
   var part_8 = delay[8].ATAtdm;
   var part_9 = delay[9].ATAtdm;
   var part_10 = delay[10].ATAtdm;
   var part_11 = delay[11].ATAtdm;
   var part_12 = delay[12].ATAtdm;
   var part_13 = delay[13].ATAtdm;
   var part_14 = delay[14].ATAtdm;
   var part_15 = delay[15].ATAtdm;
   var part_16 = delay[16].ATAtdm;
   var part_17 = delay[17].ATAtdm;
   var part_18 = delay[18].ATAtdm;
   var part_19 = delay[19].ATAtdm;
   var hasil_part_0 = Number(delay[0].number_of_ata1);
   var hasil_part_1 = Number(delay[1].number_of_ata1);
   var hasil_part_2 = Number(delay[2].number_of_ata1);
   var hasil_part_3 = Number(delay[3].number_of_ata1);
   var hasil_part_4 = Number(delay[4].number_of_ata1);
   var hasil_part_5 = Number(delay[5].number_of_ata1);
   var hasil_part_6 = Number(delay[6].number_of_ata1);
   var hasil_part_7 = Number(delay[7].number_of_ata1);
   var hasil_part_8 = Number(delay[8].number_of_ata1);
   var hasil_part_9 = Number(delay[9].number_of_ata1);
   var hasil_part_10 = Number(delay[10].number_of_ata1);
   var hasil_part_11 = Number(delay[11].number_of_ata1);
   var hasil_part_12 = Number(delay[12].number_of_ata1);
   var hasil_part_13 = Number(delay[13].number_of_ata1);
   var hasil_part_14 = Number(delay[14].number_of_ata1);
   var hasil_part_15 = Number(delay[15].number_of_ata1);
   var hasil_part_16 = Number(delay[16].number_of_ata1);
   var hasil_part_17 = Number(delay[17].number_of_ata1);
   var hasil_part_18 = Number(delay[18].number_of_ata1);
   var hasil_part_19 = Number(delay[19].number_of_ata1);

   var pr = [
   delay[0].ATAtdm,
   delay[1].ATAtdm,
   delay[2].ATAtdm,
   delay[3].ATAtdm,
   delay[4].ATAtdm,
   delay[5].ATAtdm,
   delay[6].ATAtdm,
   delay[7].ATAtdm,
   delay[8].ATAtdm,
   delay[9].ATAtdm,
   delay[10].ATAtdm,
   delay[11].ATAtdm,
   delay[12].ATAtdm,
   delay[13].ATAtdm,
   delay[14].ATAtdm,
   delay[15].ATAtdm,
   delay[16].ATAtdm,
   delay[17].ATAtdm,
   delay[18].ATAtdm,
   delay[19].ATAtdm
   ];

   Highcharts.chart('crot', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP DELAY - ' + judul
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number of Delay'
      },
       showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal').modal('show');
             if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center"># </th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },


            ]
          });
          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_8,
      y: hasil_part_8
    },
    {
      name: part_9,
      y: hasil_part_9
    },
    {
      name: part_10,
      y: hasil_part_10
    },
    {
      name: part_11,
      y: hasil_part_11
    },
    {
      name: part_12,
      y: hasil_part_12
    },
    {
      name: part_13,
      y: hasil_part_13
    },
    {
      name: part_14,
      y: hasil_part_14
    },
    {
      name: part_15,
      y: hasil_part_15
    },
    {
      name: part_16,
      y: hasil_part_16
    },
    {
      name: part_17,
      y: hasil_part_17
    },
    {
      name: part_18,
      y: hasil_part_18
    },
    {
      name: part_19,
      y: hasil_part_19
    }
    ]
  }
  ]
});

} else if (panjang_delay == 19) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var part_12 = delay[12].ATAtdm;
 var part_13 = delay[13].ATAtdm;
 var part_14 = delay[14].ATAtdm;
 var part_15 = delay[15].ATAtdm;
 var part_16 = delay[16].ATAtdm;
 var part_17 = delay[17].ATAtdm;
 var part_18 = delay[18].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var hasil_part_12 = Number(delay[12].number_of_ata1);
 var hasil_part_13 = Number(delay[13].number_of_ata1);
 var hasil_part_14 = Number(delay[14].number_of_ata1);
 var hasil_part_15 = Number(delay[15].number_of_ata1);
 var hasil_part_16 = Number(delay[16].number_of_ata1);
 var hasil_part_17 = Number(delay[17].number_of_ata1);
 var hasil_part_18 = Number(delay[18].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm,
 delay[12].ATAtdm,
 delay[13].ATAtdm,
 delay[14].ATAtdm,
 delay[15].ATAtdm,
 delay[16].ATAtdm,
 delay[17].ATAtdm,
 delay[18].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  },
  {
    name: part_17,
    y: hasil_part_17
  },
  {
    name: part_18,
    y: hasil_part_18
  }
  ]
}
]
});

} else if (panjang_delay == 18) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var part_12 = delay[12].ATAtdm;
 var part_13 = delay[13].ATAtdm;
 var part_14 = delay[14].ATAtdm;
 var part_15 = delay[15].ATAtdm;
 var part_16 = delay[16].ATAtdm;
 var part_17 = delay[17].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var hasil_part_12 = Number(delay[12].number_of_ata1);
 var hasil_part_13 = Number(delay[13].number_of_ata1);
 var hasil_part_14 = Number(delay[14].number_of_ata1);
 var hasil_part_15 = Number(delay[15].number_of_ata1);
 var hasil_part_16 = Number(delay[16].number_of_ata1);
 var hasil_part_17 = Number(delay[17].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm,
 delay[12].ATAtdm,
 delay[13].ATAtdm,
 delay[14].ATAtdm,
 delay[15].ATAtdm,
 delay[16].ATAtdm,
 delay[17].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  },
  {
    name: part_17,
    y: hasil_part_17
  }
  ]
}
]
});

} else if (panjang_delay == 17) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var part_12 = delay[12].ATAtdm;
 var part_13 = delay[13].ATAtdm;
 var part_14 = delay[14].ATAtdm;
 var part_15 = delay[15].ATAtdm;
 var part_16 = delay[16].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var hasil_part_12 = Number(delay[12].number_of_ata1);
 var hasil_part_13 = Number(delay[13].number_of_ata1);
 var hasil_part_14 = Number(delay[14].number_of_ata1);
 var hasil_part_15 = Number(delay[15].number_of_ata1);
 var hasil_part_16 = Number(delay[16].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm,
 delay[12].ATAtdm,
 delay[13].ATAtdm,
 delay[14].ATAtdm,
 delay[15].ATAtdm,
 delay[16].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  }
  ]
}
]
});
} else if (panjang_delay == 16) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var part_12 = delay[12].ATAtdm;
 var part_13 = delay[13].ATAtdm;
 var part_14 = delay[14].ATAtdm;
 var part_15 = delay[15].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var hasil_part_12 = Number(delay[12].number_of_ata1);
 var hasil_part_13 = Number(delay[13].number_of_ata1);
 var hasil_part_14 = Number(delay[14].number_of_ata1);
 var hasil_part_15 = Number(delay[15].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm,
 delay[12].ATAtdm,
 delay[13].ATAtdm,
 delay[14].ATAtdm,
 delay[15].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  }
  ]
}
]
});
} else if (panjang_delay == 15) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var part_12 = delay[12].ATAtdm;
 var part_13 = delay[13].ATAtdm;
 var part_14 = delay[14].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var hasil_part_12 = Number(delay[12].number_of_ata1);
 var hasil_part_13 = Number(delay[13].number_of_ata1);
 var hasil_part_14 = Number(delay[14].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm,
 delay[12].ATAtdm,
 delay[13].ATAtdm,
 delay[14].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  }
  ]
}
]
});
} else if (panjang_delay == 14) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var part_12 = delay[12].ATAtdm;
 var part_13 = delay[13].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var hasil_part_12 = Number(delay[12].number_of_ata1);
 var hasil_part_13 = Number(delay[13].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm,
 delay[12].ATAtdm,
 delay[13].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  }
  ]
}
]
});
} else if (panjang_delay == 13) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var part_12 = delay[12].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var hasil_part_12 = Number(delay[12].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm,
 delay[12].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  }
  ]
}
]
});
} else if (panjang_delay == 12) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  }
  ]
}
]
});

} else if (panjang_delay == 11) {
  var part_0 = delay[0].ATAtdm;
  var part_1 = delay[1].ATAtdm;
  var part_2 = delay[2].ATAtdm;
  var part_3 = delay[3].ATAtdm;
  var part_4 = delay[4].ATAtdm;
  var part_5 = delay[5].ATAtdm;
  var part_6 = delay[6].ATAtdm;
  var part_7 = delay[7].ATAtdm;
  var part_8 = delay[8].ATAtdm;
  var part_9 = delay[9].ATAtdm;
  var part_10 = delay[10].ATAtdm;
  var hasil_part_0 = Number(delay[0].number_of_ata1);
  var hasil_part_1 = Number(delay[1].number_of_ata1);
  var hasil_part_2 = Number(delay[2].number_of_ata1);
  var hasil_part_3 = Number(delay[3].number_of_ata1);
  var hasil_part_4 = Number(delay[4].number_of_ata1);
  var hasil_part_5 = Number(delay[5].number_of_ata1);
  var hasil_part_6 = Number(delay[6].number_of_ata1);
  var hasil_part_7 = Number(delay[7].number_of_ata1);
  var hasil_part_8 = Number(delay[8].number_of_ata1);
  var hasil_part_9 = Number(delay[9].number_of_ata1);
  var hasil_part_10 = Number(delay[10].number_of_ata1);
  var pr = [
  delay[0].ATAtdm,
  delay[1].ATAtdm,
  delay[2].ATAtdm,
  delay[3].ATAtdm,
  delay[4].ATAtdm,
  delay[5].ATAtdm,
  delay[6].ATAtdm,
  delay[7].ATAtdm,
  delay[8].ATAtdm,
  delay[9].ATAtdm,
  delay[10].ATAtdm
  ];

  Highcharts.chart('crot', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP DELAY - ' + judul
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number of Delay'
      },
       showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal').modal('show');
             if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center"># </th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },


            ]
          });
          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_8,
      y: hasil_part_8
    },
    {
      name: part_9,
      y: hasil_part_9
    },
    {
      name: part_10,
      y: hasil_part_10
    }
    ]
  }
  ]
});

}  
else if (panjang_delay ==  10) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm,delay[3].ATAtdm,delay[4].ATAtdm,delay[5].ATAtdm,delay[6].ATAtdm,delay[7].ATAtdm,delay[8].ATAtdm,delay[9].ATAtdm];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  }
  ]
}
]
});
}  else if (panjang_delay == 9) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm,delay[3].ATAtdm,delay[4].ATAtdm,delay[5].ATAtdm,delay[6].ATAtdm,delay[7].ATAtdm,delay[8].ATAtdm];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
            $('#myModal').modal('show');
            if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_8,
      y: hasil_part_8
    }
    ]
  }
  ]
});

} else if (panjang_delay == 8) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm,delay[3].ATAtdm,delay[4].ATAtdm,delay[5].ATAtdm,delay[6].ATAtdm,delay[7].ATAtdm];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
            $('#myModal').modal('show');
            if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    }
    ]
  }
  ]
});

} else if (panjang_delay == 7) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm,delay[3].ATAtdm,delay[4].ATAtdm,delay[5].ATAtdm,delay[6].ATAtdm];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
            $('#myModal').modal('show');
            if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    }
    ]
  }
  ]
});

} else if (panjang_delay == 6) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm,delay[3].ATAtdm,delay[4].ATAtdm,delay[5].ATAtdm];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
            $('#myModal').modal('show');
            if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    }
    ]
  }
  ]
});

} else if (panjang_delay == 5) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm,delay[3].ATAtdm,delay[4].ATAtdm];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
            $('#myModal').modal('show');
            if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    }
    ]
  }
  ]
});

} else if (panjang_delay == 4) {
  var part_0 = delay[0].ATAtdm;
  var part_1 = delay[1].ATAtdm;
  var part_2 = delay[2].ATAtdm;
  var part_3 = delay[3].ATAtdm;
  var hasil_part_0 = Number(delay[0].number_of_ata1);
  var hasil_part_1 = Number(delay[1].number_of_ata1);
  var hasil_part_2 = Number(delay[2].number_of_ata1);
  var hasil_part_3 = Number(delay[3].number_of_ata1);
  var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm,delay[3].ATAtdm];
  Highcharts.chart('crot', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP DELAY - ' + judul
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number of Delay'
      },
       showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal').modal('show');
             if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });
          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    }
    ]
  }
  ]
});

} else if (panjang_delay == 3) {
  var part_0 = delay[0].ATAtdm;
  var part_1 = delay[1].ATAtdm;
  var part_2 = delay[2].ATAtdm;
  var hasil_part_0 = Number(delay[0].number_of_ata1);
  var hasil_part_1 = Number(delay[1].number_of_ata1);
  var hasil_part_2 = Number(delay[2].number_of_ata1);
  var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm];
  Highcharts.chart('crot', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP DELAY - ' + judul
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number of Delay'
      },
       showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal').modal('show');
             if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    }
    ]
  }
  ]
});

} else if (panjang_delay == 2) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var pr = [delay[0].ATAtdm,delay[1].ATAtdm];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
            $('#myModal').modal('show');
            if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0
    },
    {
      name: part_1,
      y: hasil_part_1
    }
    ]
  }
  ]
});
} else if (panjang_delay == 1) {
 var part_0 = delay[0].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var pr = [delay[0].ATAtdm];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  }
  ]
}
]
});

}

}


}

});

 $('body').addClass('mini-navbar');
    }
  })
  $(document).on("click", "#pareto_view", function () {
    $('#crut').html("");
    $('#crot').html("");
    $('#cret').html("");
    var actype = $('#actype').val();
    var acreg  = $('#acreg').val();
    var date_to = $('#date_to').val();
    var date_from = $('#date_from').val();
    var d_t = moment(date_to).format("DD MMMM YYYY");
    var d_f = moment(date_from).format("DD MMMM YYYY");
    var p_graph = $('#p_graph').val();
    var filter = $('#filter').val();
    var KeyProblem = $('#KeyProblem').val();
    var ata = $('#ata').val();
    if (actype === "" || actype == null) {
      alert('Please select A/C Type !');
      return false;
    }    if (date_to == '' || date_from == '') {
      alert('Please select Date To and Date From !');
      return false;
    }
    if (p_graph == '') {
      alert('Please select Graph settings for X-axis !');
      return false;
    }
    $('#pareto_view').attr('disabled', true);
    $('#pareto_view').html('Loading .. !');
    $.ajax({
     type:"POST",
     data:{actype:actype,acreg:acreg,date_to:date_to,date_from:date_from,p_graph:p_graph,ata:ata,filter:filter},
     url:"<?= base_url('Pareto/search') ?>",
     dataType: 'json',
     success: function(data){
      $('#pareto_view').attr('disabled', false);
      $('#pareto_view').html('<i class="fa fa-print"></i>&nbsp;&nbsp;DISPLAY REPORT');
      var judul         = actype + ' ' + acreg + ' ' + ata + ' ' + KeyProblem ;
      var sql_pirep     = data.sql_pirep;
      var sql_delay     = data.sql_delay;
      var marep         =  data.arr_marep;
      var panjang_marep =  Number(data.marep_cnt);

      if (marep !== "") {
        if (panjang_marep == 20) {
         var part_0 = marep[0].ata;
         var part_1 = marep[1].ata;
         var part_2 = marep[2].ata;
         var part_3 = marep[3].ata;
         var part_4 = marep[4].ata;  
         var part_5 = marep[5].ata;  
         var part_6 = marep[6].ata;  
         var part_7 = marep[7].ata;  
         var part_8 = marep[8].ata;  
         var part_9 = marep[9].ata;  
         var part_10 = marep[10].ata;  
         var part_11 = marep[11].ata;  
         var part_12 = marep[12].ata;  
         var part_13 = marep[13].ata;  
         var part_14 = marep[14].ata;  
         var part_15 = marep[15].ata;  
         var part_16 = marep[16].ata;  
         var part_17 = marep[17].ata;  
         var part_18 = marep[18].ata;  
         var part_19 = marep[17].ata; 

         var hasil_part_0 = Number(marep[0].number_of_ata);
         var hasil_part_1 = Number(marep[1].number_of_ata);
         var hasil_part_2 = Number(marep[2].number_of_ata);
         var hasil_part_3 = Number(marep[3].number_of_ata);
         var hasil_part_4 = Number(marep[4].number_of_ata);
         var hasil_part_5 = Number(marep[5].number_of_ata);
         var hasil_part_6 = Number(marep[6].number_of_ata);
         var hasil_part_7 = Number(marep[7].number_of_ata);
         var hasil_part_8 = Number(marep[8].number_of_ata);
         var hasil_part_9 = Number(marep[9].number_of_ata);
         var hasil_part_10 = Number(marep[10].number_of_ata);
         var hasil_part_11 = Number(marep[11].number_of_ata);
         var hasil_part_12 = Number(marep[12].number_of_ata);
         var hasil_part_13 = Number(marep[13].number_of_ata);
         var hasil_part_14 = Number(marep[14].number_of_ata);
         var hasil_part_15 = Number(marep[15].number_of_ata);
         var hasil_part_16 = Number(marep[16].number_of_ata);
         var hasil_part_17 = Number(marep[17].number_of_ata);
         var hasil_part_18 = Number(marep[18].number_of_ata);
         var hasil_part_19 = Number(marep[19].number_of_ata);


         var mr = 
         [
         marep[0].ata,
         marep[1].ata,
         marep[2].ata,
         marep[3].ata,
         marep[4].ata,
         marep[5].ata,
         marep[6].ata,
         marep[7].ata,
         marep[8].ata,
         marep[9].ata,
         marep[10].ata,
         marep[11].ata,
         marep[12].ata,
         marep[13].ata,
         marep[14].ata,
         marep[15].ata,
         marep[16].ata,
         marep[17].ata,
         marep[18].ata,
         marep[19].ata
         ];

         Highcharts.chart('cret', {
          chart: {
            type: 'column'
          },
          title: {
            text: 'TOP MAREP - ' + judul
          },
          subtitle: {
            text: 'Periode : ' + d_f + ' - ' + d_t
          },
          xAxis: {
            type: 'category'
          },
          yAxis: {
            title: {
              text: 'Number of Marep'
            },
             showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
          },
          legend: {
            enabled: false
          },
          plotOptions: {
            series: {
              borderWidth: 0,
              cursor: 'pointer',
              point: {
                events: {
                  click: function () {
                   $('#marep_modal').modal('show');
                   if ($.fn.dataTable.isDataTable('#marep_table')) {
                    var table = $("#marep_table").DataTable();
                    table.destroy();
                    $('#marep_table').html("");
                  }
                  var category = this.category;
                  var value_category = this.y;
                  var dikirim = mr[category];
                  var pencarian_hasil = '<thead id ="sakj">' +
                  '<th class="text-center">No</th>' +
                  '<th class="text-center">Date</th>' +
                  '<th class="text-center">Sequence</th>' +
                  '<th class="text-center">Notification Number</th>' +
                  '<th class="text-center">A/C Type</th>' +
                  '<th class="text-center">A/C Reg</th>' +
                  '<th class="text-center">StaDep</th>' +
                  '<th class="text-center">StaArr</th>' +
                  '<th class="text-center">Flight No </th>' +
                  '<th class="text-center">ATA</th>' +
                  '<th class="text-center">SUB ATA</th>' +
                  '<th class="text-center">Problem</th>' +
                  '<th class="text-center">Rectification</th>' +
                  '<th class="text-center">Coding</th>' +
                  '<th class="text-center">#</th>' +
                  '</tr></thead>' +
                  '<tbody id="enakeun"></tbody>';
                  $('#marep_table').append(pencarian_hasil);
                  var table = $("#marep_table").DataTable({
                    retrieve: true,
                    pagin: false,
                    dom: 'Bfrtip',
                    buttons: [
                    {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
                      columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
                    }
                  },
                  {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
                  exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
                  },
                  pageSize: 'LETTER'}
                  ],
                  ajax: {
                    "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
                    "type": "POST",
                    "data": {
                      "actype": actype,
                      "acreg": acreg,
                      "p_graph": p_graph,
                      "ata": ata,
                      "date_from": date_from,
                      "date_to": date_to,
                      "dikirim": dikirim,
                      "value_category": value_category,
                      "filter" : filter
                    },
                  },
                  'columns': [
                  {
                    data: 'no'
                  },
                  {
                    data: 'TargetDate'
                  },
                  {
                    data: 'SEQ'
                  },
                  {
                    data: 'Notification'
                  },
                  {
                    data: 'ac'
                  },
                  {
                    data: 'REG'
                  },
                  {
                    data: 'STADEP'
                  },
                  {
                    data: 'STAARR'
                  },
                  {
                    data: 'FN'
                  },
                  {
                    data: 'ATA'
                  },
                  {
                    data: 'SUBATA'
                  },
                  {
                    data: 'PROBLEM'
                  },
                  {
                    data: 'ACTION'
                  }, 
                  {
                    data: 'PirepMarep'
                  },
                  {
                    data: 'act'
                  },
                  ]
                });
                }
              }
            },
            dataLabels: {
              enabled: true,
              format: '{point.y}'
            }
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
        },
        series: [
        {
          name: "Number Of Marep",
          colorByPoint: true,
          data: [
          {
            name: part_0,
            y: hasil_part_0

          },
          {
            name: part_1,
            y: hasil_part_1

          },
          {
            name: part_2,
            y: hasil_part_2
          },
          {
            name: part_3,
            y: hasil_part_3
          },
          {
            name: part_4,
            y: hasil_part_4
          },
          {
            name: part_5,
            y: hasil_part_5
          },
          {
            name: part_6,
            y: hasil_part_6
          },
          {
            name: part_7,
            y: hasil_part_7
          },
          {
            name: part_7,
            y: hasil_part_7
          },
          {
            name: part_8,
            y: hasil_part_8
          },
          {
            name: part_9,
            y: hasil_part_9
          },
          {
            name: part_10,
            y: hasil_part_10
          },
          {
            name: part_11,
            y: hasil_part_11
          },
          {
            name: part_12,
            y: hasil_part_12
          },
          {
            name: part_12,
            y: hasil_part_12
          },
          {
            name: part_13,
            y: hasil_part_13
          },
          {
            name: part_14,
            y: hasil_part_14
          },
          {
            name: part_15,
            y: hasil_part_15
          },
          {
            name: part_16,
            y: hasil_part_16
          },
          {
            name: part_17,
            y: hasil_part_17
          },
          {
            name: part_18,
            y: hasil_part_18
          },
          {
            name: part_19,
            y: hasil_part_19
          }
          ]
        }
        ]
      });
} 

else if (panjang_marep == 19) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  
 var part_12 = marep[12].ata;  
 var part_13 = marep[13].ata;  
 var part_14 = marep[14].ata;  
 var part_15 = marep[15].ata;  
 var part_16 = marep[16].ata;  
 var part_17 = marep[17].ata;  
 var part_18 = marep[18].ata;  


 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);
 var hasil_part_12 = Number(marep[12].number_of_ata);
 var hasil_part_13 = Number(marep[13].number_of_ata);
 var hasil_part_14 = Number(marep[14].number_of_ata);
 var hasil_part_15 = Number(marep[15].number_of_ata);
 var hasil_part_16 = Number(marep[16].number_of_ata);
 var hasil_part_17 = Number(marep[17].number_of_ata);
 var hasil_part_18 = Number(marep[18].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata,
 marep[12].ata,
 marep[13].ata,
 marep[14].ata,
 marep[15].ata,
 marep[16].ata,
 marep[17].ata,
 marep[18].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  },
  {
    name: part_17,
    y: hasil_part_17
  },
  {
    name: part_18,
    y: hasil_part_18
  }
  ]
}
]
});

}
else if (panjang_marep == 18) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  
 var part_12 = marep[12].ata;  
 var part_13 = marep[13].ata;  
 var part_14 = marep[14].ata;  
 var part_15 = marep[15].ata;  
 var part_16 = marep[16].ata;  
 var part_17 = marep[17].ata;  



 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);
 var hasil_part_12 = Number(marep[12].number_of_ata);
 var hasil_part_13 = Number(marep[13].number_of_ata);
 var hasil_part_14 = Number(marep[14].number_of_ata);
 var hasil_part_15 = Number(marep[15].number_of_ata);
 var hasil_part_16 = Number(marep[16].number_of_ata);
 var hasil_part_17 = Number(marep[17].number_of_ata);


 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata,
 marep[12].ata,
 marep[13].ata,
 marep[14].ata,
 marep[15].ata,
 marep[16].ata,
 marep[17].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  },
  {
    name: part_17,
    y: hasil_part_17
  }
  ]
}
]
});

}
else if (panjang_marep == 17) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  
 var part_12 = marep[12].ata;  
 var part_13 = marep[13].ata;  
 var part_14 = marep[14].ata;  
 var part_15 = marep[15].ata;  
 var part_16 = marep[16].ata;  


 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);
 var hasil_part_12 = Number(marep[12].number_of_ata);
 var hasil_part_13 = Number(marep[13].number_of_ata);
 var hasil_part_14 = Number(marep[14].number_of_ata);
 var hasil_part_15 = Number(marep[15].number_of_ata);
 var hasil_part_16 = Number(marep[16].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata,
 marep[12].ata,
 marep[13].ata,
 marep[14].ata,
 marep[15].ata,
 marep[16].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  }
  ]
}
]
});

}
else if (panjang_marep == 16) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  
 var part_12 = marep[12].ata;  
 var part_13 = marep[13].ata;  
 var part_14 = marep[14].ata;  
 var part_15 = marep[15].ata;  
 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);
 var hasil_part_12 = Number(marep[12].number_of_ata);
 var hasil_part_13 = Number(marep[13].number_of_ata);
 var hasil_part_14 = Number(marep[14].number_of_ata);
 var hasil_part_15 = Number(marep[15].number_of_ata);
 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata,
 marep[12].ata,
 marep[13].ata,
 marep[14].ata,
 marep[15].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },

  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  }
  ]
}
]
});

}
else if (panjang_marep == 15) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  
 var part_12 = marep[12].ata;  
 var part_13 = marep[13].ata;  
 var part_14 = marep[14].ata;  
 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);
 var hasil_part_12 = Number(marep[12].number_of_ata);
 var hasil_part_13 = Number(marep[13].number_of_ata);
 var hasil_part_14 = Number(marep[14].number_of_ata);
 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata,
 marep[12].ata,
 marep[13].ata,
 marep[14].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },

  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  }
  ]
}
]
});

}
else if (panjang_marep == 14) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  
 var part_12 = marep[12].ata;  
 var part_13 = marep[13].ata;  

 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);
 var hasil_part_12 = Number(marep[12].number_of_ata);
 var hasil_part_13 = Number(marep[13].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata,
 marep[12].ata,
 marep[13].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  }
  ]
}
]
});

}
else if (panjang_marep == 13) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  
 var part_12 = marep[12].ata;  

 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);
 var hasil_part_12 = Number(marep[12].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata,
 marep[12].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_12,
    y: hasil_part_12
  }
  ]
}
]
});

}
else if (panjang_marep == 12) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var part_11 = marep[11].ata;  

 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);
 var hasil_part_11 = Number(marep[11].number_of_ata);


 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata,
 marep[11].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  }
  ]
}
]
});

}
else if (panjang_marep == 11) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var part_10 = marep[10].ata;  
 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var hasil_part_10 = Number(marep[10].number_of_ata);



 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata,
 marep[10].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  }
  ]
}
]
});

}
else if (panjang_marep == 10) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  
 var part_8 = marep[8].ata;  
 var part_9 = marep[9].ata;  
 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);
 var hasil_part_8 = Number(marep[8].number_of_ata);
 var hasil_part_9 = Number(marep[9].number_of_ata);
 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata,
 marep[8].ata,
 marep[9].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  }
  ]
}
]
});
}
else if (panjang_marep == 9) {
  var part_0 = marep[0].ata;
  var part_1 = marep[1].ata;
  var part_2 = marep[2].ata;
  var part_3 = marep[3].ata;
  var part_4 = marep[4].ata;  
  var part_5 = marep[5].ata;  
  var part_6 = marep[6].ata;  
  var part_7 = marep[7].ata;  
  var part_8 = marep[8].ata;  

  var hasil_part_0 = Number(marep[0].number_of_ata);
  var hasil_part_1 = Number(marep[1].number_of_ata);
  var hasil_part_2 = Number(marep[2].number_of_ata);
  var hasil_part_3 = Number(marep[3].number_of_ata);
  var hasil_part_4 = Number(marep[4].number_of_ata);
  var hasil_part_5 = Number(marep[5].number_of_ata);
  var hasil_part_6 = Number(marep[6].number_of_ata);
  var hasil_part_7 = Number(marep[7].number_of_ata);
  var hasil_part_8 = Number(marep[8].number_of_ata);

  var mr = 
  [
  marep[0].ata,
  marep[1].ata,
  marep[2].ata,
  marep[3].ata,
  marep[4].ata,
  marep[5].ata,
  marep[6].ata,
  marep[7].ata,
  marep[8].ata
  ];

  Highcharts.chart('cret', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP MAREP - ' + judul
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number of Marep'
      },
       showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#marep_modal').modal('show');
             if ($.fn.dataTable.isDataTable('#marep_table')) {
              var table = $("#marep_table").DataTable();
              table.destroy();
              $('#marep_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = mr[category];
            var pencarian_hasil = '<thead id ="sakj">' +
            '<th class="text-center">No</th>' +
            '<th class="text-center">Date</th>' +
            '<th class="text-center">Sequence</th>' +
            '<th class="text-center">Notification Number</th>' +
            '<th class="text-center">A/C Type</th>' +
            '<th class="text-center">A/C Reg</th>' +
            '<th class="text-center">StaDep</th>' +
            '<th class="text-center">StaArr</th>' +
            '<th class="text-center">Flight No </th>' +
            '<th class="text-center">ATA</th>' +
            '<th class="text-center">SUB ATA</th>' +
            '<th class="text-center">Problem</th>' +
            '<th class="text-center">Rectification</th>' +
            '<th class="text-center">Coding</th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead>' +
            '<tbody id="enakeun"></tbody>';
            $('#marep_table').append(pencarian_hasil);
            var table = $("#marep_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'SEQ'
            },
            {
              data: 'Notification'
            },
            {
              data: 'ac'
            },
            {
              data: 'REG'
            },
            {
              data: 'STADEP'
            },
            {
              data: 'STAARR'
            },
            {
              data: 'FN'
            },
            {
              data: 'ATA'
            },
            {
              data: 'SUBATA'
            },
            {
              data: 'PROBLEM'
            },
            {
              data: 'ACTION'
            }, 
            {
              data: 'PirepMarep'
            },
            {
              data: 'act'
            },
            ]
          });
          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Marep",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_8,
      y: hasil_part_8
    }
    ]
  }
  ]
});


}
else if (panjang_marep == 8) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var part_6 = marep[6].ata;  
 var part_7 = marep[7].ata;  

 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var hasil_part_6 = Number(marep[6].number_of_ata);
 var hasil_part_7 = Number(marep[7].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata,
 marep[6].ata,
 marep[7].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_7,
    y: hasil_part_7
  }
  ]
}
]
});

}
else if (panjang_marep == 7) {
  var part_0 = marep[0].ata;
  var part_1 = marep[1].ata;
  var part_2 = marep[2].ata;
  var part_3 = marep[3].ata;
  var part_4 = marep[4].ata;  
  var part_5 = marep[5].ata;  
  var part_6 = marep[6].ata;  

  var hasil_part_0 = Number(marep[0].number_of_ata);
  var hasil_part_1 = Number(marep[1].number_of_ata);
  var hasil_part_2 = Number(marep[2].number_of_ata);
  var hasil_part_3 = Number(marep[3].number_of_ata);
  var hasil_part_4 = Number(marep[4].number_of_ata);
  var hasil_part_5 = Number(marep[5].number_of_ata);
  var hasil_part_6 = Number(marep[6].number_of_ata);

  var mr = 
  [
  marep[0].ata,
  marep[1].ata,
  marep[2].ata,
  marep[3].ata,
  marep[4].ata,
  marep[5].ata,
  marep[6].ata
  ];

  Highcharts.chart('cret', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP MAREP - ' + judul
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number of Marep'
      },
       showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#marep_modal').modal('show');
             if ($.fn.dataTable.isDataTable('#marep_table')) {
              var table = $("#marep_table").DataTable();
              table.destroy();
              $('#marep_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = mr[category];
            var pencarian_hasil = '<thead id ="sakj">' +
            '<th class="text-center">No</th>' +
            '<th class="text-center">Date</th>' +
            '<th class="text-center">Sequence</th>' +
            '<th class="text-center">Notification Number</th>' +
            '<th class="text-center">A/C Type</th>' +
            '<th class="text-center">A/C Reg</th>' +
            '<th class="text-center">StaDep</th>' +
            '<th class="text-center">StaArr</th>' +
            '<th class="text-center">Flight No </th>' +
            '<th class="text-center">ATA</th>' +
            '<th class="text-center">SUB ATA</th>' +
            '<th class="text-center">Problem</th>' +
            '<th class="text-center">Rectification</th>' +
            '<th class="text-center">Coding</th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead>' +
            '<tbody id="enakeun"></tbody>';
            $('#marep_table').append(pencarian_hasil);
            var table = $("#marep_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'SEQ'
            },
            {
              data: 'Notification'
            },
            {
              data: 'ac'
            },
            {
              data: 'REG'
            },
            {
              data: 'STADEP'
            },
            {
              data: 'STAARR'
            },
            {
              data: 'FN'
            },
            {
              data: 'ATA'
            },
            {
              data: 'SUBATA'
            },
            {
              data: 'PROBLEM'
            },
            {
              data: 'ACTION'
            }, 
            {
              data: 'PirepMarep'
            },
            {
              data: 'act'
            },
            ]
          });
          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Marep",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    }
    ]
  }
  ]
});

}
else if (panjang_marep == 6) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var part_5 = marep[5].ata;  
 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var hasil_part_5 = Number(marep[5].number_of_ata);
 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata,
 marep[5].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  }
  ]
}
]
});


}
else if (panjang_marep == 4) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;

 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  }
  ]
}
]
});

}
else if (panjang_marep == 3) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;

 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  }
  ]
}
]
});

}
else if (panjang_marep == 2) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;


 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);

 var mr = 
 [
 marep[0].ata,
 marep[1].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  }
  ]
}
]
});

}
else if (panjang_marep == 1) {

 var part_0 = marep[0].ata;



 var hasil_part_0 = Number(marep[0].number_of_ata);

 var mr = 
 [
 marep[0].ata
 ];

 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  }
  ]
}
]
});

}

else if (panjang_marep == 5) {
 var part_0 = marep[0].ata;
 var part_1 = marep[1].ata;
 var part_2 = marep[2].ata;
 var part_3 = marep[3].ata;
 var part_4 = marep[4].ata;  
 var hasil_part_0 = Number(marep[0].number_of_ata);
 var hasil_part_1 = Number(marep[1].number_of_ata);
 var hasil_part_2 = Number(marep[2].number_of_ata);
 var hasil_part_3 = Number(marep[3].number_of_ata);
 var hasil_part_4 = Number(marep[4].number_of_ata);
 var mr = 
 [
 marep[0].ata,
 marep[1].ata,
 marep[2].ata,
 marep[3].ata,
 marep[4].ata
 ];
 Highcharts.chart('cret', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP MAREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Marep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#marep_modal').modal('show');
           if ($.fn.dataTable.isDataTable('#marep_table')) {
            var table = $("#marep_table").DataTable();
            table.destroy();
            $('#marep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = mr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#marep_table').append(pencarian_hasil);
          var table = $("#marep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP MAREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP MAREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailMarep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Marep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  }
  ]
}
]
});
}


}
var pirep         =  data.arr_pirep;
var panjang_pirep = Number(data.pirep_cnt);
if (pirep !== "") {
  if (panjang_pirep == 20) {
   var part_0 = pirep[0].ata;
   var part_1 = pirep[1].ata;
   var part_2 = pirep[2].ata;
   var part_3 = pirep[3].ata;
   var part_4 = pirep[4].ata;
   var part_5 = pirep[5].ata;
   var part_6 = pirep[6].ata;
   var part_7 = pirep[7].ata;
   var part_8 = pirep[8].ata;
   var part_9 = pirep[9].ata;
   var part_10 = pirep[10].ata;
   var part_11 = pirep[11].ata;
   var part_12 = pirep[12].ata;
   var part_13 = pirep[13].ata;
   var part_14 = pirep[14].ata;
   var part_15 = pirep[15].ata;
   var part_16 = pirep[16].ata;
   var part_17 = pirep[17].ata;
   var part_18 = pirep[18].ata;
   var part_19 = pirep[19].ata;
   var hasil_part_0 = Number(pirep[0].number_of_ata);
   var hasil_part_1 = Number(pirep[1].number_of_ata);
   var hasil_part_2 = Number(pirep[2].number_of_ata);
   var hasil_part_3 = Number(pirep[3].number_of_ata);
   var hasil_part_4 = Number(pirep[4].number_of_ata);
   var hasil_part_5 = Number(pirep[5].number_of_ata);
   var hasil_part_6 = Number(pirep[6].number_of_ata);
   var hasil_part_7 = Number(pirep[7].number_of_ata);
   var hasil_part_8 = Number(pirep[8].number_of_ata);
   var hasil_part_9 = Number(pirep[9].number_of_ata);
   var hasil_part_10 = Number(pirep[10].number_of_ata);
   var hasil_part_11 = Number(pirep[11].number_of_ata);
   var hasil_part_12 = Number(pirep[12].number_of_ata);
   var hasil_part_13 = Number(pirep[13].number_of_ata);
   var hasil_part_14 = Number(pirep[14].number_of_ata);
   var hasil_part_15 = Number(pirep[15].number_of_ata);
   var hasil_part_16 = Number(pirep[16].number_of_ata);
   var hasil_part_17 = Number(pirep[17].number_of_ata);
   var hasil_part_18 = Number(pirep[18].number_of_ata);
   var hasil_part_19 = Number(pirep[19].number_of_ata);
   var zr = [
   pirep[0].ata,
   pirep[1].ata,
   pirep[2].ata,
   pirep[3].ata,
   pirep[4].ata,
   pirep[5].ata,
   pirep[6].ata,
   pirep[7].ata,
   pirep[8].ata,
   pirep[9].ata,
   pirep[10].ata,
   pirep[11].ata,
   pirep[12].ata,
   pirep[13].ata,
   pirep[14].ata,
   pirep[15].ata,
   pirep[16].ata,
   pirep[17].ata,
   pirep[18].ata,
   pirep[19].ata
   ];

   Highcharts.chart('crut', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP PIREP - ' + judul
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number of Pirep'
      },
       showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal2').modal('show');
             if ($.fn.dataTable.isDataTable('#pirep_table')) {
              var table = $("#pirep_table").DataTable();
              table.destroy();
              $('#pirep_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = zr[category];
            var pencarian_hasil = '<thead id ="sakj">' +
            '<th class="text-center">No</th>' +
            '<th class="text-center">Date</th>' +
            '<th class="text-center">Sequence</th>' +
            '<th class="text-center">Notification Number</th>' +
            '<th class="text-center">A/C Type</th>' +
            '<th class="text-center">A/C Reg</th>' +
            '<th class="text-center">StaDep</th>' +
            '<th class="text-center">StaArr</th>' +
            '<th class="text-center">Flight No </th>' +
            '<th class="text-center">ATA</th>' +
            '<th class="text-center">SUB ATA</th>' +
            '<th class="text-center">Problem</th>' +
            '<th class="text-center">Rectification</th>' +
            '<th class="text-center">Coding</th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead>' +
            '<tbody id="enakeun"></tbody>';
            $('#pirep_table').append(pencarian_hasil);
            var table = $("#pirep_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'SEQ'
            },
            {
              data: 'Notification'
            },
            {
              data: 'ac'
            },
            {
              data: 'REG'
            },
            {
              data: 'STADEP'
            },
            {
              data: 'STAARR'
            },
            {
              data: 'FN'
            },
            {
              data: 'ATA'
            },
            {
              data: 'SUBATA'
            },
            {
              data: 'PROBLEM'
            },
            {
              data: 'ACTION'
            }, 
            {
              data: 'PirepMarep'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Pirep",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_8,
      y: hasil_part_8
    },
    {
      name: part_9,
      y: hasil_part_9
    },
    {
      name: part_10,
      y: hasil_part_10
    },
    {
      name: part_11,
      y: hasil_part_11
    },
    {
      name: part_12,
      y: hasil_part_12
    },
    {
      name: part_13,
      y: hasil_part_13
    },
    {
      name: part_13,
      y: hasil_part_13
    },
    {
      name: part_14,
      y: hasil_part_14
    },
    {
      name: part_15,
      y: hasil_part_15
    },
    {
      name: part_16,
      y: hasil_part_16
    },
    {
      name: part_17,
      y: hasil_part_17
    },
    {
      name: part_18,
      y: hasil_part_18
    },
    {
      name: part_19,
      y: hasil_part_19
    }
    ]
  }
  ]
});

} 
else if (panjang_pirep == 19) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var part_12 = pirep[12].ata;
 var part_13 = pirep[13].ata;
 var part_14 = pirep[14].ata;
 var part_15 = pirep[15].ata;
 var part_16 = pirep[16].ata;
 var part_17 = pirep[17].ata;
 var part_18 = pirep[18].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var hasil_part_12 = Number(pirep[12].number_of_ata);
 var hasil_part_13 = Number(pirep[13].number_of_ata);
 var hasil_part_14 = Number(pirep[14].number_of_ata);
 var hasil_part_15 = Number(pirep[15].number_of_ata);
 var hasil_part_16 = Number(pirep[16].number_of_ata);
 var hasil_part_17 = Number(pirep[17].number_of_ata);
 var hasil_part_18 = Number(pirep[18].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata,
 pirep[12].ata,
 pirep[13].ata,
 pirep[14].ata,
 pirep[15].ata,
 pirep[16].ata,
 pirep[17].ata,
 pirep[18].ata
 ];

 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  },
  {
    name: part_17,
    y: hasil_part_17
  },
  {
    name: part_18,
    y: hasil_part_18
  }
  ]
}
]
});

}
else if (panjang_pirep == 18) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var part_12 = pirep[12].ata;
 var part_13 = pirep[13].ata;
 var part_14 = pirep[14].ata;
 var part_15 = pirep[15].ata;
 var part_16 = pirep[16].ata;
 var part_17 = pirep[17].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var hasil_part_12 = Number(pirep[12].number_of_ata);
 var hasil_part_13 = Number(pirep[13].number_of_ata);
 var hasil_part_14 = Number(pirep[14].number_of_ata);
 var hasil_part_15 = Number(pirep[15].number_of_ata);
 var hasil_part_16 = Number(pirep[16].number_of_ata);
 var hasil_part_17 = Number(pirep[17].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata,
 pirep[12].ata,
 pirep[13].ata,
 pirep[14].ata,
 pirep[15].ata,
 pirep[16].ata,
 pirep[17].ata
 ];

 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  },
  {
    name: part_17,
    y: hasil_part_17
  }
  ]
}
]
});


}
else if (panjang_pirep == 17) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var part_12 = pirep[12].ata;
 var part_13 = pirep[13].ata;
 var part_14 = pirep[14].ata;
 var part_15 = pirep[15].ata;
 var part_16 = pirep[16].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var hasil_part_12 = Number(pirep[12].number_of_ata);
 var hasil_part_13 = Number(pirep[13].number_of_ata);
 var hasil_part_14 = Number(pirep[14].number_of_ata);
 var hasil_part_15 = Number(pirep[15].number_of_ata);
 var hasil_part_16 = Number(pirep[16].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata,
 pirep[12].ata,
 pirep[13].ata,
 pirep[14].ata,
 pirep[15].ata,
 pirep[16].ata
 ];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  }
  ]
}
]
});
}
else if (panjang_pirep == 16) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var part_12 = pirep[12].ata;
 var part_13 = pirep[13].ata;
 var part_14 = pirep[14].ata;
 var part_15 = pirep[15].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var hasil_part_12 = Number(pirep[12].number_of_ata);
 var hasil_part_13 = Number(pirep[13].number_of_ata);
 var hasil_part_14 = Number(pirep[14].number_of_ata);
 var hasil_part_15 = Number(pirep[15].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata,
 pirep[12].ata,
 pirep[13].ata,
 pirep[14].ata,
 pirep[15].ata
 ];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  }
  ]
}
]
});

}
else if (panjang_pirep == 15) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var part_12 = pirep[12].ata;
 var part_13 = pirep[13].ata;
 var part_14 = pirep[14].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var hasil_part_12 = Number(pirep[12].number_of_ata);
 var hasil_part_13 = Number(pirep[13].number_of_ata);
 var hasil_part_14 = Number(pirep[14].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata,
 pirep[12].ata,
 pirep[13].ata,
 pirep[14].ata
 ];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  }
  ]
}
]
});
}
else if (panjang_pirep == 14) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var part_12 = pirep[12].ata;
 var part_13 = pirep[13].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var hasil_part_12 = Number(pirep[12].number_of_ata);
 var hasil_part_13 = Number(pirep[13].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata,
 pirep[12].ata,
 pirep[13].ata
 ];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_13,
    y: hasil_part_13
  }
  ]
}
]
});
}
else if (panjang_pirep == 13) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var part_12 = pirep[12].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var hasil_part_12 = Number(pirep[12].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata,
 pirep[12].ata
 ];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  }
  ]
}
]
});
}
else if (panjang_pirep == 12) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var part_11 = pirep[11].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var hasil_part_11 = Number(pirep[11].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata,
 pirep[11].ata
 ];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  }
  ]
}
]
});
}
else if (panjang_pirep == 11) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var part_10 = pirep[10].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var hasil_part_10 = Number(pirep[10].number_of_ata);
 var zr = [
 pirep[0].ata,
 pirep[1].ata,
 pirep[2].ata,
 pirep[3].ata,
 pirep[4].ata,
 pirep[5].ata,
 pirep[6].ata,
 pirep[7].ata,
 pirep[8].ata,
 pirep[9].ata,
 pirep[10].ata
 ];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  }
  ]
}
]
});

}
else if (panjang_pirep == 10) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var part_9 = pirep[9].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var hasil_part_9 = Number(pirep[9].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata,pirep[3].ata,pirep[4].ata,pirep[5].ata,pirep[6].ata,pirep[7].ata,pirep[8].ata,pirep[9].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  }
  ]
}
]
});
}else if (panjang_pirep == 9) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var part_8 = pirep[8].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var hasil_part_8 = Number(pirep[8].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata,pirep[3].ata,pirep[4].ata,pirep[5].ata,pirep[6].ata,pirep[7].ata,pirep[8].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  }
  ]
}
]
});
} else if (panjang_pirep == 8) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var part_7 = pirep[7].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var hasil_part_7 = Number(pirep[7].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata,pirep[3].ata,pirep[4].ata,pirep[5].ata,pirep[6].ata,pirep[7].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  }
  ]
}
]
});
} else if (panjang_pirep == 7) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var part_6 = pirep[6].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var hasil_part_6 = Number(pirep[6].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata,pirep[3].ata,pirep[4].ata,pirep[5].ata,pirep[6].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  }
  ]
}
]
});

} else if (panjang_pirep == 6) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var part_5 = pirep[5].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var hasil_part_5 = Number(pirep[5].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata,pirep[3].ata,pirep[4].ata,pirep[5].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  }
  ]
}
]
});
} else if (panjang_pirep == 5) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var part_4 = pirep[4].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var hasil_part_4 = Number(pirep[4].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata,pirep[3].ata,pirep[4].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  }
  ]
}
]
});

} else if (panjang_pirep == 4) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var part_3 = pirep[3].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var hasil_part_3 = Number(pirep[3].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata,pirep[3].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  }
  ]
}
]
});

} else if (panjang_pirep == 3) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var part_2 = pirep[2].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var hasil_part_2 = Number(pirep[2].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata,pirep[2].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },

  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  }
  ]
}
]
});

} else if (panjang_pirep == 2) {
 var part_0 = pirep[0].ata;
 var part_1 = pirep[1].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var hasil_part_1 = Number(pirep[1].number_of_ata);
 var zr = [pirep[0].ata,pirep[1].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  }
  ]
}
]
});

} else if (panjang_pirep == 1) {
 var part_0 = pirep[0].ata;
 var hasil_part_0 = Number(pirep[0].number_of_ata);
 var zr = [pirep[0].ata];
 Highcharts.chart('crut', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP PIREP - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Pirep'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal2').modal('show');
           if ($.fn.dataTable.isDataTable('#pirep_table')) {
            var table = $("#pirep_table").DataTable();
            table.destroy();
            $('#pirep_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = zr[category];
          var pencarian_hasil = '<thead id ="sakj">' +
          '<th class="text-center">No</th>' +
          '<th class="text-center">Date</th>' +
          '<th class="text-center">Sequence</th>' +
          '<th class="text-center">Notification Number</th>' +
          '<th class="text-center">A/C Type</th>' +
          '<th class="text-center">A/C Reg</th>' +
          '<th class="text-center">StaDep</th>' +
          '<th class="text-center">StaArr</th>' +
          '<th class="text-center">Flight No </th>' +
          '<th class="text-center">ATA</th>' +
          '<th class="text-center">SUB ATA</th>' +
          '<th class="text-center">Problem</th>' +
          '<th class="text-center">Rectification</th>' +
          '<th class="text-center">Coding</th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead>' +
          '<tbody id="enakeun"></tbody>';
          $('#pirep_table').append(pencarian_hasil);
          var table = $("#pirep_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP PIREP DETAIL',  exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP PIREP DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetailPirep') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'SEQ'
          },
          {
            data: 'Notification'
          },
          {
            data: 'ac'
          },
          {
            data: 'REG'
          },
          {
            data: 'STADEP'
          },
          {
            data: 'STAARR'
          },
          {
            data: 'FN'
          },
          {
            data: 'ATA'
          },
          {
            data: 'SUBATA'
          },
          {
            data: 'PROBLEM'
          },
          {
            data: 'ACTION'
          }, 
          {
            data: 'PirepMarep'
          },
          {
            data: 'act'
          },
          ]
        });


        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Pirep",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  }
  ]
}
]
});
}
}

var delay =  data.arr_delay;
var panjang_delay = Number(data.delay_cnt);
if (delay !== "") {
 if (panjang_delay == 20) {
   var part_0 = delay[0].ATAtdm;
   var part_1 = delay[1].ATAtdm;
   var part_2 = delay[2].ATAtdm;
   var part_3 = delay[3].ATAtdm;
   var part_4 = delay[4].ATAtdm;
   var part_5 = delay[5].ATAtdm;
   var part_6 = delay[6].ATAtdm;
   var part_7 = delay[7].ATAtdm;
   var part_8 = delay[8].ATAtdm;
   var part_9 = delay[9].ATAtdm;
   var part_10 = delay[10].ATAtdm;
   var part_11 = delay[11].ATAtdm;
   var part_12 = delay[12].ATAtdm;
   var part_13 = delay[13].ATAtdm;
   var part_14 = delay[14].ATAtdm;
   var part_15 = delay[15].ATAtdm;
   var part_16 = delay[16].ATAtdm;
   var part_17 = delay[17].ATAtdm;
   var part_18 = delay[18].ATAtdm;
   var part_19 = delay[19].ATAtdm;
   var hasil_part_0 = Number(delay[0].number_of_ata1);
   var hasil_part_1 = Number(delay[1].number_of_ata1);
   var hasil_part_2 = Number(delay[2].number_of_ata1);
   var hasil_part_3 = Number(delay[3].number_of_ata1);
   var hasil_part_4 = Number(delay[4].number_of_ata1);
   var hasil_part_5 = Number(delay[5].number_of_ata1);
   var hasil_part_6 = Number(delay[6].number_of_ata1);
   var hasil_part_7 = Number(delay[7].number_of_ata1);
   var hasil_part_8 = Number(delay[8].number_of_ata1);
   var hasil_part_9 = Number(delay[9].number_of_ata1);
   var hasil_part_10 = Number(delay[10].number_of_ata1);
   var hasil_part_11 = Number(delay[11].number_of_ata1);
   var hasil_part_12 = Number(delay[12].number_of_ata1);
   var hasil_part_13 = Number(delay[13].number_of_ata1);
   var hasil_part_14 = Number(delay[14].number_of_ata1);
   var hasil_part_15 = Number(delay[15].number_of_ata1);
   var hasil_part_16 = Number(delay[16].number_of_ata1);
   var hasil_part_17 = Number(delay[17].number_of_ata1);
   var hasil_part_18 = Number(delay[18].number_of_ata1);
   var hasil_part_19 = Number(delay[19].number_of_ata1);

   var pr = [
   delay[0].ATAtdm,
   delay[1].ATAtdm,
   delay[2].ATAtdm,
   delay[3].ATAtdm,
   delay[4].ATAtdm,
   delay[5].ATAtdm,
   delay[6].ATAtdm,
   delay[7].ATAtdm,
   delay[8].ATAtdm,
   delay[9].ATAtdm,
   delay[10].ATAtdm,
   delay[11].ATAtdm,
   delay[12].ATAtdm,
   delay[13].ATAtdm,
   delay[14].ATAtdm,
   delay[15].ATAtdm,
   delay[16].ATAtdm,
   delay[17].ATAtdm,
   delay[18].ATAtdm,
   delay[19].ATAtdm
   ];

   Highcharts.chart('crot', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP DELAY - ' + judul
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number of Delay'
      },
       showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal').modal('show');
             if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center"># </th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },


            ]
          });
          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_8,
      y: hasil_part_8
    },
    {
      name: part_9,
      y: hasil_part_9
    },
    {
      name: part_10,
      y: hasil_part_10
    },
    {
      name: part_11,
      y: hasil_part_11
    },
    {
      name: part_12,
      y: hasil_part_12
    },
    {
      name: part_13,
      y: hasil_part_13
    },
    {
      name: part_14,
      y: hasil_part_14
    },
    {
      name: part_15,
      y: hasil_part_15
    },
    {
      name: part_16,
      y: hasil_part_16
    },
    {
      name: part_17,
      y: hasil_part_17
    },
    {
      name: part_18,
      y: hasil_part_18
    },
    {
      name: part_19,
      y: hasil_part_19
    }
    ]
  }
  ]
});

} else if (panjang_delay == 19) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var part_12 = delay[12].ATAtdm;
 var part_13 = delay[13].ATAtdm;
 var part_14 = delay[14].ATAtdm;
 var part_15 = delay[15].ATAtdm;
 var part_16 = delay[16].ATAtdm;
 var part_17 = delay[17].ATAtdm;
 var part_18 = delay[18].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var hasil_part_12 = Number(delay[12].number_of_ata1);
 var hasil_part_13 = Number(delay[13].number_of_ata1);
 var hasil_part_14 = Number(delay[14].number_of_ata1);
 var hasil_part_15 = Number(delay[15].number_of_ata1);
 var hasil_part_16 = Number(delay[16].number_of_ata1);
 var hasil_part_17 = Number(delay[17].number_of_ata1);
 var hasil_part_18 = Number(delay[18].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm,
 delay[12].ATAtdm,
 delay[13].ATAtdm,
 delay[14].ATAtdm,
 delay[15].ATAtdm,
 delay[16].ATAtdm,
 delay[17].ATAtdm,
 delay[18].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  },
  {
    name: part_17,
    y: hasil_part_17
  },
  {
    name: part_18,
    y: hasil_part_18
  }
  ]
}
]
});

} else if (panjang_delay == 18) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var part_12 = delay[12].ATAtdm;
 var part_13 = delay[13].ATAtdm;
 var part_14 = delay[14].ATAtdm;
 var part_15 = delay[15].ATAtdm;
 var part_16 = delay[16].ATAtdm;
 var part_17 = delay[17].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var hasil_part_12 = Number(delay[12].number_of_ata1);
 var hasil_part_13 = Number(delay[13].number_of_ata1);
 var hasil_part_14 = Number(delay[14].number_of_ata1);
 var hasil_part_15 = Number(delay[15].number_of_ata1);
 var hasil_part_16 = Number(delay[16].number_of_ata1);
 var hasil_part_17 = Number(delay[17].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm,
 delay[12].ATAtdm,
 delay[13].ATAtdm,
 delay[14].ATAtdm,
 delay[15].ATAtdm,
 delay[16].ATAtdm,
 delay[17].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  },
  {
    name: part_17,
    y: hasil_part_17
  }
  ]
}
]
});

} else if (panjang_delay == 17) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var part_12 = delay[12].ATAtdm;
 var part_13 = delay[13].ATAtdm;
 var part_14 = delay[14].ATAtdm;
 var part_15 = delay[15].ATAtdm;
 var part_16 = delay[16].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var hasil_part_12 = Number(delay[12].number_of_ata1);
 var hasil_part_13 = Number(delay[13].number_of_ata1);
 var hasil_part_14 = Number(delay[14].number_of_ata1);
 var hasil_part_15 = Number(delay[15].number_of_ata1);
 var hasil_part_16 = Number(delay[16].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm,
 delay[12].ATAtdm,
 delay[13].ATAtdm,
 delay[14].ATAtdm,
 delay[15].ATAtdm,
 delay[16].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  },
  {
    name: part_16,
    y: hasil_part_16
  }
  ]
}
]
});
} else if (panjang_delay == 16) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var part_12 = delay[12].ATAtdm;
 var part_13 = delay[13].ATAtdm;
 var part_14 = delay[14].ATAtdm;
 var part_15 = delay[15].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var hasil_part_12 = Number(delay[12].number_of_ata1);
 var hasil_part_13 = Number(delay[13].number_of_ata1);
 var hasil_part_14 = Number(delay[14].number_of_ata1);
 var hasil_part_15 = Number(delay[15].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm,
 delay[12].ATAtdm,
 delay[13].ATAtdm,
 delay[14].ATAtdm,
 delay[15].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  },
  {
    name: part_15,
    y: hasil_part_15
  }
  ]
}
]
});
} else if (panjang_delay == 15) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var part_12 = delay[12].ATAtdm;
 var part_13 = delay[13].ATAtdm;
 var part_14 = delay[14].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var hasil_part_12 = Number(delay[12].number_of_ata1);
 var hasil_part_13 = Number(delay[13].number_of_ata1);
 var hasil_part_14 = Number(delay[14].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm,
 delay[12].ATAtdm,
 delay[13].ATAtdm,
 delay[14].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  },
  {
    name: part_14,
    y: hasil_part_14
  }
  ]
}
]
});
} else if (panjang_delay == 14) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var part_12 = delay[12].ATAtdm;
 var part_13 = delay[13].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var hasil_part_12 = Number(delay[12].number_of_ata1);
 var hasil_part_13 = Number(delay[13].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm,
 delay[12].ATAtdm,
 delay[13].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  },
  {
    name: part_13,
    y: hasil_part_13
  }
  ]
}
]
});
} else if (panjang_delay == 13) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var part_12 = delay[12].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var hasil_part_12 = Number(delay[12].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm,
 delay[12].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  },
  {
    name: part_12,
    y: hasil_part_12
  }
  ]
}
]
});
} else if (panjang_delay == 12) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var part_10 = delay[10].ATAtdm;
 var part_11 = delay[11].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var hasil_part_10 = Number(delay[10].number_of_ata1);
 var hasil_part_11 = Number(delay[11].number_of_ata1);
 var pr = [
 delay[0].ATAtdm,
 delay[1].ATAtdm,
 delay[2].ATAtdm,
 delay[3].ATAtdm,
 delay[4].ATAtdm,
 delay[5].ATAtdm,
 delay[6].ATAtdm,
 delay[7].ATAtdm,
 delay[8].ATAtdm,
 delay[9].ATAtdm,
 delay[10].ATAtdm,
 delay[11].ATAtdm
 ];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  },
  {
    name: part_10,
    y: hasil_part_10
  },
  {
    name: part_11,
    y: hasil_part_11
  }
  ]
}
]
});

} else if (panjang_delay == 11) {
  var part_0 = delay[0].ATAtdm;
  var part_1 = delay[1].ATAtdm;
  var part_2 = delay[2].ATAtdm;
  var part_3 = delay[3].ATAtdm;
  var part_4 = delay[4].ATAtdm;
  var part_5 = delay[5].ATAtdm;
  var part_6 = delay[6].ATAtdm;
  var part_7 = delay[7].ATAtdm;
  var part_8 = delay[8].ATAtdm;
  var part_9 = delay[9].ATAtdm;
  var part_10 = delay[10].ATAtdm;
  var hasil_part_0 = Number(delay[0].number_of_ata1);
  var hasil_part_1 = Number(delay[1].number_of_ata1);
  var hasil_part_2 = Number(delay[2].number_of_ata1);
  var hasil_part_3 = Number(delay[3].number_of_ata1);
  var hasil_part_4 = Number(delay[4].number_of_ata1);
  var hasil_part_5 = Number(delay[5].number_of_ata1);
  var hasil_part_6 = Number(delay[6].number_of_ata1);
  var hasil_part_7 = Number(delay[7].number_of_ata1);
  var hasil_part_8 = Number(delay[8].number_of_ata1);
  var hasil_part_9 = Number(delay[9].number_of_ata1);
  var hasil_part_10 = Number(delay[10].number_of_ata1);
  var pr = [
  delay[0].ATAtdm,
  delay[1].ATAtdm,
  delay[2].ATAtdm,
  delay[3].ATAtdm,
  delay[4].ATAtdm,
  delay[5].ATAtdm,
  delay[6].ATAtdm,
  delay[7].ATAtdm,
  delay[8].ATAtdm,
  delay[9].ATAtdm,
  delay[10].ATAtdm
  ];

  Highcharts.chart('crot', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP DELAY - ' + judul
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number of Delay'
      },
       showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal').modal('show');
             if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center"># </th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },


            ]
          });
          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_8,
      y: hasil_part_8
    },
    {
      name: part_9,
      y: hasil_part_9
    },
    {
      name: part_10,
      y: hasil_part_10
    }
    ]
  }
  ]
});

}  
else if (panjang_delay ==  10) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var part_9 = delay[9].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var hasil_part_9 = Number(delay[9].number_of_ata1);
 var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm,delay[3].ATAtdm,delay[4].ATAtdm,delay[5].ATAtdm,delay[6].ATAtdm,delay[7].ATAtdm,delay[8].ATAtdm,delay[9].ATAtdm];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center"># </th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });
        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  },
  {
    name: part_1,
    y: hasil_part_1

  },
  {
    name: part_2,
    y: hasil_part_2
  },
  {
    name: part_3,
    y: hasil_part_3
  },
  {
    name: part_4,
    y: hasil_part_4
  },
  {
    name: part_5,
    y: hasil_part_5
  },
  {
    name: part_6,
    y: hasil_part_6
  },
  {
    name: part_7,
    y: hasil_part_7
  },
  {
    name: part_8,
    y: hasil_part_8
  },
  {
    name: part_9,
    y: hasil_part_9
  }
  ]
}
]
});
}  else if (panjang_delay == 9) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var part_8 = delay[8].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var hasil_part_8 = Number(delay[8].number_of_ata1);
 var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm,delay[3].ATAtdm,delay[4].ATAtdm,delay[5].ATAtdm,delay[6].ATAtdm,delay[7].ATAtdm,delay[8].ATAtdm];

 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
            $('#myModal').modal('show');
            if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    },
    {
      name: part_8,
      y: hasil_part_8
    }
    ]
  }
  ]
});

} else if (panjang_delay == 8) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var part_7 = delay[7].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var hasil_part_7 = Number(delay[7].number_of_ata1);
 var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm,delay[3].ATAtdm,delay[4].ATAtdm,delay[5].ATAtdm,delay[6].ATAtdm,delay[7].ATAtdm];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
            $('#myModal').modal('show');
            if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    },
    {
      name: part_7,
      y: hasil_part_7
    }
    ]
  }
  ]
});

} else if (panjang_delay == 7) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var part_6 = delay[6].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var hasil_part_6 = Number(delay[6].number_of_ata1);
 var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm,delay[3].ATAtdm,delay[4].ATAtdm,delay[5].ATAtdm,delay[6].ATAtdm];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
            $('#myModal').modal('show');
            if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    },
    {
      name: part_6,
      y: hasil_part_6
    }
    ]
  }
  ]
});

} else if (panjang_delay == 6) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var part_5 = delay[5].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var hasil_part_5 = Number(delay[5].number_of_ata1);
 var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm,delay[3].ATAtdm,delay[4].ATAtdm,delay[5].ATAtdm];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
            $('#myModal').modal('show');
            if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    },
    {
      name: part_5,
      y: hasil_part_5
    }
    ]
  }
  ]
});

} else if (panjang_delay == 5) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var part_2 = delay[2].ATAtdm;
 var part_3 = delay[3].ATAtdm;
 var part_4 = delay[4].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var hasil_part_2 = Number(delay[2].number_of_ata1);
 var hasil_part_3 = Number(delay[3].number_of_ata1);
 var hasil_part_4 = Number(delay[4].number_of_ata1);
 var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm,delay[3].ATAtdm,delay[4].ATAtdm];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  subtitle: {
    text: 'Periode : ' + d_f + ' - ' + d_t
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
            $('#myModal').modal('show');
            if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    },
    {
      name: part_4,
      y: hasil_part_4
    }
    ]
  }
  ]
});

} else if (panjang_delay == 4) {
  var part_0 = delay[0].ATAtdm;
  var part_1 = delay[1].ATAtdm;
  var part_2 = delay[2].ATAtdm;
  var part_3 = delay[3].ATAtdm;
  var hasil_part_0 = Number(delay[0].number_of_ata1);
  var hasil_part_1 = Number(delay[1].number_of_ata1);
  var hasil_part_2 = Number(delay[2].number_of_ata1);
  var hasil_part_3 = Number(delay[3].number_of_ata1);
  var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm,delay[3].ATAtdm];
  Highcharts.chart('crot', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP DELAY - ' + judul
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number of Delay'
      },
       showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal').modal('show');
             if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });
          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    },
    {
      name: part_3,
      y: hasil_part_3
    }
    ]
  }
  ]
});

} else if (panjang_delay == 3) {
  var part_0 = delay[0].ATAtdm;
  var part_1 = delay[1].ATAtdm;
  var part_2 = delay[2].ATAtdm;
  var hasil_part_0 = Number(delay[0].number_of_ata1);
  var hasil_part_1 = Number(delay[1].number_of_ata1);
  var hasil_part_2 = Number(delay[2].number_of_ata1);
  var pr = [delay[0].ATAtdm,delay[1].ATAtdm,delay[2].ATAtdm];
  Highcharts.chart('crot', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'TOP DELAY - ' + judul
    },
    subtitle: {
      text: 'Periode : ' + d_f + ' - ' + d_t
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Number of Delay'
      },
       showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
             $('#myModal').modal('show');
             if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0

    },
    {
      name: part_1,
      y: hasil_part_1

    },
    {
      name: part_2,
      y: hasil_part_2
    }
    ]
  }
  ]
});

} else if (panjang_delay == 2) {
 var part_0 = delay[0].ATAtdm;
 var part_1 = delay[1].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var hasil_part_1 = Number(delay[1].number_of_ata1);
 var pr = [delay[0].ATAtdm,delay[1].ATAtdm];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
            $('#myModal').modal('show');
            if ($.fn.dataTable.isDataTable('#delay_table')) {
              var table = $("#delay_table").DataTable();
              table.destroy();
              $('#delay_table').html("");
            }
            var category = this.category;
            var value_category = this.y;
            var dikirim = pr[category];
            var pencarian_hasil = '<thead > '+
            '<th class="text-center">No</th>' + 
            '<th class="text-center">Notification</th>' + 
            '<th class="text-center">Date</th>' + 
            '<th class="text-center">A/C Type</th>' + 
            '<th class="text-center">A/C Reg</th>' + 
            '<th class="text-center">Sta Dep</th>' + 
            '<th class="text-center">Sta Arr</th>' + 
            '<th class="text-center">Flight No</th>' + 
            '<th class="text-center">Techical Delay Length</th>' + 
            '<th class="text-center">ATA </th>' +
            '<th class="text-center">Sub ATA </th>' +
            '<th class="text-center">Problem </th>' +
            '<th class="text-center">Rectification </th>' +
            '<th class="text-center">Chronology </th>' +
            '<th class="text-center">DCP </th>' +
            '<th class="text-center">RTB/RTA/RTO </th>' +
            '<th class="text-center">#</th>' +
            '</tr></thead> ' +
            '<tbody></tbody>';
            $('#delay_table').append(pencarian_hasil);
            var table = $("#delay_table").DataTable({
              retrieve: true,
              pagin: false,
              dom: 'Bfrtip',
              buttons: [
              {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
              }
            },
            {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
            exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            },
            pageSize: 'LETTER'}
            ],
            ajax: {
              "url": "<?= base_url('Pareto/searchDetail') ?>",
              "type": "POST",
              "data": {
                "actype": actype,
                "acreg": acreg,
                "p_graph": p_graph,
                "ata": ata,
                "date_from": date_from,
                "date_to": date_to,
                "dikirim": dikirim,
                "value_category": value_category,
                "filter" : filter
              },
            },
            'columns': [
            {
              data: 'no'
            },
            {
              data: 'EventID'
            },
            {
              data: 'TargetDate'
            },
            {
              data: 'ac'
            },
            {
              data: 'Reg'
            },
            {
              data: 'DepSta'
            },
            {
              data: 'ArivSta'
            },
            {
              data: 'FlightNo'
            },
            {
              data: 'TDAM'
            },
            {
              data: 'ATAtdm'
            },
            {
              data: 'SubATAtdm'
            },
            {
              data: 'Problem'
            },
            {
              data: 'last'
            }, 
            {
              data: 'Rectification'
            },
            {
              data: 'DCP'
            },
            {
              data: 'RtABO'
            },
            {
              data: 'act'
            },
            ]
          });

          }
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },
  series: [
  {
    name: "Number Of Delay",
    colorByPoint: true,
    data: [
    {
      name: part_0,
      y: hasil_part_0
    },
    {
      name: part_1,
      y: hasil_part_1
    }
    ]
  }
  ]
});
} else if (panjang_delay == 1) {
 var part_0 = delay[0].ATAtdm;
 var hasil_part_0 = Number(delay[0].number_of_ata1);
 var pr = [delay[0].ATAtdm];
 Highcharts.chart('crot', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOP DELAY - ' + judul
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Number of Delay'
    },
     showFirstLabel: true,
          gridLineColor: '#efefef',
          gridLineDashStyle: 'shortdash',
          tickInterval: 1,
          showFirstLabel: true,
          showLastLabel: true,
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
           $('#myModal').modal('show');
           if ($.fn.dataTable.isDataTable('#delay_table')) {
            var table = $("#delay_table").DataTable();
            table.destroy();
            $('#delay_table').html("");
          }
          var category = this.category;
          var value_category = this.y;
          var dikirim = pr[category];
          var pencarian_hasil = '<thead > '+
          '<th class="text-center">No</th>' + 
          '<th class="text-center">Notification</th>' + 
          '<th class="text-center">Date</th>' + 
          '<th class="text-center">A/C Type</th>' + 
          '<th class="text-center">A/C Reg</th>' + 
          '<th class="text-center">Sta Dep</th>' + 
          '<th class="text-center">Sta Arr</th>' + 
          '<th class="text-center">Flight No</th>' + 
          '<th class="text-center">Techical Delay Length</th>' + 
          '<th class="text-center">ATA </th>' +
          '<th class="text-center">Sub ATA </th>' +
          '<th class="text-center">Problem </th>' +
          '<th class="text-center">Rectification </th>' +
          '<th class="text-center">Chronology </th>' +
          '<th class="text-center">DCP </th>' +
          '<th class="text-center">RTB/RTA/RTO </th>' +
          '<th class="text-center">#</th>' +
          '</tr></thead> ' +
          '<tbody></tbody>';
          $('#delay_table').append(pencarian_hasil);
          var table = $("#delay_table").DataTable({
            retrieve: true,
            pagin: false,
            dom: 'Bfrtip',
            buttons: [
            {extend: 'excel', title: 'REPORT TOP DELAY DETAIL', exportOptions: {
              columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            }
          },
          {extend: 'pdf', title: 'REPORT TOP DELAY DETAIL', orientation: 'landscape',
          exportOptions: {
            columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 ]
          },
          pageSize: 'LETTER'}
          ],
          ajax: {
            "url": "<?= base_url('Pareto/searchDetail') ?>",
            "type": "POST",
            "data": {
              "actype": actype,
              "acreg": acreg,
              "p_graph": p_graph,
              "ata": ata,
              "date_from": date_from,
              "date_to": date_to,
              "dikirim": dikirim,
              "value_category": value_category,
              "filter" : filter
            },
          },
          'columns': [
          {
            data: 'no'
          },
          {
            data: 'EventID'
          },
          {
            data: 'TargetDate'
          },
          {
            data: 'ac'
          },
          {
            data: 'Reg'
          },
          {
            data: 'DepSta'
          },
          {
            data: 'ArivSta'
          },
          {
            data: 'FlightNo'
          },
          {
            data: 'TDAM'
          },
          {
            data: 'ATAtdm'
          },
          {
            data: 'SubATAtdm'
          },
          {
            data: 'Problem'
          },
          {
            data: 'last'
          }, 
          {
            data: 'Rectification'
          },
          {
            data: 'DCP'
          },
          {
            data: 'RtABO'
          },
          {
            data: 'act'
          },


          ]
        });

        }
      }
    },
    dataLabels: {
      enabled: true,
      format: '{point.y}'
    }
  }
},
tooltip: {
  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
  pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
},
series: [
{
  name: "Number Of Delay",
  colorByPoint: true,
  data: [
  {
    name: part_0,
    y: hasil_part_0

  }
  ]
}
]
});

}

}


}

});

 $('body').addClass('mini-navbar');
})

 function radioget(getValue)
 {
   $('#p_graph').val(getValue);
   if (getValue == "ata") {
     document.getElementById("ata").disabled = true;
     document.getElementById("acreg").disabled = false;
     document.getElementById("KeyProblem").disabled = true;
     $('#ata').append('<option selected="selected" value=""></option>');
     $('#ata').append('<option selected="selected" value=""></option>');
   }else if (getValue == "ac_reg") {
     document.getElementById("ata").disabled = false;
     document.getElementById("acreg").disabled = true;
     document.getElementById("KeyProblem").disabled = false;
   } else if (getValue == "subata") {
    document.getElementById("ata").disabled = false;
    document.getElementById("acreg").disabled = false;
    document.getElementById("KeyProblem").disabled = true;
  } else if (getValue == "keyproblem") {
    document.getElementById("ata").disabled = false;
    document.getElementById("acreg").disabled = false;
    document.getElementById("KeyProblem").disabled = true;
    $('#KeyProblem').append('<option selected="selected" value=""></option>');
  }
  else {
   document.getElementById("ata").disabled = false;
 }

}


</script>