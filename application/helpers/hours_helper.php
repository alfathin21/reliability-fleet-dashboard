<?php 
function convertToHoursMins($time,$min, $format = '%02d:%02d') {
    if ($time < 1) {
        $hours = ($time * 60) + $min;
        return  $hours;
    } else {
        $hours = ($time * 60) + $min;
        return  $hours;
    }
   
}
function  is_log_in() 
{
	$ci = get_instance();
	if ( !$ci->session->userdata('nama')) {
		redirect(base_url());
	} 
}

function convertToHours($time, $format = '%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    return sprintf($format, $hours);
}


function convertToMins($time, $format = '%02d') {
    if ($time < 1) {
        return;
    }
    $minutes = ($time % 60);
    return sprintf($format, $minutes);
}
