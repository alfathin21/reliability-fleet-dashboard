<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PComponent extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		is_log_in();
		$this->load->model('Techlog_model');
		error_reporting(0);
		
	}
	public function index()
	{
		$data['title'] = 'Pareto Component Removal';
		$data['unit'] = $this->session->userdata('unit');
		$data['ata'] = $this->Techlog_model->getata();
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/removal');
		$this->load->view('template/fo2');
	}
	public function search()
	{
		$ac = $this->input->post("actype[]");
		$filter = $this->input->post("filter");
		$batas = intval($filter);
		$a = [];
		$k = [];
		for ($i=0; $i < count($ac) ; $i++) { 
			$a[$i] =  "'".$ac[$i]."'".',';
			$k[$i] =  $ac[$i].',';
		}
		$b =  implode(',',$a);
		$hj =  implode(',',$k);
		$c =  str_replace(',,', ',', $b);
		$za =  str_replace(',,', ',', $hj);
		$d =  rtrim($c,',');	
		$e =  rtrim($za,',');
		$ACType = "AND Aircraft IN ($d)";
		if(!empty($_POST["date_from"])){
			$DateStart = $_POST["date_from"];
		}
		else{
			$DateStart = "";
		}
		if(!empty($_POST["date_to"])){
			$DateEnd = $_POST["date_to"];
		}
		else{
			$DateEnd = "";
		}

		if (empty($_POST['ata'])) {
			$ata = '';
		} else {
			$ata = 'AND ATA  = '.$_POST['ata'];
		}


		if (!empty($_POST["s"]) && $_POST['s'] == 'S' && !empty($_POST["u"]) && $_POST['u'] == 'U') {
			$s = $_POST["s"];
			$u = $_POST["u"];
			$s_jadi = "'".$s."'";
			$u_jadi = "'".$u."'";
			$data  = $s_jadi.','.$u_jadi;
			$where_remcode = "AND RemCode IN ($data)";
		}
		else if(!empty($_POST["u"]) && $_POST['u'] == 'U'){
			$data = "'%".$_POST["u"]."%'";
			$where_remcode = "AND RemCode LIKE".$data;
		} else if (!empty($_POST["s"]) && $_POST['s'] == 'S') {
			$data = "'%".$_POST["s"]."%'";
			$where_remcode = "AND RemCode LIKE".$data;
		} 
		else {
			$where_remcode = "";
		}

		$sql_graph_comp = "SELECT PartNo, PartName, COUNT(PartNo) AS number_of_part
		FROM tblcompremoval WHERE DateRem BETWEEN '".$DateStart."' AND '".$DateEnd."'".$ACType."".$where_remcode."".$ata." GROUP BY PartNo ORDER BY number_of_part DESC LIMIT $batas";

		$result = $this->db->query($sql_graph_comp)->result_array();
		$jumlah = count($result);
		$response = [];
		$graph = [];
		$no = 1;
		foreach ($result as $key) {
			$op['no'] = $no;
			$op['PartNo'] = $key["PartNo"];
			$op['PartName'] = $key["PartName"];
			$op['number_of_part'] = $key['number_of_part']; 
			if ($key["PartNo"] != null and $key['number_of_part'] != null) {
				$cd['name'] = $key["PartNo"];
				$cd['y'] = strval($key['number_of_part']); 
				array_push($graph, $cd);
			}


			$no++;
			array_push($response, $op);

		}

		$newresponse = array(
			'data' => $response,
			'jumlah' => $jumlah,
			'graph'	=> $graph,
			'title' => $e,
			'sql'	=> $sql_graph_comp
		);
		echo json_encode($newresponse);

	}



	public function CompDetail()
	{


		$ac = $this->input->post("actype[]");
		$a = [];
		$k = [];
		for ($i=0; $i < count($ac) ; $i++) { 
			$a[$i] =  "'".$ac[$i]."'".',';
			$k[$i] =  $ac[$i].',';
		}
		$b =  implode(',',$a);
		$hj =  implode(',',$k);
		$c =  str_replace(',,', ',', $b);
		$za =  str_replace(',,', ',', $hj);
		$d =  rtrim($c,',');	
		$e =  rtrim($za,',');
		$ACType = "AND Aircraft IN ($d)";
		if(!empty($_POST["date_from"])){
			$DateStart = $_POST["date_from"];
		}
		else{
			$DateStart = "";
		}
		if(!empty($_POST["date_to"])){
			$DateEnd = $_POST["date_to"];
		}
		else{
			$DateEnd = "";
		}
		if (!empty($_POST["s"]) && $_POST['s'] == 'S' && !empty($_POST["u"]) && $_POST['u'] == 'U') {
			$s = $_POST["s"];
			$u = $_POST["u"];
			$s_jadi = "'".$s."'";
			$u_jadi = "'".$u."'";
			$data  = $s_jadi.','.$u_jadi;
			$where_remcode = "AND RemCode IN ($data)";
		}
		else if(!empty($_POST["u"]) && $_POST['u'] == 'U'){
			$data = "'%".$_POST["u"]."%'";
			$where_remcode = "AND RemCode LIKE".$data;
		} else if (!empty($_POST["s"]) && $_POST['s'] == 'S') {
			$data = "'%".$_POST["s"]."%'";
			$where_remcode = "AND RemCode LIKE".$data;
		} 
		else {
			$where_remcode = "";
		}


		if (empty($_POST['ata'])) {
			$ata = '';
		} else {
			$ata = 'AND ATA  = '.$_POST['ata'];
		}

		$part_no  = "'%".$_POST["dikirim"]."%'";
		$unit = $this->session->userdata('unit');

		$sql_graph_comp = "SELECT ID, ATA, AIN, PartNo, SerialNo, PartName, Reg, Aircraft, RemCode, RealReason AS real_reason, DateRem
		FROM tblcompremoval WHERE DateRem BETWEEN '".$DateStart."' AND '".$DateEnd."'".$ACType."".$where_remcode." and PartNo LIKE ".$part_no."".$ata." ORDER BY PartNo ";

		$result = $this->db->query($sql_graph_comp)->result_array();
		$no = 1;

		$response = [];
		foreach ($result as $key ) {
			$clsa = $key["ID"];

			$op['no'] = $no;
			$op['ID'] = $key["ID"];
			$op['ATA'] = $key["ATA"];
			$op['AIN'] = $key['AIN']; 
			$op['PartNo'] = $key['PartNo']; 
			$op['SerialNo'] = $key['SerialNo']; 
			$op['PartName'] = $key['PartName']; 
			$op['Reg'] = $key['Reg']; 
			$op['Aircraft'] = $key['Aircraft']; 
			$op['RemCode'] = $key['RemCode']; 
			$op['real_reason'] = $key['real_reason']; 
			$op['DateRem'] = $key['DateRem'];
			if ($unit == 'TER-1' || $unit == '') {
				$op['act'] = "<a target='_blank' href=".base_url('PComponent/up?notif='.$clsa.'')." class='btn btn-primary btn-sm'><i class='fa fa-edit'></i></a>";
			} else {
				$op['act'] = '';
			}
			array_push($response, $op);
			$no++;
		}

		$newresponse = array('data' => $response);
		echo json_encode($newresponse);
	}
	public function up()
	{
		$data['title'] = 'Pareto Component Removal';
		$notif = $this->input->get("notif");
		$data['dt']    = $this->db->query("SELECT ID, ATA, AIN, PartNo, SerialNo, PartName, Reg, Aircraft, RemCode, RealReason AS real_reason, DateRem FROM tblcompremoval where ID = $notif ")->row_array();
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/pcupdate');
		$this->load->view('template/fo2');
	}


	public function updatedetail()
	{
		$id = $this->input->post("id");
		$part_number = $this->input->post("part_number");
		$part_name = $this->input->post("part_name");
		$equipment = $this->input->post("equipment");
		$serial_number = $this->input->post("serial_number");
		$reg = $this->input->post("reg");
		$actype = $this->input->post("actype");
		$RemCode = $this->input->post("RemCode");
		$ata = $this->input->post("ata");
		$date_removal = $this->input->post("date_removal");
		$real_reason = $this->input->post("real_reason");
		$this->db->set('ATA', $ata);
		$this->db->set('AIN', $equipment);
		$this->db->set('PartNo', $part_number);
		$this->db->set('PartName', $part_name);
		$this->db->set('Reg', $reg);
		$this->db->set('SerialNo', $serial_number);
		$this->db->set('RemCode', $RemCode);
		$this->db->set('RealReason', $real_reason);
		$this->db->set('DateRem', $date_removal);
		$this->db->where('ID', $id);
		$this->db->update('tblcompremoval');
		$this->session->set_flashdata("Pesan", "Update");
		redirect('PComponent','refresh');

	}


}

/* End of file PComponent.php */
/* Location: ./application/controllers/PComponent.php */