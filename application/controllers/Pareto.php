<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pareto extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		is_log_in();
		$this->load->model('Techlog_model');
		error_reporting(0);
		
	}
	public function index()
	{
		$data['title'] = 'Pareto Techlog / Delay';
		$data['unit'] = $this->session->userdata('unit');
		$data['ata'] = $this->Techlog_model->getata();
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/pareto');
		$this->load->view('template/fo2');
	} 

	public function search()
	{
		
		$filter = $this->input->post("filter");
		$batas = intval($filter);
		$ac = $this->input->post("actype[]");
		$a = [];
		$k = [];
		$unit = $this->session->userdata('unit');
		for ($i=0; $i < count($ac) ; $i++) { 
			$a[$i] =  "'".$ac[$i]."'".',';
			$k[$i] =  $ac[$i].',';
		}
		$b =  implode(',',$a);
		$hj =  implode(',',$k);
		$c =  str_replace(',,', ',', $b);
		$za =  str_replace(',,', ',', $hj);
		$d =  rtrim($c,',');	
		$e =  rtrim($za,',');
		$ACType = "ACtype IN ($d)";
		$graf_actype = $d;
		if(empty($_POST["acreg"])){
			$ACReg = "";
		}
		else{
			$ACReg = $_POST['acreg'];
		}

		$ata = $_POST['ata'];

		if(!empty($_POST["date_from"])){

			$a = "'".$_POST["date_from"]."'";
			$DateStart = $a;
		}
		else{
			$DateStart = "";
		}
		if(!empty($_POST["date_to"])){
			$b = "'".$_POST["date_to"]."'";
			$DateEnd = $b;
		}
		else{
			$DateEnd = "";
		}
		$Graph_type = $_POST['p_graph'];

		if($Graph_type == 'ata' || $Graph_type == 'ac_reg'){
			if($Graph_type == 'ata'){
				
				$sql_graph_pirep = "SELECT ata, COUNT(ata) AS number_of_ata FROM tblpirep_swift WHERE ".$ACType." AND ata >= 21 AND REG LIKE '%".$ACReg."%' AND PirepMarep = 'pirep' AND DATE BETWEEN ".$DateStart." AND ".$DateEnd." AND ata like '%".$ata."%'  GROUP BY ata ORDER BY number_of_ata DESC LIMIT $batas";

				$sql_graph_marep= "SELECT ata, COUNT(ata) AS number_of_ata FROM tblpirep_swift WHERE ".$ACType." AND ata >= 21 AND REG LIKE '%".$ACReg."%' AND PirepMarep = 'Marep' AND DATE BETWEEN ".$DateStart." AND ".$DateEnd." AND ata like '%".$ata."%'  GROUP BY ata ORDER BY number_of_ata DESC LIMIT $batas";
				
				$sql_graph_delay = "SELECT ATAtdm, COUNT(Atatdm) AS number_of_ata1 FROM mcdrnew WHERE ".$ACType." AND DCP <> 'X' AND REG LIKE '%".$ACReg."' AND DateEvent BETWEEN ".$DateStart." AND ".$DateEnd." AND ATAtdm like '%".$ata."%' GROUP BY ATAtdm ORDER BY number_of_ata1 DESC LIMIT $batas";


				$res_graph_pirep =  $this->db->query($sql_graph_pirep)->result_array();
				$res_graph_delay = $this->db->query($sql_graph_delay)->result_array();
				$res_graph_marep = $this->db->query($sql_graph_marep)->result_array();

				$row_delay_cnt =  $this->db->query($sql_graph_delay)->num_rows();
				$row_pirep_cnt = $this->db->query($sql_graph_pirep)->num_rows();
				$row_marep_cnt = $this->db->query($sql_graph_marep)->num_rows();
				
				
				$i = 0;
				foreach ($res_graph_pirep as $key ) {
					$arr_pirep[$i]['ata'] = $key['ata'];
					$arr_pirep[$i]['number_of_ata'] = $key['number_of_ata'];
					$i++;
				}


				$i = 0;
				foreach ($res_graph_delay as $key) {
					$arr_delay[$i]['ATAtdm'] = $key['ATAtdm'];
					$arr_delay[$i]['number_of_ata1'] = $key['number_of_ata1'];
					$i++;
				}

				$i = 0;
				foreach ($res_graph_marep as $key ) {
					$arr_marep[$i]['ata'] = $key['ata'];
					$arr_marep[$i]['number_of_ata'] = $key['number_of_ata'];
					$i++;
				}
			}
			else if($Graph_type == 'ac_reg'){


				$sql_graph_pirep = "SELECT REG, COUNT(REG) AS number_of_reg FROM tblpirep_swift WHERE DATE BETWEEN ".$DateStart." AND ".$DateEnd." AND ata >= 21 AND ".$ACType." AND REG LIKE '%".$ACReg."%' AND PirepMarep = 'pirep' AND ATA like '%".$ata."%' GROUP BY REG ORDER BY number_of_reg DESC LIMIT $batas ";


				$sql_graph_marep = "SELECT REG, COUNT(REG) AS number_of_reg FROM tblpirep_swift WHERE DATE BETWEEN ".$DateStart." AND ".$DateEnd." AND ata >= 21 AND ".$ACType." AND REG LIKE '%".$ACReg."%' AND PirepMarep = 'Marep' AND ATA like '%".$ata."%' GROUP BY REG ORDER BY number_of_reg DESC LIMIT $batas ";


				$sql_graph_delay = "SELECT Reg, COUNT(Reg) AS number_of_reg FROM mcdrnew WHERE ".$ACType." AND DCP <> 'X' AND REG LIKE '%".$ACReg."' AND DateEvent BETWEEN ".$DateStart." AND ".$DateEnd." AND ATAtdm like '%".$ata."%'  GROUP BY REG ORDER BY number_of_reg DESC LIMIT $batas";



				$res_graph_pirep =  $this->db->query($sql_graph_pirep)->result_array();
				$res_graph_delay = $this->db->query($sql_graph_delay)->result_array();
				$res_graph_marep = $this->db->query($sql_graph_marep)->result_array();
				$row_delay_cnt =  $this->db->query($sql_graph_delay)->num_rows();
				$row_pirep_cnt = $this->db->query($sql_graph_pirep)->num_rows();
				$row_marep_cnt = $this->db->query($res_graph_marep)->num_rows();
				$i = 0;
				foreach ($res_graph_pirep as $key ) {
					if ($key['REG'] != null and $key['number_of_reg'] != null ) {
						$arr_pirep[$i]['ata'] = $key['REG'];
						$arr_pirep[$i]['number_of_ata'] = $key['number_of_reg'];
						$i++;
					}
				}
				$i = 0;
				foreach ($res_graph_marep as $key ) {

					if ($key['REG'] != null and $key['number_of_reg'] != null ) {
						$arr_marep[$i]['ata'] = $key['REG'];
						$arr_marep[$i]['number_of_ata'] = $key['number_of_reg'];
						$i++;
					}
				}

				$i = 0;
				foreach ($res_graph_delay as $key) {
					if ($key['REG'] != null and $key['number_of_reg'] != null ) {
						$arr_delay[$i]['ATAtdm'] = $key['Reg'];
						$arr_delay[$i]['number_of_ata1'] = $key['number_of_reg'];
						$i++;
					}

				}
			}

			$response  = 
			[
				"arr_delay" => $arr_delay,
				"arr_pirep"	=> $arr_pirep,
				"arr_marep"	=> $arr_marep,
				"sql"		=> $sql_graph_marep,
				"judul" 	=> $e,
				"ata"		=> $ata,
				"marep_cnt" => count($arr_marep),
				"pirep_cnt" => count($arr_pirep),
				"delay_cnt" => count($arr_delay)
			];
			echo json_encode($response);

		}
		else{
			if ($Graph_type == 'subata') {
					// awal
				$sql_graph_pirep = "SELECT CASE
				WHEN subata = '0' THEN CONCAT_WS('-',ata, '00')
				WHEN subata = '' THEN CONCAT_WS('-', ata, '00')
				ELSE CONCAT_WS('-', ata, subata)
				END AS ata_subata
				FROM tblpirep_swift WHERE ata >= 21 AND DATE BETWEEN ".$DateStart." AND ".$DateEnd." AND ".$ACType." AND REG LIKE '%".$ACReg."%' AND PirepMarep = 'pirep' AND ATA like '%".$ata."%' LIMIT $batas ";


				$sql_graph_marep = "SELECT CASE
				WHEN subata = '0' THEN CONCAT_WS('-',ata, '00')
				WHEN subata = '' THEN CONCAT_WS('-', ata, '00')
				ELSE CONCAT_WS('-', ata, subata)
				END AS ata_subata
				FROM tblpirep_swift WHERE ata >= 21 AND DATE BETWEEN ".$DateStart." AND ".$DateEnd." AND ".$ACType." AND REG LIKE '%".$ACReg."%' AND PirepMarep = 'Marep' AND ATA like '%".$ata."%' LIMIT $batas ";

				$sql_graph_delay = "SELECT CASE
				WHEN ISNULL(SubATAtdm) THEN CONCAT_WS('-' ,ATAtdm, '00')
				WHEN SubATAtdm = '' THEN CONCAT_WS('-' ,ATAtdm, '00')
				WHEN SubATAtdm = '00' THEN CONCAT_WS('-' ,ATAtdm, '00')
				WHEN SubATAtdm = '0' THEN CONCAT_WS('-' ,ATAtdm, '00')
				ELSE CONCAT_WS('-' ,ATAtdm, SubATAtdm)
				END AS new_ata
				FROM mcdrnew WHERE DCP <>'X' AND ".$ACType." AND REG LIKE '%".$ACReg."' AND DateEvent BETWEEN ".$DateStart." AND ".$DateEnd." AND ATAtdm like '%".$ata."%' LIMIT $batas ";



				$res_graph_pirep =  $this->db->query($sql_graph_pirep)->result_array();
				$row_pirep_cnt = $this->db->query($sql_graph_pirep)->num_rows();

				$res_graph_delay = $this->db->query($sql_graph_delay)->result_array();
				$row_delay_cnt =  $this->db->query($sql_graph_delay)->num_rows();
				
				$res_graph_marep = $this->db->query($sql_graph_marep)->result_array();
				$row_marep_cnt =  $this->db->query($sql_graph_marep)->num_rows();


				$temp_pirep = [];
				$temp_marep = [];
				$ar = [];
				$ar_new = [];

				if($row_pirep_cnt>0){
					$i = 0;
					foreach ($res_graph_pirep as $key) {
						$temp_pirep[$i] = $key['ata_subata'];
						$i++;
					}
					for($i=0; $i<sizeof($temp_pirep); $i++){
						if($temp_pirep[$i] == NULL){
							$ar[$i] = '0000';
						}
						else {
							$ar[$i] = $temp_pirep[$i];
						}
					}
					$ar0 = array_count_values($ar);
					arsort($ar0);
		            $keys=array_keys($ar0);//Split the array so we can find the most occuring key
		            $arr_pirep = Array();
		            for($i=0; $i<$batas; $i++){
		            	if ($keys[$i] != null and $ar0[$keys[$i]]) {
		            		$arr_pirep[$i]['ata'] = $keys[$i];
		            		$arr_pirep[$i]['number_of_ata'] = $ar0[$keys[$i]];
		            	}

		            }
		        }



		        if($row_marep_cnt>0){
		        	$i = 0;
		        	foreach ($res_graph_marep as $key) {
		        		$temp_marep[$i] = $key['ata_subata'];
		        		$i++;
		        	}
		        	for($i=0; $i<sizeof($temp_marep); $i++){
		        		if($temp_marep[$i] == NULL){
		        			$ar[$i] = '0000';
		        		}
		        		else {
		        			$ar[$i] = $temp_marep[$i];
		        		}
		        	}
		        	$ar0 = array_count_values($ar);
		        	arsort($ar0);
		        	$keys=array_keys($ar0);
		        	$arr_marep = Array();
		        	for($i=0; $i<$batas; $i++){

		        		if ($keys[$i] != null and $ar0[$keys[$i]]) {
		        			$arr_marep[$i]['ata'] = $keys[$i];
		        			$arr_marep[$i]['number_of_ata'] = $ar0[$keys[$i]];
		        		}

		        		
		        	}
		        }


          //======================================================================Delay=======================================================

		        if($row_delay_cnt>0){
		        	$i = 0;
		        	foreach ($res_graph_delay as $key) {
		        		$temp_delay[$i] = $key['new_ata'];

		        		$i++;
		        	}


		        	for($i=0; $i<sizeof($temp_delay); $i++){
		        		if($temp_delay[$i] == NULL){
		        			$ar_new[$i] = '0000';
		        		}
		        		else {
		        			$ar_new[$i] = $temp_delay[$i];
		        		}
		        	}



		        	$ar1 = array_count_values($ar_new);

		        	arsort($ar1);

		        	$keys=array_keys($ar1);

		        	$arr_delay = Array();
		        	for($i=0; $i<$batas; $i++){
		        		if ($keys[$i] != null and $ar0[$keys[$i]]) {
		        			$arr_delay[$i]['ATAtdm'] = $keys[$i];
		        			$arr_delay[$i]['number_of_ata1'] = $ar1[$keys[$i]];
		        		}


		        	}
		        }

		        $response  = 
		        [
		        	"arr_delay" => $arr_delay,
		        	"arr_pirep"	=> $arr_pirep,
		        	"arr_marep" => $arr_marep,
		        	"judul"		=> $e,
		        	"sql"		=> $sql_graph_pirep,
		        	"marep_cnt" => count($arr_marep),
		        	"pirep_cnt" => count($arr_pirep),
		        	"delay_cnt" => count($arr_delay)
		        ];
		        echo json_encode($response);

				// akhir
		    } else if ($Graph_type == 'keyproblem') {

		    	$sql_graph_pirep = "SELECT distinct Keyword, COUNT(Keyword) AS number_of_keyword FROM tblpirep_swift WHERE DATE BETWEEN ".$DateStart." AND ".$DateEnd." AND ata >= 21 AND ".$ACType." AND REG LIKE '%".$ACReg."%' AND Keyword != '' AND PirepMarep = 'pirep' AND ATA like '%".$ata."%'  GROUP BY Keyword ORDER BY number_of_keyword DESC LIMIT $batas ";


		    	$sql_graph_marep = "SELECT distinct Keyword, COUNT(Keyword) AS number_of_keyword FROM tblpirep_swift WHERE DATE BETWEEN ".$DateStart." AND ".$DateEnd." AND ata >= 21 AND ".$ACType." AND REG LIKE '%".$ACReg."%' AND Keyword != '' AND PirepMarep = 'Marep' AND ATA like '%".$ata."%' GROUP BY Keyword ORDER BY number_of_keyword DESC LIMIT $batas ";


		    	$sql_graph_delay = "SELECT distinct KeyProblem, COUNT(KeyProblem) AS number_of_key FROM mcdrnew WHERE ".$ACType." AND DCP <> 'X' AND REG LIKE '%".$ACReg."' AND DateEvent BETWEEN ".$DateStart." AND ".$DateEnd." AND KeyProblem != '' AND ATAtdm like '%".$ata."%' GROUP BY KeyProblem ORDER BY number_of_key DESC LIMIT $batas ";

		    	$res_graph_pirep =  $this->db->query($sql_graph_pirep)->result_array();
		    	$res_graph_delay = $this->db->query($sql_graph_delay)->result_array();
		    	$res_graph_marep = $this->db->query($sql_graph_marep)->result_array();

		    	$row_delay_cnt =  $this->db->query($sql_graph_marep)->num_rows();
		    	$row_pirep_cnt = $this->db->query($sql_graph_pirep)->num_rows();
		    	$row_marep_cnt = $this->db->query($sql_graph_marep)->num_rows();

		    	$i = 0;
		    	$tmp = [];
		    	$tpm = [];
		    	foreach ($res_graph_pirep as $key ) {
		    		if ($key['Keyword'] != null and  $key['number_of_keyword'] != null) {
		    			$arr_pirep[$i]['ata'] = $key['Keyword'];
		    			$arr_pirep[$i]['number_of_ata'] = $key['number_of_keyword'];
		    			$i++;
		    		}

		    		
		    	}
		    	$i = 0;
		    	foreach ($res_graph_marep as $key ) {
		    		if ($key['Keyword'] != null and  $key['number_of_keyword'] != null) {
		    			$arr_marep[$i]['ata'] = $key['Keyword'];
		    			$arr_marep[$i]['number_of_ata'] = $key['number_of_keyword'];
		    			$i++;
		    		}
		    		
		    	}
		    	$i = 0;
		    	foreach ($res_graph_delay as $key) {
		    		if ($key['Keyword'] != null and  $key['number_of_keyword'] != null) {
		    			$arr_delay[$i]['ATAtdm'] = $key['KeyProblem'];
		    			$arr_delay[$i]['number_of_ata1'] = $key['number_of_key'];
		    			$i++;
		    		}
		    		
		    	}
		    	$akhir = array_sum($tmp);
		    	$response  = 
		    	[
		    		"arr_delay" => $arr_delay,
		    		"arr_pirep"	=> $arr_pirep,
		    		"arr_marep"	=> $arr_marep,
		    		"judul"		=> $e,
		    		"marep_cnt" => count($arr_marep),
		    		"pirep_cnt" => count($arr_pirep),
		    		"delay_cnt" => count($arr_delay)
		    	];
		    	echo json_encode($response);

		    }

		}

	}
	public function searchDetail()
	{
		$ac = $this->input->post("actype[]");
		$a = [];
		$k = [];
		for ($i=0; $i < count($ac) ; $i++) { 
			$a[$i] =  "'".$ac[$i]."'".',';
			$k[$i] =  $ac[$i].',';
		}
		$b =  implode(',',$a);
		$hj =  implode(',',$k);
		$c =  str_replace(',,', ',', $b);
		$za =  str_replace(',,', ',', $hj);
		$d =  rtrim($c,',');	
		$e =  rtrim($za,',');
		$ACType = "ACtype IN ($d)";

		$graf_actype = $d;
		if(empty($_POST["acreg"])){
			$ACReg = "";
		}
		else{
			$ACReg = $_POST['acreg'];
		}
		
		$ata = $_POST['ata'];

		if(!empty($_POST["date_from"])){

			$a = "'".$_POST["date_from"]."'";
			$DateStart = $a;
		}
		else{
			$DateStart = "";
		}
		if(!empty($_POST["date_to"])){
			$b = "'".$_POST["date_to"]."'";
			$DateEnd = $b;
		}
		else{
			$DateEnd = "";
		}
		if ($_POST["dikirim"] == 'UNDEFINED') {
			$dikirim = '';
		}else {

			$dikirim = $_POST["dikirim"];
		}
		$set = $_POST['p_graph'];
		$value = $_POST['value_category'];
		$str_stop = intval($value);
		if ($set == 'keyproblem') {
			$param = 'KeyProblem';
		} else if ($set == 'ac_reg') {
			$param = 'Reg';
		} else if ($set == 'ata' || $set == 'subata') {
			$param = 'ATAtdm';
		} 

		$sql = "SELECT ID, ACtype as ac,DateEvent,EventID,ACtype, Reg, DepSta, ArivSta,KeyProblem,FlightNo,HoursTek,Mintek, ATAtdm, SubATAtdm, Problem,Rectification, DCP, RtABO, MinTot, LastRectification AS last FROM mcdrnew WHERE ".$ACType." AND DCP <> 'X' AND REG LIKE '%".$ACReg."' AND DateEvent BETWEEN ".$DateStart." AND ".$DateEnd." AND ATAtdm like '%".$ata."%' AND $param = '".$dikirim."' order by HoursTek + 0 DESC ";

		$result = $this->db->query($sql)->result_array();
		$response = [];
		$no = 1;
		$unit = $this->session->userdata('unit');
		foreach ($result as $key) {
			if ($no > $str_stop) break;
			$bnk['no']		= $no;
			$bnk['ac'] = $key['ac'];
			$bnk['Reg'] = $key['Reg'];
			$clsa = $key['ID'];
			if ($key['KeyProblem'] == '' or empty($key['KeyProblem']) ) {
				$bnk['KeyProblem'] = 'UNDEFINED';
			}else {
				$bnk['KeyProblem'] = $key['KeyProblem'];
			} 
			if ($bnk['DateEvent'] == null or empty($bnk['DateEvent'])) {
				$bnk['TargetDate'] = '-';
			} else {
				$bnk['TargetDate'] = $key["DateEvent"];
			}

			if ($key['EventID'] == null or empty($key['EventID'])) {
				$bnk['EventID'] = '-';
			} else {
				$bnk['EventID'] = $key['EventID'];
			}
			if ($key['last'] == null or empty($key['last'])) {
				$bnk['last'] = '-';
			} else {
				$bnk['last'] = $key['last'];
			}
			$bnk['DepSta'] = $key['DepSta'];
			$bnk['ArivSta'] = $key['ArivSta'];
			$bnk['FlightNo'] = $key['FlightNo'];
			$bnk['SubATAtdm'] = $key['SubATAtdm'];
			$bnk['ATAtdm'] = $key['ATAtdm'];
			$bnk['Rectification'] = $key['Rectification'];
			$bnk['RtABO'] = $key['RtABO'];
			$bnk['DCP'] = $key['DCP'];
			$bnk['Problem'] = $key['Problem'];
			$bnk['TDAM'] = convertToHoursMins($key['HoursTek'],$key['Mintek']);
			if ($unit == 'TER-1') {
				$bnk['act'] = "<a  href=".base_url('Pareto/DelayUpdate?notif='.$clsa.'')." class='btn btn-success btn-sm'><i class='fa fa-edit'></i></a>";
			} else {
				$bnk['act'] = '';
			}


			$no++;
			array_push($response,$bnk);
		}
		$newresponse = array('data' => $response,'sql' => $sql);

		echo json_encode($newresponse);
	}


	public function searchDetailPirep()
	{
		$ac = $this->input->post("actype[]");
		$a = [];
		$k = [];
		for ($i=0; $i < count($ac) ; $i++) { 
			$a[$i] =  "'".$ac[$i]."'".',';
			$k[$i] =  $ac[$i].',';
		}
		$unit = $this->session->userdata('unit');
		$b =  implode(',',$a);
		$hj =  implode(',',$k);
		$c =  str_replace(',,', ',', $b);
		$za =  str_replace(',,', ',', $hj);
		$d =  rtrim($c,',');	
		$e =  rtrim($za,',');
		$ACType = "ACtype IN ($d)";
		$graf_actype = $d;
		if(empty($_POST["acreg"])){
			$ACReg = "";
		}
		else{
			$ACReg = $_POST['acreg'];
		}
		$ata = $_POST['ata'];

		if(!empty($_POST["date_from"])){

			$a = "'".$_POST["date_from"]."'";
			$DateStart = $a;
		}
		else{
			$DateStart = "";
		}
		if(!empty($_POST["date_to"])){
			$b = "'".$_POST["date_to"]."'";
			$DateEnd = $b;
		}
		else{
			$DateEnd = "";
		}
		if ($_POST["dikirim"] == 'UNDEFINED') {
			$dikirim = '';
		}else {

			$dikirim = $_POST["dikirim"];
		}

		$set = $_POST['p_graph'];
		$value = $_POST['value_category'];
		$str_stop = intval($value);

		if ($set == 'keyproblem') {
			$param = 'Keyword';
		} else if ($set == 'ac_reg') {
			$param = 'REG';
		} else if ($set == 'ata' || $set == 'subata') {
			$dikirim = substr($_POST["dikirim"],0,2);
			$param = 'ATA';
		} 

		$sql = "SELECT ACtype as ac, DATE, SEQ, Notification,REG, STADEP, STAARR, FN, ATA, SUBATA, PROBLEM, ACTION, PirepMarep,Keyword FROM tblpirep_swift WHERE DATE BETWEEN ".$DateStart." AND ".$DateEnd." AND ata >= 21 AND ".$ACType." AND REG LIKE '%".$ACReg."%' AND PirepMarep = 'pirep' AND ATA like '%".$ata."%' AND $param = '".$dikirim."'  ";

		$result = $this->db->query($sql)->result_array();
		$response = [];
		$no = 1;
		foreach ($result as $key) {
			if ($no > $str_stop) break;
			$bnk['no']		= $no;
			$clsa = $key['Notification'];
			$bnk['ac'] = $key['ac'];
			$bnk['REG'] = $key['REG'];
			if ($key['Keyword'] == '' or empty($key['Keyword']) ) {
				$bnk['Keyword'] = 'UNDEFINED';
			}else {
				$bnk['Keyword'] = $key['Keyword'];
			}
			$bnk['PROBLEM'] = $key['PROBLEM'];
			$bnk['SEQ'] = $key['SEQ'];
			$bnk['Notification'] = $key['Notification'];
			$bnk['REG'] = $key['REG'];
			$bnk['STADEP'] = $key['STADEP'];
			$bnk['STAARR'] = $key['STAARR'];
			$bnk['FN'] = $key['FN'];
			$bnk['ATA'] = $key['ATA'];
			$bnk['SUBATA'] = $key['SUBATA'];
			$bnk['ACTION'] = $key['ACTION'];
			$bnk['PirepMarep'] = $key['PirepMarep'];

			if ($key['DATE'] == null || empty($key['DATE'])) {
				$bnk['TargetDate'] = '-';
			} else {
				$bnk['TargetDate'] = $key['DATE'];
			}
			if ($unit == 'TER-1') {
				$bnk['act'] = "<a  href=".base_url('Pareto/PirepUpdate?notif='.$clsa.'')." class='btn btn-success btn-sm'><i class='fa fa-edit'></i></a>";
			} else {
				$bnk['act'] = '';
			}

			array_push($response,$bnk);
			$no++;
		}

		$newresponse = array('data' => $response);

		echo json_encode($newresponse);
	}




	public function searchDetailMarep()
	{
		$ac = $this->input->post("actype[]");
		$a = [];
		$k = [];
		for ($i=0; $i < count($ac) ; $i++) { 
			$a[$i] =  "'".$ac[$i]."'".',';
			$k[$i] =  $ac[$i].',';
		}
		$unit = $this->session->userdata('unit');
		$b =  implode(',',$a);
		$hj =  implode(',',$k);
		$c =  str_replace(',,', ',', $b);
		$za =  str_replace(',,', ',', $hj);
		$d =  rtrim($c,',');	
		$e =  rtrim($za,',');
		$ACType = "ACtype IN ($d)";
		$graf_actype = $d;
		if(empty($_POST["acreg"])){
			$ACReg = "";
		}
		else{
			$ACReg = $_POST['acreg'];
		}
		$ata = $_POST['ata'];
		if(!empty($_POST["date_from"])){

			$a = "'".$_POST["date_from"]."'";
			$DateStart = $a;
		}
		else{
			$DateStart = "";
		}
		if(!empty($_POST["date_to"])){
			$b = "'".$_POST["date_to"]."'";
			$DateEnd = $b;
		}
		else{
			$DateEnd = "";
		}
		if ($_POST["dikirim"] == 'UNDEFINED') {
			$dikirim = '';
		}else {

			$dikirim = $_POST["dikirim"];
		}

		$set = $_POST['p_graph'];
		$value = $_POST['value_category'];
		$str_stop = intval($value);

		if ($set == 'keyproblem') {
			$param = 'Keyword';
		} else if ($set == 'ac_reg') {
			$param = 'REG';
		} else if ($set == 'ata' || $set == 'subata') {
			$dikirim = substr($_POST["dikirim"],0,2);
			$param = 'ATA';
		} 

		$sql = "SELECT ACtype as ac, DATE, SEQ, Notification,REG, STADEP, STAARR, FN, ATA, SUBATA, PROBLEM, ACTION, PirepMarep,Keyword FROM tblpirep_swift WHERE DATE BETWEEN ".$DateStart." AND ".$DateEnd." AND ata >= 21 AND ".$ACType." AND REG LIKE '%".$ACReg."%' AND PirepMarep = 'Marep' AND ATA like '%".$ata."%' AND $param = '".$dikirim."'  ";

		$result = $this->db->query($sql)->result_array();
		$response = [];
		$no = 1;
		foreach ($result as $key) {
			if ($no > $str_stop) break;
			$bnk['no']		= $no;
			$clsa = $key['Notification'];
			$bnk['ac'] = $key['ac'];
			$bnk['REG'] = $key['REG'];
			if ($key['Keyword'] == '' or empty($key['Keyword']) ) {
				$bnk['Keyword'] = 'UNDEFINED';
			}else {
				$bnk['Keyword'] = $key['Keyword'];
			}
			$bnk['PROBLEM'] = $key['PROBLEM'];
			$bnk['SEQ'] = $key['SEQ'];
			$bnk['Notification'] = $key['Notification'];
			$bnk['REG'] = $key['REG'];
			$bnk['STADEP'] = $key['STADEP'];
			$bnk['STAARR'] = $key['STAARR'];
			$bnk['FN'] = $key['FN'];
			$bnk['ATA'] = $key['ATA'];
			$bnk['SUBATA'] = $key['SUBATA'];
			$bnk['ACTION'] = $key['ACTION'];
			$bnk['PirepMarep'] = $key['PirepMarep'];

			if ($key['DATE'] == null || empty($key['DATE'])) {
				$bnk['TargetDate'] = '-';
			} else {
				$bnk['TargetDate'] = $key['DATE'];
			}
			if ($unit == 'TER-1') {
				$bnk['act'] = "<a  href=".base_url('Pareto/PirepUpdate?notif='.$clsa.'')." class='btn btn-success btn-sm'><i class='fa fa-edit'></i></a>";
			} else {
				$bnk['act'] = '';
			}

			array_push($response,$bnk);
			$no++;
		}

		$newresponse = array('data' => $response);

		echo json_encode($newresponse);
	}
	public function PirepUpdate()
	{
		$notif = $this->input->get("notif");
		$data['title'] = 'Pirep Update Data';
		$data['actype'] = $this->Techlog_model->getactype();
		$data['dt'] = $this->db->query("SELECT ACtype as ac, DATE, SEQ, Notification,REG, STADEP, STAARR, FN, ATA, SUBATA, PROBLEM, ACTION, PirepMarep,Keyword FROM tblpirep_swift WHERE Notification = '$notif' ")->row_array();
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/pirepupdate');
		$this->load->view('template/fo2');
	}

	public function DelayUpdate()
	{
		$notif = $this->input->get("notif");
		$data['title'] = 'Delay Update Data';
		$data['actype'] = $this->Techlog_model->getactype();
		$data['dt'] = $this->db->query("SELECT ID, ACtype as ac,DateEvent,EventID,ACtype, Reg, DepSta, ArivSta,KeyProblem,FlightNo,HoursTek,Mintek, ATAtdm, SubATAtdm, Problem,Rectification, DCP, RtABO, MinTot, LastRectification AS last FROM mcdrnew WHERE ID = '$notif' ")->row_array();
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/pirep');
		$this->load->view('template/fo2');
	}

	public function UpdatePirep()
	{
		$notif = $this->input->post("notif");
		$seq = $this->input->post("seq");
		$stadep = $this->input->post("stadep");
		$staar = $this->input->post("staar");
		$ata = $this->input->post("ata");
		$date = $this->input->post("date");
		$subata = $this->input->post("subata");
		$fn = $this->input->post("fn");
		$reg = $this->input->post("reg");
		$actype = $this->input->post("actype");
		$problem = $this->input->post("problem");
		$action = $this->input->post("action");
		$coding = $this->input->post("coding");
		$KeyProblem = $this->input->post("KeyProblem");

		$this->db->set('ACTYPE',$actype);
		$this->db->set('REG',$reg);
		$this->db->set('STADEP',$stadep);
		$this->db->set('STAARR',$staar);
		$this->db->set('DATE',$date);
		$this->db->set('FN',$fn);
		$this->db->set('SEQ',$seq);
		$this->db->set('ATA',$ata);
		$this->db->set('SUBATA',$subata);
		$this->db->set('PROBLEM',$problem);
		$this->db->set('ACTION',$action);
		$this->db->set('PirepMarep',$coding);
		$this->db->set('Keyword',$keyproblem);
		$this->db->where('Notification', $notif);
		$this->db->update('tblpirep_swift');
		$this->session->set_flashdata("Pesan", "Update");
		redirect('Pareto','refresh');

	}


	public function UpdateDelay()
	{

		$notif = $this->input->post("notif");
		$event = $this->input->post("event");
		$stadep = $this->input->post("stadep");
		$staar = $this->input->post("staar");
		$ata = $this->input->post("ata");
		$subata = $this->input->post("subata");
		$fn = $this->input->post("fn");
		$rta = $this->input->post("rta");
		$reg = $this->input->post("reg");
		$actype = $this->input->post("actype");
		$date = $this->input->post("date");
		$dcp = $this->input->post("dcp");
		$problem = $this->input->post("problem");
		$action = $this->input->post("action");
		$tdl = $this->input->post("tdl");
		$last = $this->input->post("last");
		$keyproblem = $this->input->post("keyproblem");
		$fdd = $this->input->post("fdd");
		$aog = $this->input->post("aog");

		$this->db->set('ACType',$actype);
		$this->db->set('Reg',$reg);
		$this->db->set('FlightNo',$fn);
		$this->db->set('DepSta',$stadep);
		$this->db->set('ArivSta',$staar);
		$this->db->set('DCP',$dcp);
		$this->db->set('EventID',$event);
		$this->db->set('DateEvent',$date);
		$this->db->set('ATAtdm',$ata);
		$this->db->set('SubATAtdm',$subata);
		$this->db->set('Problem',$problem);
		$this->db->set('Rectification',$action);
		$this->db->set('LastRectification',$last);
		$this->db->set('KeyProblem',$keyproblem);
		$this->db->set('FDD',$fdd);
		$this->db->set('Aog',$aog);
		$this->db->where('ID', $notif);
		$this->db->update('mcdrnew');
		$this->session->set_flashdata("Pesan", "Update");
		redirect('Techlog','refresh');

	}














}

/* End of file Pareto.php */
/* Location: ./application/controllers/Pareto.php */