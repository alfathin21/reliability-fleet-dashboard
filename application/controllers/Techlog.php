<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Techlog extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		is_log_in();
		$this->load->model('Techlog_model');	
		error_reporting(0);
	}
	public function index()
	{
		$data['title'] = 'Techlog / Delay';
		$data['ata'] = $this->Techlog_model->getata();
		$data['desc'] = $this->Techlog_model->getdesc();
		$data['unit'] = $this->session->userdata('unit');
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/techlog');
		$this->load->view('template/fo2');
	}
	public function search()
	{
		$ac = $this->input->post("actype[]");
		$a = [];
		for ($i=0; $i < count($ac) ; $i++) { 
			$a[$i] =  "'".$ac[$i]."'".',';
		}
		$b = implode(',',$a);
		$c = str_replace(',,', ',', $b);
		$d =  rtrim($c,',');	
		$ACType = "IN ($d)";
		$graf_actype = $d;
		if(empty($_POST["acreg"])){
			$ACReg = "";
		}
		else{
			$ACReg = " AND REG LIKE '%".$_POST['acreg']."%'";
		}
		if(empty($_POST["datefrom"])){
			$DateStart = "";
			$DateStart2 = "";
		}
		else{
			$temp_date =  $_POST['datefrom'];
			$graf_datefrom = $temp_date;
			$DateStart = " AND DATE BETWEEN '".$temp_date."'";
			$DateStart2 = " AND DATEEVENT BETWEEN '".$temp_date."'";
		}
		if(empty($_POST["dateto"])){
			$DateEnd = "";
		}
		else{
			$temp_date = $_POST['dateto'];
			$graf_dateto = $temp_date;
			$DateEnd = " AND '".$temp_date."'";
		}
		if(empty($_POST["ata"])){
			$ATA = "";
			$ATA2 = "";
		}
		else{
			$ATA = " AND ATA = '".$_POST['ata']."'";
			$ATA2 = " AND ATATDM = '".$_POST['ata']."'";
		}
		if(empty($_POST["sub_ata"])){
			$Fault_code = "";
			$Fault_code2 = "";
		}
		else{
			$Fault_code = " AND SUBATA = '".$_POST['sub_ata']."'";
			$Fault_code2 = " AND SUBATATDM = '".$_POST['sub_ata']."'";
		}
		if(empty($_POST["keyword"])){
			$Keyword = "";
		}
		else{
			$Keyword = " AND (PROBLEM LIKE '%".$_POST['keyword']."%' OR RECTIFICATION LIKE '%".$_POST['keyword']."%')";
		}
		if(empty($_POST["KeyProblem"])){
			$keyproblem = "";
		}
		else{
			$keyproblem = " AND (KeyProblem LIKE '%".$_POST['KeyProblem']."%' )";
		}
		$cl_delay = $_POST['cl_delay'];
		$cl_cancel = $_POST['cl_cancel'];
		$cl_x = $_POST['cl_x'];
		if ($cl_x === 'x' && $cl_cancel === 'c' && $cl_delay === 'd') {
			$graph_title = "All Filter Delay - ";
			$s_jadi = "'".strtoupper($cl_delay)."'";
			$u_jadi = "'".strtoupper($cl_cancel)."'";
			$z_jadi = "'".strtoupper($cl_x)."'";
			$data  = $s_jadi.','.$u_jadi.','.$z_jadi;
			$dcp = "AND DCP IN ($data)";
		} else if ($cl_cancel === 'c' && $cl_delay === 'd') {
			$graph_title = "Delay & Cancel - ";
			$s_jadi = "'".strtoupper($cl_delay)."'";
			$u_jadi = "'".strtoupper($cl_cancel)."'";
			$data  = $s_jadi.','.$u_jadi;
			$dcp = "AND DCP IN ($data)";
		}
		else if ($cl_x === 'x' && $cl_delay === 'd') {
			$graph_title = "Delay & Non Technical Delay - ";
			$s_jadi = "'".strtoupper($cl_delay)."'";
			$u_jadi = "'".strtoupper($cl_x)."'";
			$data  = $s_jadi.','.$u_jadi;
			$dcp = "AND DCP IN ($data)";
		}
		else if ($cl_x === 'x' && $cl_cancel === 'c') {
			$graph_title = "Cancel & Non Technical Delay - ";
			$s_jadi = "'".strtoupper($cl_cancel)."'";
			$u_jadi = "'".strtoupper($cl_x)."'";
			$data  = $s_jadi.','.$u_jadi;
			$dcp = "AND DCP IN ($data)";
		}
		else if ($cl_delay === 'd') {
			$graph_title = "Delay - ";
			$dcp = "AND DCP = '".strtoupper($cl_delay)."'";
		} else if ($cl_cancel === 'c') {
			$graph_title = "Cancel - ";
			$dcp = "AND DCP = '".strtoupper($cl_cancel)."'";
		} else if ($cl_x === 'x') {
			$graph_title = "Non Technical Delay - ";
			$dcp = "AND DCP = '".strtoupper($cl_x)."'";
		} 
		if (empty($_POST['filter'])) {
			$rta = '';
		} else {
			$rta =  "AND RtABO = '".$_POST['filter']."' ";
		}

		$sql_delay = "SELECT ID,DateEvent,EventID,ACtype, Reg, DepSta, ArivSta, FlightNo,HoursTek,Mintek, ATAtdm, SubATAtdm, Problem,Rectification, DCP, RtABO, MinTot, LastRectification AS last, keyproblem FROM mcdrnew WHERE ACtype ".$ACType."".$ACReg."".$ATA2."".$Fault_code2."".$Keyword."".$rta.""."".$dcp."".$DateStart2."".$DateEnd.""."".$keyproblem."";
		$sql_grafik = "SELECT COUNT(DateEvent) as delay, DATE_FORMAT(DateEvent, '%Y-%m') as DateEvents FROM mcdrnew WHERE ACtype ".$ACType."".$ACReg."".$ATA2."".$Fault_code2."".$Keyword."".$rta.""."".$dcp."".$DateStart2."".$DateEnd.""."".$keyproblem." GROUP BY DateEvents ORDER BY DateEvent";

		$result = $this->db->query($sql_delay)->result_array();
		$result_grafik = $this->db->query($sql_grafik)->result_array(); 
		$count_grafik = $this->db->query($sql_grafik)->num_rows();
		$temp_arr_graf = [];
		$before_temp = [];
		$i=0;

		foreach ($result_grafik as $y) {
			$o['DateEvents'] = $y['DateEvents'];
			$o['delay'] = $y['delay'];
			array_push($temp_arr_graf, $o);
		}

		$i = 0;
		$temp_arr = 0;
		$now = strtotime($graf_datefrom);
		$end_date = strtotime($graf_dateto);
		$end_date = strtotime("+1 Month", $end_date);
		while (date("Y-m" ,$now) != date("Y-m" ,$end_date)) {
      //Apabila Bulan dan tahun sekarang sama dengan bulan dan tahun pada tabel hasil query, maka hasilnya akan disimpan
      //dalam array
			if($count_grafik != 0 and $temp_arr_graf[$temp_arr]['DateEvents'] == date("Y-m", $now)){
				$arr_delay_grafik[$i]['delay'] = $temp_arr_graf[$temp_arr]['delay'];
				$arr_delay_grafik[$i]['DateEvents'] = $temp_arr_graf[$temp_arr]['DateEvents'];
				if($temp_arr < $count_grafik-1)
					$temp_arr++;
				$i++;
			}
      //Apabila masih tidak sama, berarti menyimpan jumlah kejadian 0 ke dalam array
			else {
				$arr_delay_grafik[$i]['delay'] = 0;
				$arr_delay_grafik[$i]['DateEvents'] = date("Y-m", $now);
				$i++;
			}
			$now = strtotime("+1 Month", $now);
		}

		$arr_x = array();
		$arr_y = array();

		for($i = 0; $i < count($arr_delay_grafik); $i++){
			$arr_y[] = $arr_delay_grafik[$i]['delay'];
			$arr_x[] = $arr_delay_grafik[$i]['DateEvents'];
		}
		$response = [];
		$no = 1;
		$unit = $this->session->userdata('unit');

		foreach ($result as $key) {
			$clsa = $key['ID'];
			$h['no'] = $no;
			$h['notif'] = $key["EventID"];
			$h['ACtype'] = $key["ACtype"];
			$h['Reg'] = $key["Reg"];
			$h['FlightNo'] = $key["FlightNo"];
			$h['DepSta'] = $key["DepSta"];
			$h['DateEvent'] = $key["DateEvent"];
			$h['FlightNo'] = $key["FlightNo"];
			$h['ArivSta'] = $key["ArivSta"];
			$h['Problem'] = $key["Problem"];
			$h['Rectification'] = html_entity_decode($key["Rectification"]);
			$h['ATAtdm'] = $key["ATAtdm"];
			$h['SubATAtdm'] = $key["SubATAtdm"];
			$h['DCP'] = $key["DCP"];
			$h['RtABO'] = $key["RtABO"];
			$h['last'] = html_entity_decode($key["last"]);
			$h['total'] =  convertToHoursMins($key['HoursTek'],$key['Mintek']);
			if ($unit == 'TER-1' || $unit == '') {
				$h['act'] = "<a href=".base_url('Techlog/DelayUpdate?notif='.$clsa.'')." class='btn btn-success btn-sm'><i class='fa fa-edit'></i></a>";
			} else {
				$h['act'] = '';
			}
			$no++;
			array_push($response, $h);
		}
		$newresponse = array(
			'data' => $response,
			'x'		=> $arr_x,
			'y'		=> $arr_y,
			'title'	=> $graph_title,
			'sql'	=> $sql_delay
		);
		echo json_encode($newresponse);
	}


	public function search2()
	{

		$ac = $this->input->post("actype[]");;
		$a = [];
		for ($i=0; $i < count($ac) ; $i++) { 
			$a[$i] =  "'".$ac[$i]."'".',';
		}
		$b = implode(',',$a);
		$c = str_replace(',,', ',', $b);
		$d =  rtrim($c,',');	
		$ACType = "IN ($d)";
		$graf_actype = $d;
		$unit = $this->session->userdata('unit');
		if(empty($_POST["acreg"])){
			$ACReg = "";
		}
		else{
			$ACReg = " AND REG LIKE '%".$_POST['acreg']."%'";
		}
		if(empty($_POST["datefrom"])){
			$DateStart = "";
			$DateStart2 = "";
		}
		else{
			$temp_date =  $_POST['datefrom'];
			$graf_datefrom = $temp_date;
			$DateStart = " AND DATE BETWEEN '".$temp_date."'";
			$DateStart2 = " AND DATEEVENT BETWEEN '".$temp_date."'";
		}
		if(empty($_POST["dateto"])){
			$DateEnd = "";
		}
		else{
			$temp_date = $_POST['dateto'];
			$graf_dateto = $temp_date;
			$DateEnd = " AND '".$temp_date."'";
		}
		if(empty($_POST["ata"])){
			$ATA = "";
			$ATA2 = "";
		}
		else{
			$ATA = " AND ATA = '".$_POST['ata']."'";
			$ATA2 = " AND ATATDM = '".$_POST['ata']."'";
		}
		if(empty($_POST["sub_ata"])){
			$Fault_code = "";
			$Fault_code2 = "";
		}
		else{
			$Fault_code = " AND SUBATA = '".$_POST['sub_ata']."'";
			$Fault_code2 = " AND SUBATATDM = '".$_POST['sub_ata']."'";
		}
		if(empty($_POST["keyword"])){
			$Keyword = "";
		}
		else{
			$Keyword = " AND (PROBLEM LIKE '%".$_POST['keyword']."%' OR ACTION LIKE '%".$_POST['keyword']."%')";
		}

		if(empty($_POST["KeyProblem"])){
			$keyproblem = "";
		}
		else{
			$keyproblem = " AND (Keyword LIKE '%".$_POST['KeyProblem']."%' )";
		}

		$pirep = $_POST['pirep'];
		$marep = $_POST['marep'];
		$sj = $_POST['sj'];
		$un = $_POST['un'];
		$graph_title = "Pirep - ";
		$s_jadi = "'".strtoupper($pirep)."'";
		$u_jadi = "'".strtoupper($marep)."'";
		$z_jadi = "'".strtoupper($sj)."'";
		$k_jadi = "'".strtoupper($un)."'";
		$data  = $s_jadi.','.$u_jadi.','.$z_jadi.','.$k_jadi;
		$Pimas = "AND PirepMarep IN ($data)";

		
		$sql_pirep = "SELECT DATE, SEQ, Notification, ACTYPE, REG, STADEP, STAARR, FN, ATA, SUBATA, PROBLEM, ACTION, PirepMarep FROM tblpirep_swift WHERE ACTYPE ".$ACType."".$ACReg."".$ATA."".$Fault_code."".$Keyword."".$Pimas."".$DateStart."".$DateEnd."".$keyproblem;

		$sql_grafik = "SELECT COUNT(DATE) as pirep, DATE_FORMAT(DATE, '%Y-%m') as DATES FROM tblpirep_swift WHERE ACTYPE ".$ACType."".$ACReg."".$ATA."".$Fault_code."".$Keyword."".$Pimas."".$DateStart."".$DateEnd."".$keyproblem." GROUP BY DATES ORDER BY DATE";

		$result = $this->db->query($sql_pirep)->result_array();
		$result_grafik = $this->db->query($sql_grafik)->result_array();
		$count_grafik =  $this->db->query($sql_grafik)->num_rows();

		$temp_arr_graf = [];
		$before_temp = [];
		$i=0;


		foreach ($result_grafik as $y) {
			$o['DATES'] = $y['DATES'];
			$o['pirep'] = $y['pirep'];
			array_push($temp_arr_graf, $o);
		}

		$i = 0;
		$temp_arr = 0;
		$now = strtotime($graf_datefrom);
		$end_date = strtotime($graf_dateto);
		$end_date = strtotime("+1 Month", $end_date);
		while (date("Y-m" ,$now) != date("Y-m" ,$end_date)) {
      //Apabila Bulan dan tahun sekarang sama dengan bulan dan tahun pada tabel hasil query, maka hasilnya akan disimpan
      //dalam array
			if($count_grafik != 0 and $temp_arr_graf[$temp_arr]['DATES'] == date("Y-m", $now)){
				$arr_delay_grafik[$i]['pirep'] = $temp_arr_graf[$temp_arr]['pirep'];
				$arr_delay_grafik[$i]['DATES'] = $temp_arr_graf[$temp_arr]['DATES'];
				if($temp_arr < $count_grafik-1)
					$temp_arr++;
				$i++;
			}
      //Apabila masih tidak sama, berarti menyimpan jumlah kejadian 0 ke dalam array
			else {
				$arr_delay_grafik[$i]['pirep'] = 0;
				$arr_delay_grafik[$i]['DATES'] = date("Y-m", $now);
				$i++;
			}
			$now = strtotime("+1 Month", $now);
		}
		$response = [];
		$no = 1;

		$arr_x = array();
		$arr_y = array();
		for($i = 0; $i < $count_grafik; $i++){
			$arr_y[] = $arr_delay_grafik[$i]['pirep'];
			$arr_x[] = $arr_delay_grafik[$i]['DATES'];
		}

		foreach ($result as $key) {
			$clsa = $key['Notification'];
			$h['no'] = $no;
			$h['SEQ'] = $key["SEQ"];
			$h['DATE'] = $key["DATE"];
			$h['Notification'] = $key["Notification"];
			$h['ACTYPE'] = $key["ACTYPE"];
			$h['STADEP'] = $key["STADEP"];
			$h['REG'] = $key["REG"];
			$h['STAARR'] = $key["STAARR"];
			$h['FN'] = $key["FN"];
			$h['ATA'] = $key["ATA"];
			$h['SUB_ATA'] = $key["SUBATA"];
			$h['PROBLEM'] = $key["PROBLEM"];
			$h['ACTION'] = $key["ACTION"];
			$h['PirepMarep'] = $key["PirepMarep"];
			if ($unit == 'TER-1' || $unit == '') {
				$h['act'] = "<a href=".base_url('Techlog/PirepUpdate?notif='.$clsa.'')." class='btn btn-success btn-sm'><i class='fa fa-edit'></i></a>";
			} else {
				$h['act'] = '';
			}

			$no++;
			array_push($response, $h);
		}
		$newresponse = array(
			'data' 	=> $response,
			'x'	   	=> $arr_x,
			'y'	   	=> $arr_y,
			'title'	=> 'Pirep - ',
			'sql_pirep' => $sql_pirep,
			'sql_grafik' => $sql_grafik
		);
		echo json_encode($newresponse);
	}



	public function PirepUpdate()
	{
		$notif = $this->input->get("notif");
		$data['title'] = 'Techlog / Delay';
		$data['actype'] = $this->Techlog_model->getactype();
		$data['dt'] = $this->db->query("SELECT ACtype as ac, DATE, SEQ, Notification,REG, STADEP, STAARR, FN, ATA, SUBATA, PROBLEM, ACTION, PirepMarep,Keyword FROM tblpirep_swift WHERE Notification = '$notif' ")->row_array();
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/pirepupdatedelay');
		$this->load->view('template/fo2');
	}

	public function DelayUpdate()
	{
		$notif = $this->input->get("notif");
		$data['title'] = 'Techlog / Delay';
		$data['actype'] = $this->Techlog_model->getactype();
		$data['dt'] = $this->db->query("SELECT ID, ACtype as ac,DateEvent,EventID,ACtype, Reg, DepSta, ArivSta,KeyProblem,FlightNo,HoursTek,Mintek,FDD,Aog, ATAtdm, SubATAtdm, Problem,Rectification, DCP, RtABO, MinTot, LastRectification AS last FROM mcdrnew WHERE ID = '$notif' ")->row_array();
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/delayupdate');
		$this->load->view('template/fo2');
	}

	public function UpdateDelay()
	{

		$notif = $this->input->post("notif");
		$event = $this->input->post("event");
		$stadep = $this->input->post("stadep");
		$staar = $this->input->post("staar");
		$ata = $this->input->post("ata");
		$subata = $this->input->post("subata");
		$fn = $this->input->post("fn");
		$rta = $this->input->post("rta");
		$reg = $this->input->post("reg");
		$actype = $this->input->post("actype");
		$date = $this->input->post("date");
		$dcp = $this->input->post("dcp");
		$rta = $this->input->post("rta");
		$problem = $this->input->post("problem");
		$action = $this->input->post("action");
		$tdl = $this->input->post("tdl");
		$last = $this->input->post("last");
		$keyproblem = $this->input->post("keyproblem");
		$fdd = $this->input->post("fdd");
		$aog = $this->input->post("aog");

		$this->db->set('ACType',$actype);
		$this->db->set('Reg',$reg);
		$this->db->set('FlightNo',$fn);
		$this->db->set('DepSta',$stadep);
		$this->db->set('ArivSta',$staar);
		$this->db->set('DCP',$dcp);
		$this->db->set('EventID',$event);
		$this->db->set('DateEvent',$date);
		$this->db->set('ATAtdm',$ata);
		$this->db->set('SubATAtdm',$subata);
		$this->db->set('Problem',$problem);
		$this->db->set('Rectification',$action);
		$this->db->set('RtABO',$rta);
		$this->db->set('LastRectification',$last);
		$this->db->set('KeyProblem',$keyproblem);
		$this->db->set('FDD',$fdd);
		$this->db->set('Aog',$aog);
		$this->db->where('ID', $notif);
		$this->db->update('mcdrnew');
		$this->session->set_flashdata("Pesan", "Update");
		redirect('Techlog','refresh');

	}

	public function UpdatePirep()
	{
		$notif = $this->input->post("notif");
		$seq = $this->input->post("seq");
		$stadep = $this->input->post("stadep");
		$staar = $this->input->post("staar");
		$ata = $this->input->post("ata");
		$date = $this->input->post("date");
		$subata = $this->input->post("subata");
		$fn = $this->input->post("fn");
		$reg = $this->input->post("reg");
		$actype = $this->input->post("actype");
		$problem = $this->input->post("problem");
		$action = $this->input->post("action");
		$coding = $this->input->post("coding");
		$KeyProblem = $this->input->post("KeyProblem");

		$this->db->set('ACTYPE',$actype);
		$this->db->set('REG',$reg);
		$this->db->set('STADEP',$stadep);
		$this->db->set('STAARR',$staar);
		$this->db->set('DATE',$date);
		$this->db->set('FN',$fn);
		$this->db->set('SEQ',$seq);
		$this->db->set('ATA',$ata);
		$this->db->set('SUB_ATA',$subata);
		$this->db->set('PROBLEM',$problem);
		$this->db->set('ACTION',$action);
		$this->db->set('PirepMarep',$coding);
		$this->db->set('Keyword',$keyproblem);
		$this->db->where('Notification', $notif);
		$this->db->update('tblpirep_swift');
		$this->session->set_flashdata("Pesan", "Update");
		redirect('Techlog','refresh');

	}


}
