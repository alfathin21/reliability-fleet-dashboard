<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guide extends CI_Controller {

	public function index()
	{
		$data['title'] = 'User Guide';
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/guide');
		$this->load->view('template/fo2');
	}

}

/* End of file Guide.php */
/* Location: ./application/controllers/Guide.php */