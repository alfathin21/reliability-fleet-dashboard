<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtburww extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		is_log_in();
		$this->load->model('Techlog_model');	
		error_reporting(0);
	}

	public function index()
	{
		$data['title'] = 'World Wide';
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/mtburww');
		$this->load->view('template/fo2');
	}

}

/* End of file Mtburww.php */
/* Location: ./application/controllers/Mtburww.php */